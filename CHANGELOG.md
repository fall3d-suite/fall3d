# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this
project adheres to [Semantic Versioning](https://semver.org).

## Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [latest] - 2024-11-27

## [9.0.1] - 2024-11-27

### Fixed
- Wrong indexes copying data from S_swap_in in line 895 (mod_ADS)

### Changed
- New main program (main.F90) and task_Manager becomes a subroutine
- Tasks and ensemble members finished with errors are reported and redirected to stderr
- Warnings for IEEE floating-point exceptions are not longer printed
- All MPI processes are terminated by an end program statement and all stop statements were removed
- `Example` folder renamed to `Templates`

### Removed
- Subroutine task_runend. If errors are detected all MPI process return to the main program

### Deprecated
- Removed target attribute in some variables in mod_ADS

## [9.0] - 2024-10-17

### Added
- Merging of the CPU an GPU codes into a single one
- Compilation systems based on meson and cMake
- New field in configuration file: `OUTPUT_FOOTPRINTS` to enable/disable the generation of footprint files
- New field in configuration file: `OUTPUT_JSON_FILES` to enable/disable the generation of json files
- New field in configuration file: `OUTPUT_COLUMN_LOAD_PM` to enable/disable PM column mass
- New output: Averaged concentration in vertical layers. Enabled by the option: `OUTPUT_CONCENTRATION_IN_LAYERS`

### Changed
- Flight level are now converted to pressure in order to interpolate concentrations
- Mass flow rate can be read from the 4th column in the external file with emission phases

### Removed
- Compilation with autotools
- Coordinate variable `pm_bin` removed from ensemble output file

### Fixed
- A new format is used for printing values in json files in order to solve some incompatibilities with nvfortran and new versions of gfortran
- Corrected the routine to convert from mass point to corners in 2D. It solved the differences in the ground load deposit found in the res.nc output file depending on the domain decomposition
- Solved bug writing the coordinate variable `pm_bin`
- Updated meteorological scripts to support the new CDS api (unstable yet)

## [8.3] - 2023-11-14

### Added
- The SetDbs task was re-written from scratch and new meteorological datasets were included
- New meteo model: `GEFS`, The Global Ensemble Forecast System (global weather forecast model)
- New meteo model: `IFS`, The ECMWF Integrated Forecasting System (global weather forecast model)
- New meteo model: `CARRA`, The Copernicus Arctic Regional Reanalysis (Arctic regional reanalysis)

### Fixed
- Improved management of memory. Restrictions for reading large meteorological files (>100Gb) were solved
- Set `MEMORY_CHUNK_SIZE` in the configuration file to define the size of memory chunks used to store meteo data timesteps. Must be greater than 1

## [8.2.1] - 2023-02-15

### Fixed
- Removed underflow message in FALL3D task (crop of small values in netCDF)

### Added
- README.md file
- CHANGELOG.md file

### Changed
- Gold badge has been awarded (EOSC-Synergy)

### Removed
- NOTES file

## [8.2.0] - 2022-07-29

### Added
- Concentration at surface output
- Wind rotation option
- RAMS diffusion model

### Changed
- File extensions changed from f90 to F90 (except mod_Configure.f90.in)
- Ported to accelerators using OpenACC (on branch master_gpu)

### Fixed
- Gravity current bug corrected
- Correction of minor bugs

## [8.1.1] - 2021-07-23

### Added
- Tasks PosVal added for validation of results

### Fixed
- Correction of minor bugs

## [8.1.0] - 2020-04-22

### Added
- Ensemble simulation
- Tasks PosEns added to postprocess ensemble runs (probabilistic forecasts)

### Changed
- Wet deposit enabled for radionuclide decay

### Fixed
- Correction in the deposit for wet deposition (it now includes all parts; also in the tps files)

## [8.0.1] - 2020-02-13

### Added
- Complete version; all functionalities in Folch et al. (2019) active
- Input file blocks completed, backwards compatibility will be kept until version 9.x

### Changed
- Output netCDF files follow Climate and Forecast (CF) Metadata Conventions

### Fixed
- Bug in data assimilation profiles
- Bug in sources and tracking points when the domain crosses the meridian day
- Bug in SetDbs for when using single precision

### Removed
- Global attribute `run_start_time`  

## [8.0.0] - 2019-12-20

### Added
- First public release of FALL3D v8.0.0
- Preliminary and incomplete version, some functionalities still not active
- Some changes in the blocks of the input file name.inp will exist in the final version release

