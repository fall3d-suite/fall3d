# FALL3D documentation

## Manual
* For further information, please visit the FALL3D 
[User Guide](https://fall3d-suite.gitlab.io/fall3d/).

## Contributing

* The FALL3D documentation is free and open source. You can find the source code on [GitLab](https://gitlab.com/fall3d-suite/fall3d) and issues and feature requests can be posted on the [GitLab issue tracker](https://gitlab.com/fall3d-suite/fall3d/issues). 

* We encourage the community to fix bugs and add features: if you'd like to contribute, please read the `CONTRIBUTING.md` guide and consider opening a [merge request](https://gitlab.com/fall3d-suite/fall3d/merge_requests).

## References
* Folch, A., Mingari, L., Gutierrez, N., Hanzich, M., Macedonio, G., and Costa, A.: FALL3D-8.0: a computational model for atmospheric transport and deposition of particles, aerosols and radionuclides – Part 1: Model physics and numerics, Geosci. Model Dev., 13, 1431–1458, <https://doi.org/10.5194/gmd-13-1431-2020>, 2020.
* Prata, A. T., Mingari, L., Folch, A., Macedonio, G., and Costa, A.: FALL3D-8.0: a computational model for atmospheric transport and deposition of particles, aerosols and radionuclides – Part 2: Model validation, Geosci. Model Dev., 14, 409–436, <https://doi.org/10.5194/gmd-14-409-2021>, 2021.

## Acknowledgements

* The EuroHPC “Center of Excellence for Exascale in Solid Earth” (**[ChEESE-1P](https://cheese-coe.eu)** and **[ChEESE-2P](https://www.cheese2.eu)**, 2018-2026, GA No
823844 and 101093038 respectively).
* Horizon Europe project “A Digital Twin for GEOphysical extremes” (**[DT-GEO](https://www.dtgeo.eu)**, GA No 101058129, Sep 2022 to Aug 2025).



## License

* FALL3D and this User Guide are released under the [GNU General Public License (GPL)](http://www.gnu.org/copyleft/gpl.html).
