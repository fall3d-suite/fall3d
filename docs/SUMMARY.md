
# Quick start

- [FALL3D overview](./chapters/overview.md)
- [Installation requirements](./chapters/requirements.md)
- [Installing FALL3D](./chapters/installation.md)
  - [Installing on EuroHPC systems](./chapters/installationHPC.md)
- [Running FALL3D](./chapters/running.md)
  - [Running on EuroHPC systems](./chapters/runningHPC.md)
  - [Running the Example case](./chapters/example.md)
- [Running FALL3D using Docker](./chapters/runningDocker.md)

# Reference Guide

- [Model tasks](./chapters/tasks.md)
    - [Task `SetTGSD`](./tasks/settgsd.md)
    - [Task `SetDbs`](./tasks/setdbs.md)
    - [Task `SetSrc`](./tasks/setsrc.md)
    - [Task `FALL3D`](./tasks/fall3d.md)
    - [Task `SetEns`](./tasks/setens.md)
    - [Task `PosEns`](./tasks/posens.md)
    - [Task `PosVal`](./tasks/posval.md)
- [Meteorological data](./chapters/meteorological.md)
    - [ERA5](./chapters/era5.md)
    - [GFS](./chapters/gfs.md)
    - [GEFS](./chapters/gefs.md)
    - [CARRA](./chapters/carra.md)
    - [WRF-ARW](./chapters/wrf.md)
- [Namelist file](./chapters/namelist.md)
    - [Block `TIME_UTC`](./namelist/TIME_UTC.md)
    - [Block `INSERTION_DATA`](./namelist/INSERTION_DATA.md)
    - [Block `METEO_DATA`](./namelist/METEO_DATA.md)
    - [Block `GRID`](./namelist/GRID.md)
    - [Block `SPECIES`](./namelist/SPECIES.md)
    - [Block `SPECIES_TGSD`](./namelist/SPECIES_TGSD.md)
    - [Block `PARTICLE_AGGREGATION`](./namelist/PARTICLE_AGGREGATION.md)
    - [Block `SOURCE`](./namelist/SOURCE.md)
    - [Block `ENSEMBLE`](./namelist/ENSEMBLE.md)
    - [Block `ENSEMBLE_POST`](./namelist/ENSEMBLE_POST.md)
    - [Block `MODEL_PHYSICS`](./namelist/MODEL_PHYSICS.md)
    - [Block `MODEL_OUTPUT`](./namelist/MODEL_OUTPUT.md)
    - [Block `MODEL_VALIDATION`](./namelist/MODEL_VALIDATION.md)

# Hands-on course 

- [Introduction](./hands-on/introduction.md)
    - [Meteorological data](./hands-on/session1.md)
    - [Deterministic simulation](./hands-on/session2.md)
    - [Ensemble simulations](./hands-on/session3.md)

# Other 

- [References](./chapters/references.md)
- [Acknowledgments](./chapters/acknowledgments.md)
