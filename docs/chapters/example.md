# Example test case
The FALL3D distribution includes multiple test cases (benchmark suite) that can
be downloaded from the [central repository](https://gitlab.com/fall3d-suite).
These cases are used to check the model configuration and installation rather
than to perform accurate and realistic simulations for event reconstruction.

Let's run an example case built from the template inside the folder `Templates`.
First, assume the following directory structure:

* `$WORKPATH`: path where the example case will be run
* `$BINPATH`: path with the FALL3D executable `Fall3d.x`
* `$FALL3DPATH`: path with the FALL3D distribution

Create the run folder and copy the template there:
```console
> mkdir $WORKPATH
> cd $WORKPATH
> cp $FALL3DPATH/Templates/template.inp Example.inp
```

Two additional input files are required to run this example. The meteorological
database can be downloaded from the testsuite repository using the `wget` 
command:
```console
> wget https://gitlab.geo3bcn.csic.es/fall3d/test-suite/example/-/raw/d1a704bb0551ff50033f0df3300d70166a7eccf5/Example.wrf.nc
```

In order to properly read this meteorological file, FALL3D needs a dictionary to
decode the variable names in the input file and translate them into the
internal variable representation. Samples of different dictionaries are found
in the folder `Other/Meteo/Tables` of the distribution. For this specific 
example case, you will need the `WRF.tb` dictionary:

```console
> cp $FALL3DPATH/Other/Meteo/Tables/WRF.tbl .
```

The program is now ready to be executed.
You can run FALL3D in serial mode using the model configuration defined in 
the input file `Example.inp` with the following command:
```console
> $BINPATH/Fall3d.x Example.inp All
```

> **Notes:**
> * Depending on your installation, the executable filename `Fall3d.x` could be different
> * More input files for testing can be downloaded from the
>   [Benchmark suite repository](https://gitlab.com/fall3d-suite/test-suite)

## Checking the results
If the run was successful, you should obtain a log file `Example.Fall3d.log`
with the end message:
```
  Number of warnings : 0
  Number of errors   : 0
  Task FALL3D        : ends NORMALLY
```
The results of the simulation are stored in the output netCDF file
`Example.res.nc`. For a quick view of the output file, you can use the `ncview`
tool (available [here](http://meteora.ucsd.edu/~pierce/ncview_home_page.html))
to graphically display netCDF files:
```console
> ncview Example.res.nc
```
