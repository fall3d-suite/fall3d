# GFS: Getting data

The Global Forecast System (GFS) is a National Centers for Environmental Prediction (NCEP) weather forecast model that generates data for dozens of atmospheric and land-soil variables, including temperatures, winds, precipitation, soil moisture, and atmospheric ozone concentration. GFS is a global model with a base horizontal resolution of 18 miles (28 kilometers) between grid points. Temporal resolution covers analysis and forecasts out to 16 days. Horizontal resolution drops to 44 miles (70 kilometers) between grid points for forecasts between one week and two weeks (see more details [here](https://www.ncei.noaa.gov/products/weather-climate-models/global-forecast)). 

Data from the last 10 days can be obtained from the [NOAA Operational Model Archive and Distribution System](https://nomads.ncep.noaa.gov) (NOMADS). Alternatively, we provide some scripts to automate the downloading process, select the variables required by FALL3D, domain cropping, etc. 

## GFS data in GRIB2 format

A script is provided to download the NCEP operational Global Forecast System (GFS) forecasts in GRIB2 format (conversion required):

* `gfs.py`: Download GFS forecasts in GRIB2 format. This option needs further conversion to netCDF format before data is ingested by FALL3D.
  <details>
  <summary>Command line options. Click to expand!</summary>
  <p>

    ```
    usage: gfs.py [-h] [-d start_date] [-x lonmin lonmax] [-y latmin latmax] [-t tmin tmax] [-r resolution] [-c cycle] [-s step] [-b block] [-i file] [-v]

    Download the NCEP operational Global Forecast System (GFS) analysis and forecast data required by the FALL3D model

    options:
      -h, --help            show this help message and exit
      -d start_date, --date start_date
                            Initial date in format YYYYMMDD
      -x lonmin lonmax, --lon lonmin lonmax
                            Longitude range
      -y latmin latmax, --lat latmin latmax
                            Latitude range
      -t tmin tmax, --time tmin tmax
                            Forecast time range (h)
      -r resolution, --res resolution
                            Spatial resolution (deg)
      -c cycle, --cycle cycle
                            Cycle
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details>

## GFS data in netCDF format

FALL3D requires input meteorological data in netCDF format. Consequently, GFS data must be concatenated and converted from GRIB2 to obtain a single netCDF file. To this purpose, the grib utility `wgrib2` can be used (more information [here](https://www.cpc.ncep.noaa.gov/products/wesley/wgrib2/)). As an example, a bash script is included in the FALL3D distribution to perform the conversion: `Other/Meteo/Utils/grib2nc.sh`. Just edit the file header according to your GFS files:

```
########## Edit header ##########
WGRIBEXE=wgrib2
OUTPUTFILE=output.nc
TABLEFILE=grib_tables/gfs_0p25.levels
TMIN=0
TMAX=12
STEP=6
CYCLE=12
DATE=20230404
GRIBPATH=~/fall3d/fall3dutil/tests
GRIBFILE (){
    fname=${GRIBPATH}/gfs.t${CYCLE}z.pgrb2.0p25.f${HOUR}
    echo ${fname}
}
#################################
```

**Important note:**

* The variable `TABLEFILE` specifies the list of vertical levels and depends on the resolution of your GFS data. In the folder `grib_tables` you'll find the tables for resolutions of 0.25° (`gfs_0p25.levels`), 0.50° (`gfs_0p50.levels`) and 1.00° (`gfs_1p00.levels`). Define `TABLEFILE` accordingly.
