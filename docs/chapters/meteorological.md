# Meteorological data

In order to run FALL3D, you need to provide meteorological data as an input. Multiple datasets are supported, including global model forecasts (GFS), global analyses and re-analyses (GDAS, ERA5), and mesoscale models. 

## List of supported datasets

This is the list of meteorological models available:
  - `WRF-ARW`: The Advanced Research WRF (ARW) (mesoscale model)
  - `GFS`: The Global Forecast System (global weather forecast model)
  - `GEFS`: The Global Ensemble Forecast System (global weather forecast model)
  - `ERA5`: The ERA5 ECMWF reanalysis in pressure levels (global climate and weather renalysis)
  - `ERA5ML`: The ERA5 ECMWF reanalysis in model levels (global climate and weather renalysis)
  - `IFS`: The ECMWF Integrated Forecasting System (global weather forecast model)
  - `CARRA`: The Copernicus Arctic Regional Reanalysis (Arctic regional reanalysis )

<div class="table-wrapper">
<table>
  <thead><tr>
    <th><b>ID</b></th>
    <th><b>Map Projection</b></th>
    <th><b>Resolution</b></th>
    <th><b>Time Resolution</b></th>
    <th><b>Vertical Coordinates</b></th>
    <th><b>Vertical levels</b></th>
    <th><b>Period</b></th>
    <th><b>Format</b></th>
  </tr></thead>
  <tr>
    <td colspan="8" align="center"><u><i>Global model forecasts</i></u></td>
  </tr>
  <tr>
    <td rowspan="3">GFS (NCEP)</td>
    <td rowspan="3">Regular lat-lon</td>
    <td>0.25º</td>
    <td>1 h</td>
    <td rowspan="3">Isobaric</td>
    <td>41</td>
    <td>+384 h</td>
    <td rowspan="3">GRIB2</td>
  </tr>
  <tr>
    <td>0.5º</td>
    <td>3 h</td>
    <td>57</td>
    <td>+384 h</td>
  </tr>
  <tr>
    <td>1.0º</td>
    <td>3 h</td>
    <td>41</td>
    <td>+384 h</td>
  </tr>
  <tr>
    <td>GEFS (NCEP)</td>
    <td>Regular lat-lon</td>
    <td>0.5º</td>
    <td>3 h</td>
    <td>Isobaric</td>
    <td>31</td>
    <td>+840 h</td>
    <td>GRIB2</td>
  </tr>
  <tr>
    <td>IFS (ECMWF)</td>
    <td>Regular lat-lon</td>
    <td>0.125º</td>
    <td>1 h</td>
    <td>Hybrid</td>
    <td>137</td>
    <td>10 days</td>
    <td>netCDF or GRIB2</td>
  </tr>
  <tr>
    <td colspan="8" align="center"><u><i>Global model final analyses and re-analyses</i></u></td>
  </tr>
  <tr>
    <td>ERA5</td>
    <td>Regular lat-lon</td>
    <td>0.25º</td>
    <td>1 h</td>
    <td>Isobaric</td>
    <td>37</td>
    <td>1940-present</td>
    <td>netCDF or GRIB1</td>
  </tr>
  <tr>
    <td>ERA5ML</td>
    <td>Regular lat-lon</td>
    <td>0.25º</td>
    <td>1 h</td>
    <td>Hybrid</td>
    <td>137</td>
    <td>1940-present</td>
    <td>netCDF or GRIB2</td>
  </tr>
  <tr>
    <td>CARRA</td>
    <td>Lambert Conformal</td>
    <td>2.5 km</td>
    <td>3 h</td>
    <td>Isobaric</td>
    <td>12</td>
    <td>1990-present</td>
    <td>GRIB2</td>
  </tr>
  <tr>
    <td colspan="8" align="center"><u><i>Mesoscale models</i></u></td>
  </tr>
  <tr>
    <td rowspan="4">WRF-ARW</td>
    <td>Regular lat-lon</td>
    <td rowspan="4">user-defined</td>
    <td rowspan="4">user-defined</td>
    <td rowspan="4">Terrain Following<br>or<br>Hybrid</td>
    <td rowspan="4">user-defined</td>
    <td rowspan="4">user-defined</td>
    <td rowspan="4">netCDF</td>
  </tr>
  <tr>
    <td>Lambert Conformal</td>
  </tr>
  <tr>
    <td>Mercator</td>
  </tr>
  <tr>
    <td>Polar Stereographic</td>
  </tr>
</table>
</div>

## Utilities and dependencies

The FALL3D distribution package comes with a set of Python utilities for downloading and pre-processing global meteorological fields required by the model. These python scripts allow also downloading meteorological fields for a particular region and date range. The following scripts are included in the folder `Other/Meteo/Utils`:

| Script       | Comments  |
| :----------  | :-------- |
| `era5_pl.py` | Crops and downloads ERA5 data on pressure levels (37 vertical levels) from the Copernicus Climate Data Store (CDS) infrastructure |
| `era5_ml.py` | Crops and downloads ERA5 data on model levels (137 vertical levels) from the Copernicus Climate Data Store (CDS) infrastructure |
| `era5_sfc.py` | Crops and downloads ERA5 data at surface from the Copernicus Climate Data Store (CDS) infrastructure |
| `carra_pl.py` | Downloads CARRA data on pressure levels (12 vertical levels) from the Copernicus Climate Data Store (CDS) infrastructure |
| `carra_sfc.py` | Downloads CARRA data at surface from the Copernicus Climate Data Store (CDS) infrastructure |
| `gfs.py` | Crops and downloads National Centers for Environmental Prediction (NCEP) GFS forecasts in GRIB2 native format (pre-processing required) format |
| `gefs.py` | Crops and downloads National Centers for Environmental Prediction (NCEP) GEFS forecasts in GRIB2 native format (pre-processing required) format |

The Python scripts above require the `fall3dutil` package, which can be installed on Linux and MacOS systems from the Python Package Index (PyPI) using pip. To this purpose, you can use a Python virtual environment to avoid conflicts. To create a virtual environment on a typical Linux system, use the basic venv command:

```
python3 -m venv fall3d_env
source fall3d_env/bin/activate
```

This will create a new virtual environment in the `fall3d_env` subdirectory, and configure the current shell to use it as the default python environment. For more information click [here](https://packaging.python.org/tutorials/installing-packages/#creating-virtual-environments). Once the virtual environment is activated, you can install the last version of the `fall3dutil` package in the `fall3d_env` using:

```
pip install fall3dutil
```

If you decide not to work in an environment, then you can execute this command
```
python3 -m pip install --user fall3dutil
