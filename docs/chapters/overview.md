# FALL3D in a nutshell
**FALL3D** is an open-source off-line Eulerian model for atmospheric
passive transport and deposition. The model, originally developed for
inert volcanic particles (tephra), has a track record of 50+ on different
model applications and code validation, as well as an ever-growing community
of users worldwide, including academia, private, research, and several
institutions tasked with operational forecast of volcanic ash clouds. 

The model solves the so-called Advection-Diffusion-Sedimentation (ADS) equation: 
\\[
 \frac{\partial c}{\partial t} + \nabla \vec{F} + \nabla \vec{G} + \nabla \vec{H} = S - I 
\\]
where \\(\vec{F}= c~\vec{u}\\) is the advective flux, \\(\vec{G}= c~\vec{u_s}\\)
is the sedimentation flux, \\(\vec{H}=-\mathbb{K} \nabla c\\) is the diffusive flux,
and \\(S\\) and \\(I\\) are the source and sink terms respectively.
In the expressions above, \\(t\\) denotes time, \\(c\\) is concentration, \\(\vec{u}\\)
is the fluid velocity vector (wind velocity), \\(\vec{u_s}\\) is the terminal
settling velocity of the substance, and \\(\mathbb{K}\\) is the diffusion tensor.

# Changes from version 8 onwards
In FALL3D v8.x, the ADS equation was extended to handle passive transport of other
substances different from tephra. In a general sense, substances in FALL3D are grouped
in 3 broad categories: 
* Category **particles**: includes any inert substance characterized by a
  sedimentation velocity.
* Category **aerosol**: refers to substances potentially non-inert
  (i.e. with chemical or phase changes mechanisms) and having a negligible
  sedimentation velocity. 
* Category **radionuclides**: refers to isotope particles subjected to
  radioactive decay.

Each category admits, in turn, different sub-categories or **species**,
defined internally as structures of data that inherit the parent category
properties. For example, particles can be subdivided into tephra or mineral
dust; aerosol species can include H<sub>2</sub>O, SO<sub>2</sub>, etc.; and
radionuclides can include several isotope species. Finally, each sub-category
of species is tagged with an attribute name that is used for descriptive
purposes only. 

![Category of species](categories.png)

# Changes from version 9 onwards
In FALL3D v9.x the CPU and GPU versions have been unified into a single source code. The compiling system relies on meson or cMake (autotools has been deprecated) and different binaries can be generated for different architectures and flavours.
