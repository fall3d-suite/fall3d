# Model tasks

In **FALL3D**, the former pre-process auxiliary programs (`SetTgsd`, `SetDbs` and `SetSrc`) have been parallelized and embedded within the main code, so that a single executable file exists for all the pre-process steps and execution workflow. These formerly independent programs can still be run individually invoked as model tasks (specified by a program call argument) or, alternatively, concatenated as a single model execution. In the first case, pre-process tasks generate output files that are later on furnished as inputs to the model (or to subsequent pre-process tasks). In contrast, the second option does not require of intermediate file writing/reading and, therefore, saves disk space and overall computing time. 

The next figure shows a diagram of the FALL3D workflow, the data transfer between different tasks and the involved I/O files:

![FAILED workflow](diagram.svg)

## List of model tasks

This is the list of available tasks:

* Task `SetTgsd` generates particle Total Grain Size Distributions (TGSD) for species of category particles or radionuclides
* Task `SetDbs` interpolates (for a defined time slab) meteorological variables from the meteorological model grid to the FAL3D computational domain
* Task `SetSrc` generates emission source terms for the different species. It can also perform a-priori particle aggregation and a TGSD cut-off
* Task `FALL3D` runs the FALL3D solver
* Task `ALL` runs all previous tasks concatenated
* Task `PosEns` merges and post-processes outputs from single ensemble members. Only for ensemble runs
* Task `PosVal` validates single or ensemble-based results

All the tasks require a common configuration file as an input, i.e. the [namelist file](#the-namelist-file) `name.inp`.
In addition, there are different input and output files associated with each task, as is summarized in the next table:

| Task     | Namelist file required | Mandatory<br>input files      | Optional input files | Output files                  |
| ------   | :------: | ------                                      | ------               | ------                        |
| `SetTgsd`| Yes      |                                             | Custom distribution  | `name.specie.tgsd`            |
| `SetDbs` | Yes      | Meteorological file in netCDF format        | Dictionary file      | `name.dbs.nc`, `name.dbs.pro` |
| `SetSrc` | Yes      |`name.specie.tgsd`, `name.dbs.pro`           |                      | `name.specie.grn`, `name.src` |
| `FALL3D` | Yes      |`name.src`, `name.specie.grn`, `name.dbs.nc` | `name.pts`           | `name.res.nc`, `name.rst.nc`  |
| `All`    | Yes      | Meteorological file in netCDF format        | `name.pts`           | `name.res.nc`, `name.rst.nc`  |
| `PosEns` | Yes      | List of `name.res.nc` files                 |                      | `name.ens.nc`                 |
| `PosVal` | Yes      |`name.res.nc` or `name.ens.nc`               |  see section [Task PosVal](../tasks/posval.md) |     |

The task `SetEns` is not explicitly executed by the user, but is automatically invoked when ensemble runs are performed in order to set up the ensemble.
The ensemble run option is enabled whenever FALL3D is executed with the argument `-nens`, which allows one to define the size of the ensemble.

> **Notes:**
> * The model tasks `SetEns`, `PosEns` and `PosVal` have been added from version 8.1. 

## The namelist file 
The namelist file `name.inp` is an input configuration file in ASCII format required by all tasks. 
It consists of a set of blocks defining physical parameterisations, time ranges, model domain, emission source, numerical schemes, etc. 
Each task reads only the necessary blocks generating self-consistent input files. 
Parameters within a block are listed one per record, in arbitrary order, and optionally can be followed by one (or more) blank space and a comment. 
Comments start with the exclamation mark symbol "!".
Real numbers can be expressed using the FORTRAN notation (e.g. 12E7). 

See section [Namelist file](namelist.md) for a full description of the file structure.

## The LOG file
In addition, each task `TASK` produces a log file `name.TASK.log` reporting relevant information and potential errors or warnings.
