# Running FALL3D
Model execution requires a unique namelist input file `*.inp`
to define the computation domain, time range, physics options
and other model parameters. Start with the default input file
`Example.inp` in the `Example` directory and edit it for your
case.

FALL3D can be executed in serial mode by typing:
```console
> ./Fall3d.x TASK namelist.inp
```
in order to perform the task `TASK`. Here, `Fall3d.x` refers to
the executable file in the `bin` folder generated during the
installation and `namelist.inp` is your namelist input file
(see section [Namelist file](./namelist.md) for a detailed
description of the structure of this file). The available tasks
are:
* `SetTgsd`
* `SetDbs`
* `SetSrc`
* `FALL3D`
* `All`
* `PosEns`
* `PosVal`

For more details about the different model tasks, see the section
[Model tasks](./tasks.md).

If FALL3D was compiled with the MPI option, run the model in
parallel mode by typing:
```console
> mpirun -n np Fall3d.x Task name.inp [NPX] [NPY] [NPZ] [-nens SIZE]
```
where:
* `np`: number of processors
* `NPX`, `NPY`, `NPZ`: number of processors along the dimensions
   x, y, and z in the domain decomposition (default=1) 
* `SIZE`: ensemble size (default=1)

> **Notes:**
> * The execution command for MPI runs may be different,
>   depending on the MPI installations (e.g., `mpirun` or `mpiexec`)
> * The total number of processors is
>   `NPX` \\(\times\\) `NPY` \\(\times\\) `NPY` \\(\times\\) `SIZE`
>   and should be equivalent to the argument `np`
> * Using **binary compiled with OpenACC**, the total MPI ranks (option `np`) **must be equal** to the total **available GPUs** number to obtain a good performance.

A log file `name.Task.log` will be generated to track the task execution
and to report eventual warnings and errors. If the job is successful,
the last line printed in the log file should be
```
  Task FALL3D        : ends NORMALLY
```

## Ensemble runs
From version release 8.1, ensemble simulations can be performed with FALL3D 
as a single model task. Ensemble-based approaches can give a deterministic
product based on some combination of the single ensemble members (e.g. the
ensemble mean) and, as opposed to single deterministic runs, attach to it
an objective quantification of the forecast uncertainty. On the other hand,
ensemble runs can also furnish probabilistic products based on the fraction
of ensemble members that verify a certain (threshold) condition, e.g. the
probability of cloud being detected by satellite-based instrumentation, the
probability that the cloud mass concentration compromises the safety of air
navigation, the probability of particle fallout or of aerosol concentration
at surface to exceed regulatory values for impacts on infrastructures or on
air quality, etc.

In order to perform a ensemble runs, FALL3D must be executed with the optional
argument `-nens`, defining the ensemble size, set to a value greater than one.
For example, the following command will generate a 24-member ensemble and 
perform the `FALL3D` task for each ensemble member:
```console
> mpirun -np 24 Fall3d.x FALL3D name.inp -nens 24
```
A new folder structure will be created and the results for each ensemble
member will be organized in different sub-folders.

> **Notes:**
> * Ensemble simulations are only supported in parallel mode
