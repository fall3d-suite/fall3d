# CARRA: Getting data

The goal of the Copernicus Arctic Regional Reanalysis (CARRA) system was to produce the first regional atmospheric reanalysis targeted for European parts of the Arctic areas. The reanalysis covers the period from September 1990 (>30 years). The CARRA reanalysis dataset is produced at 2.5 km horizontal mesh. The reanalysis data cover two domains in the European sector of the Arctic shown below (see more information [here](https://climate.copernicus.eu/copernicus-arctic-regional-reanalysis-service)):

![domains](https://climate.copernicus.eu/sites/default/files/inline-images/07_0.png)

Data can be obtained from the [Climate Data Store](https://cds.climate.copernicus.eu). Alternatively, we provide some scripts to automate the downloading process, select the variables required by FALL3D, domain cropping, etc. 

**Important note:**
In order to use the Climate Data Store (CDS) API, you need to have an account and install a key as explained [here](https://cds.climate.copernicus.eu/api-how-to) (see Section _Install the CDS API key_).

## Surface data

You need to obtain surface and upper level data in different requests. The script `carra_sfc.py` can be used to retrieve surface data.

* `carra_sfc.py`: Download CARRA data at surface level via the Climate Data Store (CDS) infrastructure.
  <details>
  <summary>Command line options. Click to expand</summary>
  <p>

    ```
    usage: carra_sfc.py [-h] [-d start_date end_date] [-s step] [-b block] [-i file] [-v]

    Download CARRA data (single level) required by FALL3D model.

    options:
      -h, --help            show this help message and exit
      -d start_date end_date, --date start_date end_date
                            Date range in format YYYYMMDD
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details> 

## Upper level data

In addition to surface data, you need to obtain upper level data as well. 

* `carra_pl.py`: Download CARRA data required on pressure levels (12 vertical levels) via the Climate Data Store (CDS) 
  infrastructure.
  <details>
  <summary>Command line options. Click to expand</summary>
  <p>

    ```
    usage: carra_pl.py [-h] [-d start_date end_date] [-s step] [-b block] [-i file] [-v]

    Download CARRA data (pressure levels) required by FALL3D model.

    options:
      -h, --help            show this help message and exit
      -d start_date end_date, --date start_date end_date
                            Date range in format YYYYMMDD
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details> 

## Merging data
  
Once downloaded, CARRA data on upper levels (pressure levels) and at surface in GRIB format has to be converted and merged in order to generate a single netCDF file for the FALL3D model. Unfortunately, the CDS API does not allow domain subsets to be retrieved and, typically, very large files need to be downloaded. This can lead to memory conflicts during the format conversion or merging process. This simple python scripts can be used in these cases:

```python
import xarray as xr
from dask.diagnostics import ProgressBar
import cfgrib

### Parameters ###
fname_out    = 'merged_meteo.nc'
fname_in_sfc = 'surface.grib'
fname_in_pl  = 'pressure.grib'
##################

ds_list  = cfgrib.open_datasets(fname_in_pl)
ds_list += cfgrib.open_datasets(fname_in_sfc)

keys2remove = ['surface','heightAboveGround']

for i,ds in enumerate(ds_list):
    ds_list[i] = ds.drop_vars(keys2remove,errors='ignore')
    if 'sr' in ds:
        print("******Warn******* Correcting coordinates...")
        ds_list[i]['latitude']  = ds_list[0].latitude
        ds_list[i]['longitude'] = ds_list[0].longitude

print("merging....")
ds = xr.merge(ds_list).chunk(chunks={"time": 1})

print("saving...")
delayed_obj = ds.to_netcdf(fname_out, compute=False)

with ProgressBar():
    results = delayed_obj.compute()
```

to generate the `merged_meteo.nc` file that will be ingested into FALL3D.

**Important notes:**

* This process allows access to data of the CARRA-west domain. Access to CARRA-east domain has not been implemented yet.
