# GEFS: Getting data

The Global Ensemble Forecast System (GEFS) is a weather model created by the National Centers for Environmental Prediction (NCEP) that generates 31 separate forecasts (ensemble members) to address underlying uncertainties in the input data such limited coverage, instruments or observing systems biases, and the limitations of the model itself. GEFS quantifies these uncertainties by generating multiple forecasts, which in turn produce a range of potential outcomes based on differences or perturbations applied to the data after it has been incorporated into the model. Each forecast compensates for a different set of uncertainties (see more details [here](https://www.ncei.noaa.gov/products/weather-climate-models/global-ensemble-forecast)).

Data from the last 4 days can be obtained from the [NOAA Operational Model Archive and Distribution System](https://nomads.ncep.noaa.gov) (NOMADS). Alternatively, we provide some scripts to automate the downloading process, select the variables required by FALL3D, domain cropping, etc. 

## GEFS data in GRIB2 format

A script is provided to download the NCEP Global Ensemble Forecast System (GEFS) forecasts in GRIB2 format (conversion required):

* `gefs.py`: Download GEFS forecasts in GRIB2 format. This option needs further conversion to netCDF format before data is ingested by FALL3D.
  <details>
  <summary>Command line options. Click to expand!</summary>
  <p>

    ```
    usage: gefs.py [-h] [-d start_date] [-x lonmin lonmax] [-y latmin latmax] [-t tmin tmax] [-e ensmin ensmax] [-r resolution] [-c cycle] [-s step] [-b block]
                   [-i file] [-v]

    Download data from the atmospheric component of NCEP operational global ensemble modeling suite, Global Ensemble Forecast System (GEFS), required by the
    FALL3D model

    options:
      -h, --help            show this help message and exit
      -d start_date, --date start_date
                            Initial date in format YYYYMMDD
      -x lonmin lonmax, --lon lonmin lonmax
                            Longitude range
      -y latmin latmax, --lat latmin latmax
                            Latitude range
      -t tmin tmax, --time tmin tmax
                            Forecast time range (h)
      -e ensmin ensmax, --ens ensmin ensmax
                            Ensemble member range
      -r resolution, --res resolution
                            Spatial resolution (deg)
      -c cycle, --cycle cycle
                            Cycle
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details>

## GEFS data in netCDF format

FALL3D requires input meteorological data in netCDF format. Consequently, GEFS data must be concatenated and converted from GRIB2 to obtain a set of netCDF files. To this purpose, the grib utility `wgrib2` can be used (more information [here](https://www.cpc.ncep.noaa.gov/products/wesley/wgrib2/)). As an example, a bash script is included in the FALL3D distribution to perform the conversion: `Other/Meteo/Utils/grib2nc-ens.sh`. Just edit the file header according to your GEFS files:

```
########## Edit header ########## 
WGRIBEXE=wgrib2
OUTPUTFILE=output.nc
TABLEFILE=grib_tables/gefs_0p50.levels
TMIN=0
TMAX=48
STEP=3
CYCLE=12
DATE=20230404
GRIBPATH=./
GRIBFILE (){
    fnameA=${GRIBPATH}/gep04.t${CYCLE}z.pgrb2a.0p50.f${HOUR}
    fnameB=${GRIBPATH}/gep04.t${CYCLE}z.pgrb2b.0p50.f${HOUR}
}
################################# 
```
