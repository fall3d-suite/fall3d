# References

## 2024

* Costa, A., Mingari, L., Smith, V.C., Macedonio, G., McLean, D., Folch, A., Lee, J., Yun, S.-H., Eruption plumes extended more than 30 km in altitude in both phases of the Millennium eruption of Paektu (Changbaishan) volcano, Communications Earth & Environment, https://doi.org/10.1038/s43247-023-01162-0, 2024.

## 2023

* Mingari, L., Costa, A., Macedonio, G., and Folch, A.: Reconstructing tephra fall deposits via ensemble-based data assimilation techniques, Geosci. Model Dev., 16, 3459–3478, https://doi.org/10.5194/gmd-16-3459-2023, 2023.

* Folch, A., Abril, C., Afanasiev, M., et al., The EU Center of Excellence for Exascale in Solid Earth (ChEESE): Implementation, results, and roadmap for the second phase, Future Generation Computer Systems, 146, 47-61, https://doi.org/10.1016/j.future.2023.04.006, 2023.


## 2022
* Mingari, L., A. Folch, A. T. Prata, F. Pardini, G. Macedonio, and A. Costa, Data Assimilation of Volcanic Aerosol Observations Using FALL3D+PDAF, Atmos. Chem. Phys. 22 (3): 1773–92. https://doi.org/10.5194/acp-22-1773-2022, 2022.

* Folch, Arnau, Leonardo Mingari, and Andrew T. Prata, Ensemble-Based Forecast of Volcanic Clouds Using FALL3D-8.1, Front. Earth Sci. 9. https://doi.org/10.3389/feart.2021.741841, 2022.

* Titos, M., B. Martı́nez Montesinos, S. Barsotti, L. Sandri, A. Folch, L. Mingari, G. Macedonio, and A. Costa, Long-Term Hazard Assessment of Explosive Eruptions at Jan Mayen (Norway) and Implications for Air Traffic in the North Atlantic, Nat. Hazards Earth Syst. Sci. 22 (1): 139–63. https://doi.org/10.5194/nhess-22-139-2022, 2022.

## 2021
* Prata, A., Mingari, L., Folch, A., Costa, A., Macedonio, G., FALL3D-8.0: a computational model for atmospheric transport and deposition of particles and aerosols. Part II: model validation, Geoscientific Model Development dicussions, 14(1):409-436, https://doi.org/10.5194/gmd-14-409-2021, 2021.

## 2020
* Mingari, L., Folch, A., Dominguez, L., Bonadonna, C., Volcanic ash resuspension in Patagonia: numerical simulations and observations, Atmosphere, 11, 977; https://doi.org/10.3390/atmos11090977, 2020.
* Folch, A., Mingari, L., Gutierrez, N., Hanzich, M., Costa, A., Macedonio, G., FALL3D-8.0: a computational model for atmospheric transport and deposition of particles and aerosols. Part I: model physics and numerics, geoscientific model development, https://doi.org/10.5194/gmd-2019-311, 2020.
* Osores, S., Ruiz, J., Folch, A., Collini, E., Volcanic ash forecast using ensemble-based data assimilation: the Ensemble Transform Kalman Filter coupled with FALL3D-7.2 model (ETKF-FALL3D, version 1.0), Geoscientific Model Development, https://doi.org/10.5194/gmd-2019-95, 2020.

## 2019
* Poulidis, A., Takemi, T., and Iguchi, M.: Experimental High-Resolution Forecasting of Volcanic Ash Hazard at Sakurajima, Japan, Journal of Disaster Research, 14, 786–797, https://doi.org/10.20965/jdr.2019.p0786, 2019.
* Vázquez, R., Bonasia, R., Folch, A., Arce, J.L:, Macías, L., Tephra fallout hazard assessment at Tacaná volcano (Mexico), Journal of South American Earth Sciences, v.91, https://doi.org/10.1016/j.jsames.2019.02.013, 2019.

## 2018
* Poret, M. and Corradini, S. and Merucci, L. and Costa, A. and Andronico, D. and Montopoli, M. and Vulpiani, G. and Freret-Lorgeril, V., Reconstructing volcanic plume evolution integrating satellite and ground-based data: application to the 23 November 2013 Etna eruption, Atmospheric Chemistry and Physics, 18, 4695-4714, https://www.atmos-chem-phys.net/18/4695/2018, 2018.

## 2017
* Geyer, A., Martí, A., Giralt, S., Folch, A., Potential ash impact from Antarctic volcanoes: Insights from  Deception Island’s most recent eruption, Nature Scientific Reports, 7, https://doi.org/10.1038/s41598-017-16630-9, 2017.
* Mingari, L. A., Collini, E. A., Folch, A., Báez, W., Bustos, E., Osores, M. S., Reckziegel, F., Alexander, P., and
Viramonte, J. G.: Numerical simulations of windblown dust over complex terrain: the Fiambalá Basin episode in
June 2015, Atmospheric Chemistry and Physics, 17, 6759–6778, https://doi.org/10.5194/acp-17-6759-2017, 2017.
* Poret, M., Costa, A., Folch, A., and Martí, A.: Modelling tephra dispersal and ash aggregation: The 26th April
1979 eruption, La Soufriere St. Vincent, Journal of Volcanology and Geothermal Research, 347, 207 – 220,
https://doi.org/https://doi.org/10.1016/j.jvolgeores.2017.09.012, 2017.

## 2016
* Costa, A., Pioli, L., and Bonadonna, C.: Assessing tephra total grain-size distribution: Insights from field data analysis, Earth and Planetary Science Letters, 443, 90–107, https://www.sciencedirect.com/science/article/pii/S0012821X16300577, 2016.
* de la Cruz, R., Folch, A., Farre, P., Cabezas, J., Navarro, N., and Cela, J.: Optimization of atmospheric transport models on HPC platforms, Computers and Geosciences, 97, 30–39, https://doi.org/10.1016/j.cageo.2016.08.019, 2016.
* Martí, A., Folch, A., Costa, A., and Engwell, S.: Reconstructing the phases of the Campanian Ignimbrite super-eruption, Nature Scientific Reports, 6, https://doi.org/10.1038/srep21220, 2016.
* Parra, R., Bernard, B., Narvaez, D., Le Pennec, J.-L., Hasselle, N., and Folch, A.: Eruption Source Parameters
for forecasting ash dispersion and deposition from vulcanian eruptions at Tungurahua volcano: Insights from field data from the July 2013 eruption, Journal of Volcanology and Geothermal Research, 309, 1 – 13, https://doi.org/10.1016/j.jvolgeores.2015.11.001, 2016.
* Sandri, L., Costa, A., Selva, J., Tonini, R., Macedonio, G., Folch, A., and Sulpizio, R.: Beyond eruptive scenarios: assessing tephra fallout hazard from Neapolitan volcanoes, Scientific Reports, 6, https://doi.org/10.1038/srep24271, 2016.
* Reckziegel, F., Bustos, E., Mingari, L., Baez, W., Villarosa, G., Folch, A., Collini, E., Viramonte, J., Romero. J.E., Osores, S., Forecasting volcanic ash dispersal and coeval resuspension during the April-May 2015 Calbuco eruption, Journal of Volcanology and Geothermal Research, https://doi.org/10.1016/j.jvolgeores.2016.04.033, 2016.

## 2015
* Watt, S., J. S. Gilbert, A. Folch, J.C. Phillips, X-M. Cai, Enhanced tephra fallout driven by topographically  induced atmospheric turbulence, Bulletin of Volcanology, https://doi.org/10.1007/s00445-015-0927-x, 2015.

## 2014
* Biass, S., Scaini, C., Bonadonna, C., Folch, A., Smith, K., and Höskuldsson, A.: A multi-scale risk assessment for tephra fallout and airborne concentration from multiple Icelandic volcanoes. Part 1: Hazard assessment, Natural Hazards and Earth System Sciences, 14, 2265–2287, https://doi.org/10.5194/nhess-14-2265-2014, 2014.
* Costa, A., Smith, V., Macedonio, G., and Matthews, N. E.: The magnitude and impact of the Youngest
Toba Tuff super-eruption, Frontiers in Earth Science, 2, 16, https://doi.org/10.3389/feart.2014.00016, 2014.
* Folch, A., Mingari, L., Osores, M. S., and Collini, E.: Modeling volcanic ash resuspension - application to the 14-18 October 2011 outbreak episode in Central Patagonia, Argentina, Nat. Hazards Earth Syst. Sci., 14, 119–133, https://doi.org/10.5194/nhess-14-119-2014, 2014.
* Scaini, C., S. Biass, A. Galderisi, C. Bonadonna, K. Smith, A. Folch, A. Hoskuldsson, A multi-scale risk assessment to tephra fallout and dispersal at 4 Icelandic volcanoes – Part II: vulnerability and impact assessment, Nat. Hazards Earth Syst. Sci., 14, 2289-2312, https://doi.org/10.5194/nhess-14-2289-2014, 2014

## 2013
* Bonasia, R., Scaini, C., Capra, L., Nathenson, M., Siebe, C., Arana-Salinas, L., and Folch, A.: Long-range hazard assessment of volcanic ash dispersal for a Plinian eruptive scenario at Popocatépetl volcano (Mexico): implications for civil aviation safety, Bulletin of Volcanology, 76, 789, https://doi.org/10.1007/s00445-013-0789-z, 2013.
* Collini, E., Osores, S., Folch, A., Viramonte, J., Villarosa, G., and Salmuni, G.: Volcanic ash forecast during the June 2011 Cordón Caulle eruption, Natural Hazards, 66, 389–412, https://doi.org/10.1007/s11069-012-0492-y, 2013.
* Costa, A., Folch, A., and Macedonio, G.: Density-driven transport in the umbrella region of volcanic clouds: Implications for tephra dispersion models, Geophys. Res. Lett., 40, 1–5, https://doi.org/10.1002/grl.50942, corrected on 17 June 2019, 2013.
* Osores, M., Folch, A., Collini, E., Villarosa, G., Durant, A., Pujol, G., and Viramonte, J.: Validation of the FALL3D model for the 2008 Chaiten eruption using field, laboratory and satellite data„ Andean Geology, 40, 262–276, https://doi.org/10.5027/andgeoV40n2-a05, 2013.

## 2012
* Bear-Crozier, A., Kartadinata, N., Heriwaseso, A., and Møller Nielsen, O.: Development of python-FALL3D: a
modified procedure for modelling volcanic ash dispersal in the Asia-Pacific region, Natural Hazards, 64, 821–838, 2012.
* Bonasia, R., A. Costa, A. Folch, G. Macedonio, Numerical simulation of tephra transport and deposition of the 1982 El Chichón eruption and implications for hazard assessment, Journal of Volcanology and Geothermal Research, 231, 39-49, doi:10.1016/j.jvolgeores.2012.04.006, 2012.
* Costa, A., Folch, A., Macedonio, G., Giaccio, B., Isaia, R., and Smith, V. C.: Quantifying volcanic ash dispersal and impact of the Campanian Ignimbrite super-eruption, Geophysical Research Letters, 39, https://doi.org/10.1029/2012GL051605, 2012.
* Folch, A., Costa, A., and Basart, S.: Validation of the FALL3D ash dispersion model using observations of the 2010 Eyjafjallajökull volcanic ash clouds, Atmospheric Environment, 48, 165–183, https://doi.org/https://doi.org/10.1016/j.atmosenv.2011.06.072, 2012.
* Scaini, C., Folch, A., and Navarro, M.: Tephra hazard assessment at Concepción Volcano, Nicaragua, Journal
of Volcanology and Geothermal Research, 219, 41–51, https://doi.org/10.1016/j.jvolgeores.2012.01.007, 2012.
* Sulpizio, R., Folch, A., Costa, A., Scaini, C., and Dellino, P.: Hazard assessment of far-range volcanic ash dispersal from a violent Strombolian eruption at Somma-Vesuvius volcano, Naples, Italy: implications on civil aviation, Bulletin of Volcanology, 74, 2205–2218, https://doi.org/10.1007/s00445-012-0656-3, 2012.

## 2011
* Corradini, S., Merucci, L., and Folch, A.: Volcanic Ash Cloud Properties: Comparison Between MODIS Satellite Retrievals and FALL3D Transport Model, IEEE Geoscience and Remote Sensing Letters, 8, 248–252, https://doi.org/10.1109/LGRS.2010.2064156, 2011.

## 2010
* Costa, A., Folch, A., and Macedonio, G.: A model for wet aggregation of ash particles in volcanic plumes and clouds: I.Theoretical formulation, Journal of Geophysical Research, 115, https://doi.org/10.1029/2009JB007175, 2010.
* Folch, A., Costa, A., Durant, A., and Macedonio, G.: A model for wet aggregation of ash particles in volcanic plumes and clouds: II. Model application, Journal of Geophysical Research, 115, https://doi.org/10.1029/2009JB007176, 2010.
* Folch A., R. Sulpizio, Evaluating the long-range volcanic ash hazard using supercomputing facilities. Application to Somma-Vesuvius (Italy), and consequences on civil aviation over the Central Mediterranean Area, Bulletin of Volcanology, https://doi.org/10.1007/s00445-010-0386-3, 2010.
* Scollo, S., Folch, A., Coltelli, M., and Realmuto, V. J.: Threedimensional volcanic aerosol dispersal: A comparison between Multiangle Imaging Spectroradiometer (MISR) data and numerical simulations, Journal of Geophysical Research: Atmospheres, 115, https://doi.org/10.1029/2009JD013162, 2010.

## 2009
* Folch, A., Costa, A., and Macedonio, G.: FALL3D: A Computational Model for Transport and Deposition
of Volcanic Ash, Comput. Geosci., 35, 1334–1342, https://doi.org/10.1016/j.cageo.2008.08.008, 2009.

## 2008
* Folch, A., Cavazzoni, C., Costa, A., and Macedonio, G.: An automatic procedure to forecast tephra fallout, J. Volcanol. Geotherm. Res., 177, 767–777, 2008.
* Folch, A., Jorba, O., Viramonte, J., Volcanic ash forecast – application to the May 2008 Chaiten eruption, Nat. Hazards Earth Syst. Sci., 8, 927–940, 2008.
* Macedonio, G., A. Costa, A. Folch, Ash fallout scenarios at Vesuvius: numerical simulations and implications for hazard assessment, Journal of Volcanology and Geothermal Research, 178, 366-377, https://doi.org/10.1016/j.jvolgeores.2008.08.014, 2008.
* Scollo, S., Folch, A., Costa, A., A parametric and comparative study of different tephra fallout models, Journal of Volcanology and Geothermal Research, 176(2), 199-211, doi:10.1016/j.jvolgeores.2008.04.002, 2008.

## 2006
* Costa, A., Macedonio, G., and Folch, A.: A three-dimensional Eulerian model for transport and deposition of volcanic ashes, Earth and Planetary Science Letters, 241, 634 – 647, https://doi.org/http://dx.doi.org/10.1016/j.epsl.2005.11.019, 2006.

## 2000
* Kurganov, A. and Tadmor, E.: New High–Resolution Cen- tral Schemes for Nonlinear Conservation Laws and Convection–Diffusion Equations, J Comp Phys, 160, 241– 110 282, https://doi.org/http://dx.doi.org/10.1006/jcph.2000.6459, 2000.
