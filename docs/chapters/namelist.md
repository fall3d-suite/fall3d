# The namelist file
The namelist file `name.inp` is an input configuration file in ASCII format required by all tasks. 
It consists of a set of blocks defining physical parameterisations, time ranges, model domain, emission source, numerical schemes, etc. 
Each task reads only the necessary blocks generating self-consistent input files. 
Parameters within a block are listed one per record, in arbitrary order, and optionally can be followed by one (or more) blank space and a comment. 
Comments start with the exclamation mark symbol "!".
Real numbers can be expressed using the FORTRAN notation (e.g. 12E7). 

An example of namelist file can be found in the FALL3D distribution under the folder `Example/Example.inp`.

Next, a full description of each block in the namelist input file is presented.
