# Compilation Guide for EuroHPC systems

This document provides instructions on how to prepare the environment and compile FALL3D using `CMake` and `Meson` on the **Leonardo**, **Mare Nostrum 5**, and **Lumi** systems.

## Leonardo@CINECA
<details>
<summary>Expand section for <span style="color: fuchsia;"><strong>Leonardo CINECA</strong></span> installation instructions</summary>

### Load CMake
CMake is **available** from the login without **any action**.
### Load Meson
```bash
module load spack
spack load meson@1.2.2%gcc@12.2.0
```
### Build CMake
```bash
# cd into fall3d folder after git clone the repo
mkdir build
cd build
cmake .. [ options ]
make -j15
```
***Cmake options example***
```bash
# -DCMAKE_BUILD_TYPE can also be used
cmake .. -DDETAIL_BIN=YES -DWITH-MPI=YES -DWITH-ACC=YES -DWITH-R4=NO \
-DCUSTOM_COMPILER_FLAGS="-fast -g -Minfo=accel" \
-DCUSTOM_LINKER_FLAGS="-g"
```

### Build Meson
```bash
# cd into fall3d folder after git clone the repo
meson setup build [ options ]
meson compile -C build/
```
***Meson options example***
```bash
meson setup build -D DETAIL_BIN=true -D WITH-R4=false -D WITH-ACC=true \
-D WITH-MPI=true -D CUSTOM_COMPILER_FLAGS="-fast -g -Minfo=accel" \
-D CUSTOM_LINKER_FLAGS="-g" -D buildtype=plain
```
### GNU required modules

```bash
module load netcdf-fortran/
```

### NVHPC required modules
``` bash
# Leonardo will load the dependencies automatically
module load netcdf-fortran/4.6.1--openmpi--4.1.6--nvhpc--23.11
```

</details>

## MN5-ACC@BSC
<details>
<summary>Expand section for <span style="color: fuchsia;"><strong>Mare Nostrum 5</strong></span> installation instructions</summary>

### Load CMake
```bash
module load EB/apps
module load CMake/3.27.6-GCCcore-13.2.0
```

### Load Meson
```bash
module load EB/apps
module load Meson/1.2.3-GCCcore-13.2.0
```
### Build CMake
```bash
# cd into fall3d folder after git clone the repo
mkdir build
cd build
cmake .. [ options ]
make -j15
```
***Cmake options example***
```bash
# -DCMAKE_BUILD_TYPE can also be used
cmake .. -DDETAIL_BIN=YES -DWITH-MPI=YES -DWITH-ACC=YES -DWITH-R4=NO \
-DCUSTOM_COMPILER_FLAGS="-fast -g -Minfo=accel" \
-DCUSTOM_LINKER_FLAGS="-g"
```

### Build Meson
```bash
# cd into fall3d folder after git clone the repo
meson setup build [ options ]
meson compile -C build/
```
***Meson options example***
```bash
meson setup build -D DETAIL_BIN=true -D WITH-R4=false -D WITH-ACC=true \
-D WITH-MPI=true -D CUSTOM_COMPILER_FLAGS="-fast -g -Minfo=accel" \
-D CUSTOM_LINKER_FLAGS="-g" -D buildtype=plain
```

### GNU required modules

```bash
module load EB/apps
module load netCDF-Fortran/4.6.1-gompi-2023b
```

### NVHPC required modules
``` bash
module load nvidia-hpc-sdk/24.3
module load hdf5/1.14.1-2-nvidia-nvhpcx
module load pnetcdf/1.12.3-nvidia-nvhpcx
module load netcdf/c-4.9.2_fortran-4.6.1_cxx4-4.3.1-nvidia-nvhpcx
```

</details>

## LUMI@CSC
<details>
<summary>Expand section for <span style="color: fuchsia;"><strong>LUMI</strong></span> installation instructions</summary>

### Load CMake
CMake is available from the login without any action

### Load Meson
Meson can be loaded in LUMI but is not included because is not compatible with cray compiler

### Build CMake
```bash
# cd into fall3d folder after git clone the repo
mkdir build
cd build
cmake .. [ options ]
make -j15
```
***Cmake options example***
```bash
# -DCMAKE_BUILD_TYPE can also be used
cmake .. -DDETAIL_BIN=YES -DWITH-MPI=YES -DWITH-ACC=YES -DWITH-R4=NO \
-DCUSTOM_COMPILER_FLAGS="-fast -g -Minfo=accel" \
-DCUSTOM_LINKER_FLAGS="-g"
```

### CRAY serial / parallel required modules

```bash
module load cray-hdf5
module load cray-netcdf 
```

### CRAY GPU required modules
``` bash
module load LUMI/24.03
module load partition/G
module load rocm
module load cray-hdf5
module load cray-netcdf
```

</details>