# WRF-ARW

The Advanced Research WRF (WRF-ARW) model is a flexible, state-of-the-art atmospheric simulation system, and is portable and efficient on parallel computing platforms. It is suitable for use across scales, ranging from meters to thousands of kilometers, for a broad range of applications. For detailed information go to the [WRF Users' Guide](https://www2.mmm.ucar.edu/wrf/users/wrf_users_guide/build/html/index.html).

Tipically, WRF data is not publicly available. However, WRF is used by weather agencies all over the world to generate weather forecasts and, in some cases, data can be ordered by special request. Alternatively, you can run the model by yourself since WRF is an open-source code in the public domain. The following [tutorial](https://www2.mmm.ucar.edu/wrf/OnLineTutorial/) can be helpful for this purpose.
