# Installation requirements

In order to compile and install FALL3D you need:

* Unix-like operating system (MacOS or Linux)
* A modern Fortran compiler (e.g., gfortran, ifort, xlf90)
* Optional:
  * An MPI library with Fortran support for parallel executions. (i.e. OpenMPI, Intel MPI or MVAPICH2)
  * Hardware specific drivers (GPU)
  * Specific compiler to use GPU acceleration with OpenACC support (i.e. NVfortran, CRAY)
  * Python (>=3.8), required to run pre-processing and download tools
  * Git version management


## External libraries

FALL3D requires [netCDF libraries](https://docs.unidata.ucar.edu/netcdf-c/current/index.html). Specifically:

* The [netCDF-Fortran](https://www.unidata.ucar.edu/software/netcdf/) library with netCDF-4 support
* For parallel I/O support on classic netCDF files, [PnetCDF](https://parallel-netcdf.github.io/wiki/Download.html) 1.6.0 or later is required

### HPC environment
FALL3D generally relies on libraries that are widely available on scientific computing clusters. Nonetheless, there may be situations where a specific module is absent or does not support the intended compiler. If you encounter any of this issues, contact the cluster's support team as they can provide the best solutions or alternatives.

### Getting pre-built netCDF libraries
The easiest way to get netCDF in a amachine where you are a privileged user is through a package management program (e.g. apt, rpm, yum, homebrew).
NetCDF is available from many different repositories, including the Ubuntu main repositories.

When getting netCDF from a software repository, you should get a development version. 
A development version will typically have a name such as "netcdf-devel" or "libnetcdf-dev".
For example, you can install the netCDF Fortran library in Ubuntu with the command:
```console
sudo apt install libnetcdff-dev
```

### Building netCDF
The netCDF Fortran libraries depend on the netCDF C library which requires third-party libraries for full functionality, including: 
* HDF5 1.8.9 or later (for netCDF-4 support)
* zlib 1.2.5 or later (for netCDF-4 compression)

And, optionally, 
* PnetCDF 1.6.0 or later (for parallel I/O support on classic netCDF files)

For information regarding the netCDF-C libraries, see [Building netCDF-C](https://docs.unidata.ucar.edu/nug/current/getting_and_building_netcdf.html#building) and for netCDF-Fortran libraries, see [Building the NetCDF-4.2 and later Fortran libraries](https://docs.unidata.ucar.edu/netcdf-c/current/building_netcdf_fortran.html).
