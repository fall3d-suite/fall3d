# ERA5: Getting and merging data

ERA5 is the fifth generation ECMWF reanalysis for the global climate and weather from 1940 onwards. Data regridded to a regular lat-lon grid of 0.25 degrees can be obtained from the [Climate Data Store](https://cds.climate.copernicus.eu). Alternatively, we provide some scripts to automate the downloading process, select the variables required by FALL3D, domain cropping, etc. 

**Important note:**
In order to use the Climate Data Store (CDS) API, you need to have an account and install a key as explained [here](https://cds.climate.copernicus.eu/api-how-to) (see Section _Install the CDS API key_).

## Surface data

You need to obtain surface and upper level data in different requests. The script `era5_sfc.py` can be used to retrieve surface data.

* `era5_sfc.py`: Download ERA5 data at surface level via the Climate Data Store (CDS) infrastructure.
  <details>
  <summary>Command line options. Click to expand</summary>
  <p>

    ```
    usage: era5_sfc.py [-h] [-d start_date end_date] [-x lonmin lonmax] [-y latmin latmax] [-r resolution] [-s step] [-b block] [-i file] [-v]

    Download ERA5 data (single level) required by FALL3D model.

    options:
    -h, --help            show this help message and exit
    -d start_date end_date, --date start_date end_date
                        Date range in format YYYYMMDD
    -x lonmin lonmax, --lon lonmin lonmax
                        Longitude range
    -y latmin latmax, --lat latmin latmax
                        Latitude range
    -r resolution, --res resolution
                        Spatial resolution (deg)
    -s step, --step step  Temporal resolution (h)
    -b block, --block block
                        Block in the configuration file
    -i file, --input file
                        Configuration file
    -v, --verbose         increase output verbosity
    ```
  </p>
  </details> 

## Upper level data

In addition to surface data, you need to obtain upper level data as well. You can choose between model level data (high vertical resolution) or pressure level data (low vertical resolution).

* `era5_ml.py`: Download ERA5 meteorological data on model levels (137 vertical levels) via the Climate Data Store (CDS) infrastructure.
  <details>
  <summary>Command line options. Click to expand</summary>
  <p>

    ```
    usage: era5_ml.py [-h] [-d start_date end_date] [-x lonmin lonmax] [-y latmin latmax] [-r resolution] [-s step] [-b block] [-i file] [-v]

    Download ERA5 data (model levels) required by FALL3D model.

    options:
      -h, --help            show this help message and exit
      -d start_date end_date, --date start_date end_date
                            Date range in format YYYYMMDD
      -x lonmin lonmax, --lon lonmin lonmax
                            Longitude range
      -y latmin latmax, --lat latmin latmax
                            Latitude range
      -r resolution, --res resolution
                            Spatial resolution (deg)
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details> 
    
* `era5_pl.py`: Download ERA5 data required on pressure levels (37 vertical levels) via the Climate Data Store (CDS) 
  infrastructure.
  <details>
  <summary>Command line options. Click to expand</summary>
  <p>

    ```
    usage: era5_pl.py [-h] [-d start_date end_date] [-x lonmin lonmax] [-y latmin latmax] [-r resolution] [-s step] [-b block] [-i file] [-v]

    Download ERA5 data (pressure levels) required by FALL3D model.

    options:
      -h, --help            show this help message and exit
      -d start_date end_date, --date start_date end_date
                            Date range in format YYYYMMDD
      -x lonmin lonmax, --lon lonmin lonmax
                            Longitude range
      -y latmin latmax, --lat latmin latmax
                            Latitude range
      -r resolution, --res resolution
                            Spatial resolution (deg)
      -s step, --step step  Temporal resolution (h)
      -b block, --block block
                            Block in the configuration file
      -i file, --input file
                            Configuration file
      -v, --verbose         increase output verbosity
    ```
  </p>
  </details> 

## Merging data
  
Once downloaded, ERA5 data on upper levels (pressure or model levels) and at surface has to be merged in order to generate a complete database for the FALL3D model. To this purpose, you can run the command:

```
cdo merge upper_level.nc surface.nc merged_meteo.nc
```

to generate the `merged_meteo.nc` file that will be ingested into FALL3D.

**Important notes:**

* Single level and pressure level parameters are available on the C3S Climate Data Store (CDS) disks.

* ERA5 data on model levels is not available on the CDS disks, but can be accessed from ECMWF data archive (MARS). However, MARS access is relatively slower.

* You may need to download data using multiple requests for very large datasets. In this case, you should download data from different time ranges separately and then concatenate the files.

* Note that you cannot concatenate the files directly, because of the packed format in the netcdf files (i.e., data is stored as 16-bit short variables). You can convert the data to float datatype and performing the concatenation in a single command using:
```
cdo -b F32 mergetime april.nc may.nc output.nc
```
It will generate a correct output file: `output.nc` for the total period April-May.
