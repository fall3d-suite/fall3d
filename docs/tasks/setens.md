# Task `SetEns`

The task `SetEns` runs automatically and foremost whenever ensemble runs are performed. It generates and sets the ensemble members by perturbing a reference state which defines so-called central or reference member. This task also creates a structure of sub-folders (`0001`, `0002`, etc...) where the input and output files for each ensemble member are stored. In order to enable ensemble runs, FALL3D must be executed with the argument `-nens`, which defines the ensemble size. The parameters to be perturbed are defined in the block `ENSEMBLE` of the configuration file. In addition, in this block it's possible to set the perturbation amplitude (given as a percentage of the reference value or in absolute terms) and the perturbation sampling strategy (constant or Gaussian distribution).

> **Notes:**
> * The `ENSEMBLE` block in the configuration file is ignored when the ensemble option is disabled
> * This task is available since the code version 8.1.

## Input file
This task requires the namelist file `name.inp` and the random number files `name.ens` under the ensemble member sub-folders contain the random numbers required to construct the ensemble. They must be read whenever an ensemble run is performed and the variable `RANDOM_NUMBERS_FROM_FILE` is set to `YES` in the configuration file.

## Output files

### LOG file
The LOG file `name.SetEns.log` is an ASCII file where the status of the task is reported. In addition, the random numbers used to construct the ensemble are summarized here.

### Random number files
The random number files `name.ens` under the ensemble member sub-folders contain the random numbers required to construct the ensemble. The random numbers are regenerated and the files `name.ens` updated in every new run as long as the variable `RANDOM_NUMBERS_FROM_FILE` is set to `NO` in the configuration file. Otherwise, the files `name.ens` are read in a new ensemble run and remain unchanged. This is useful to reproduce a previous ensemble run, for example. If the files `name.ens` do not exist (e.g. in a first run), they are generated regardless of the value assigned to `RANDOM_NUMBERS_FROM_FILE`.


