# Task `SetDbs`

The task `SetDbs` interpolates (for a defined time slab) meteorological variables from the meteorological model grid to the FALL3D computational domain.

## Input files
This task requires two input files, i.e. the namelist file `name.inp` and a gridded dataset of meteorological variables in a netCDF file.
