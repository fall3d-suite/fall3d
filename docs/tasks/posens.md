# Task `PosEns`

The `PosEns` is a post-processing task and must be executed once an ensemble run has been performed previously. The task merges individual member outputs from a previous ensemble run, computes deterministic and probabilistic variables and produces a single output file `name.ens.nc` in netCDF format. The required ensemble outputs are specified in the `ENSEMBLE_POST` block and include ensemble mean, median or user-defined percentiles.

> **Notes:**
> * Only supported in parallel mode
> * This task is available since the code version 8.1.

## Input file
This task requires a single input file, i.e. the namelist file `name.inp`.

## Output files

### LOG file
The LOG file `name.PosEns.log` is an ASCII file where the status of the task is reported.

### Result file
The result file `name.ens.nc` contains multiple ensemble outputs as specified by the block `ENSEMBLE_POST`.
