# Block `GRID`
This block defines the grid variables needed by the SetDbs and FALL3D tasks
1. `HORIZONTAL_MAPPING` = (options)
   * Horizontal mapping
   * Input options:
      - `CARTESIAN`: Cartesian mapping
      - `SPHERICAL`: Spherical mapping
1. `VERTICAL_MAPPING` = (options)
   * Vertical mapping
   * Input options:
      - `SIGMA_NO_DECAY`: Terrain following levels with no decay
      - `SIGMA_LINEAR_DECAY`: Terrain following levels with linear decay from surface to the flat top
      - `SIGMA_EXPONENTIAL_DECAY`: Terrain following levels with exponential decay from surface to the flat top
1. `LONMIN` = (float)
   * West longitude in decimal degrees of the domain
1. `LONMAX` = (float)
   * East longitude in decimal degrees of the domain
1. `LATMIN` = (float)
   * South latitude in decimal degrees of the domain
1. `LATMAX` = (float)
   * North latitude in decimal degrees of the domai
1. `NX` = (options)
   * Define the number of grid cells or resolution along dimension x
   * Input options:
      - (integer): Number of grid cells (mass points) along x
      - `RESOLUTION` (float): Resolution (grid size) along x
1. `NY` = (options)
   * Define the number of grid cells or resolution along dimension y
   * Input options:
      - (integer): Number of grid cells (mass points) along y
      - `RESOLUTION` (float): Resolution (grid size) along y
1. `NZ` = (integer)
   * Define the number of grid cells (mass points) along dimension z
1. `ZMAX_(M)` = (float)
   * Top height of the computational domain in meters
1. `SIGMA_VALUES` = (float_list)
   * List of values of sigma coordinate in the range (0,1). The list size should be less or equal than `NZ`+1
   * Optional
   * Note: If not present, uniform distribution of vertical layers is assumed
