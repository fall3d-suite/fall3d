# Block `SPECIES`
This block is used by FALL3D, SetTgsd, and SetSrc tasks and defines which species are modeled
1. `TEPHRA` = (options)
   * Indicate if `TEPHRA` species are included
   * Input options:
      - `ON`: Activate tephra particle species
      - `OFF`: Deactivate tephra particle species
1. `DUST` = (options)
   * Indicate if `DUST` species are included
   * Input options:
      - `ON`: Activate dust particle species
      - `OFF`: Deactivate dust particle species
1. `H2O` = (options)
   * Indicate if `H2O` aerosol species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate H2O aerosol species and set the mass fraction in percent
      - `OFF`: Deactivate H2O aerosol species
   * Note: Aerosols can run independently or coupled with tephra species.
               If `TEPHRA` = `ON` it is assumed that the aerosol species are all of 
               magmatic origin mass fraction of `SO2` and `H2O` are relative to tephra.
               If `TEPHRA` = `OFF` then the mass fraction of aerosols must sum 1
1. `SO2` = (options)
   * Indicate if `SO2` aerosol species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate SO2 aerosol species and set the mass fraction in percent
      - `OFF`: Deactivate SO2 aerosol species
   * Note: Aerosols can run independently or coupled with tephra species.
               If `TEPHRA` = `ON` it is assumed that the aerosol species are all of 
               magmatic origin mass fraction of `SO2` and `H2O` are relative to tephra.
               If `TEPHRA` = `OFF` then the mass fraction of aerosols must sum 1
1. `CS134` = (options)
   * Indicate if `CS-134` radionuclide species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate Cs-134 radionuclide species and set the mass fraction in percent
      - `OFF`: Deactivate Cs-134 radionuclide species
   * Note: All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1
1. `CS137` = (options)
   * Indicate if `CS-137` radionuclide species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate Cs-137 radionuclide species and set the mass fraction in percent
      - `OFF`: Deactivate Cs-137 radionuclide species
   * Note: All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1
1. `I131` = (options)
   * Indicate if `I-131` radionuclide species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate I-131 radionuclide species and set the mass fraction in percent
      - `OFF`: Deactivate I-131 radionuclide species
   * Note: All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1
1. `SR90` = (options)
   * Indicate if `SR-90` radionuclide species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate Sr-90 radionuclide species and set the mass fraction in percent
      - `OFF`: Deactivate Sr-90 radionuclide species
   * Note: All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1
1. `Y90` = (options)
   * Indicate if `Y-90` radionuclide species are included
   * Input options:
      - `ON` `MASS_FRACTION_(%)` `=` (float): Activate Y-90 radionuclide species and set the mass fraction in percent
      - `OFF`: Deactivate Y-90 radionuclide species
   * Note: All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1
