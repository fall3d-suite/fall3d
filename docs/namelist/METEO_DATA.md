# Block `METEO_DATA`
This block defines variables related to the input meteorological dataset. It is read by the SetDbs task
1. `METEO_DATA_FORMAT` = (options)
   * Input meteorological model
   * Input options:
      - `WRF`: Advanced Research WRF (ARW), WRF-ARW - Mesoscale model
      - `GFS`: Global Forecast System - Global weather forecast model
      - `ERA5`: ERA5 ECMWF reanalysis in pressure levels - Global climate and weather renalysis
      - `ERA5ML`: ERA5 ECMWF reanalysis in model levels - Global climate and weather renalysis
      - `IFS`: ECMWF Integrated Forecasting System - Global weather forecast model
      - `CARRA`: Copernicus Arctic Regional Reanalysis in pressure levels - Arctic regional reanalysis
1. `METEO_DATA_DICTIONARY_FILE` = (string)
   * Path to the database dictionary file defining the variable names. An example can be found in the folder `Other/Meteo/Tables`
   * Optional
   * Note:  If not given, a default dictionary for each model will be used
1. `METEO_DATA_FILE` = (string)
   * Path to the meteo model data file in netCDF format
1. `METEO_ENSEMBLE_BASEPATH` = (string)
   * Root path for `METEO_DATA_FILE`
   * Optional
   * Note: Used for ensemble runs when multiple meteo files are available (`METEO_ENSEMBLE_BASEPATH`/0001/`METEO_DATA_FILE`...). If not provided a single meteo file is used for the ensemble (`METEO_DATA_FILE`)
1. `METEO_LEVELS_FILE` = (string)
   * Path to the table defining the coefficients for vertical hybrid levels
   * Note: Only required if `METEO_DATA_FORMAT` = `ERA5ML`
1. `DBS_BEGIN_METEO_DATA_(HOURS_AFTER_00)` = (float)
   * Starting time for the database file in hours after 00:00 UTC of the starting day
1. `DBS_END_METEO_DATA_(HOURS_AFTER_00)` = (float)
   * End time for the database file in hours after 00:00 UTC of the starting day
1. `METEO_COUPLING_INTERVAL_(MIN)` = (float)
   * Time interval to update (couple) meteorological variables
   * Note: wind velocity is linearly interpolated in time for each time step
1. `MEMORY_CHUNK_SIZE` = (integer)
   * Size of memory chunks used to store meteo data timesteps
   * Note: Must be greater than 1
