# Block `TIME_UTC`
This block defines variables related to date and time. It is used by FALL3D, SetDbs, and SetSrc tasks
1. `YEAR` = (integer)
   * Year of the starting date
1. `MONTH` = (integer)
   * Month of the starting date
1. `DAY` = (integer)
   * Day of the starting date
1. `RUN_START_(HOURS_AFTER_00)` = (float)
   * Starting time in hours after 00:00 UTC of the starting day
1. `RUN_END_(HOURS_AFTER_00)` = (float)
   * End time in hours after 00:00 UTC of the starting day
   * Note: Runs can continue even after the source term has been switched off (e.g., when the eruption has stopped)
1. `INITIAL_CONDITION` = (options)
   * Type of initial concentration field
   * Input options:
      - `NONE`: Initial concentration is zero
      - `INSERTION`: Initial concentration from an input file
      - `RESTART`: Initial concentration from a previous run
1. `RESTART_FILE` = (string)
   * Path to the restart file
   * Note: Only used if `INITIAL_CONDITION = RESTART`
1. `RESTART_ENSEMBLE_BASEPATH` = (string)
   * Root path for `RESTART_FILE`
   * Optional
   * Note: Used for ensemble runs when multiple restart files are available (`RESTART_ENSEMBLE_BASEPATH`/0001/`RESTART_FILE`...). If not provided a single restart file is used for the ensemble (`RESTART_FILE`)
