# Block `MODEL_VALIDATION`
This block is read by task PosVal to perform automatic model validation with a set of quantitative and categorical metrics
1. `OBSERVATIONS_TYPE` = (options)
   * Type of observations
   * Input options:
      - `SATELLITE_DETECTION`: Satellite yes/no flag contours
      - `SATELLITE_RETRIEVAL`: Satellite quantitative retrievals of `TEPHRA`/`SO2` species
      - `DEPOSIT_CONTOURS`: Deposit isopach/isopleth contours read from a gridded netCDF file
      - `DEPOSIT_POINTS`: Deposit point-wise observations
1. `OBSERVATIONS_FILE` = (string)
   * Path to the files with observations
1. `OBSERVATIONS_DICTIONARY_FILE` = (string)
   * Path to the observations dictionary file
   * Optional
   * Note: Examples can be found in `Other/Dep`
1. `RESULTS_FILE` = (string)
   * Path to the FALL3D output file with simulation results (`*.res.nc` or `*.ens.nc`)
## Sub-block `IF_OBSERVATIONS_TYPE_SATELLITE`
Used when `OBSERVATIONS_TYPE` = `SATELLITE_DETECTION` or `OBSERVATIONS_TYPE` = `SATELLITE_RETRIEVAL`
1. `COLUMN_MASS_OBSERVATION_THRESHOLD_(G/M2)` = (float)
   * Column mass threshold in g/m2 for `TEPHRA` species
   * Note: It should be consistent with `COLUMN_MASS_THRESHOLDS_(G/M2)` for ensemble runs
1. `COLUMN_MASS_OBSERVATION_THRESHOLD_(DU)` = (float)
   * Column mass threshold in DU for `SO2` species
   * Note: It should be consistent with `COLUMN_MASS_THRESHOLDS_(DU)` for ensemble runs
## Sub-block `IF_OBSERVATIONS_TYPE_DEPOSIT`
Used when `OBSERVATIONS_TYPE` = `DEPOSIT_CONTOURS` or `OBSERVATIONS_TYPE` = `DEPOSIT_POINTS`
1. `GROUND_LOAD_OBSERVATION_THRESHOLD_(KG/M2)` = (float)
   * Deposit load threshold in kg/m2 for `TEPHRA` species
   * Note: It should be consistent with `GROUND_LOAD_THRESHOLDS_(KG/M2)` for ensemble runs
