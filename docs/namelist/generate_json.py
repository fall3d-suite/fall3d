import json

### Parameters ###
fname = "namelist.json"
##################

def readItem(default, 
             description  = '',
             inputs       = ['float'],
             inputs_str   = [''],
             required     = True, 
             note         = ''):
    x = {'default'      : default,
         'description'  : description,
         'inputs'       : inputs,
         'inputs_str'   : inputs_str,
         'required'     : required,
         'note'         : note,
         }
    return x

data = {}

data = {
    'TIME_UTC': {
        'main': {'name': 'This block defines variables related to date and time. It is used by FALL3D, SetDbs, and SetSrc tasks'}
        },
    'INSERTION_DATA': {
        'main': {'name': 'This block is read by the FALL3D task only if `INITIAL_CONDITION` = `INSERTION`'}
        },
    'METEO_DATA': {
        'main': {'name': 'This block defines variables related to the input meteorological dataset. It is read by the SetDbs task'}
        },
    'GRID': {
        'main': {'name': 'This block defines the grid variables needed by the SetDbs and FALL3D tasks'}
        },
    'SPECIES': {
        'main': {'name': 'This block is used by FALL3D, SetTgsd, and SetSrc tasks and defines which species are modeled'}
        },
    'SPECIES_TGSD': {
        'main':          {'name': 'These blocks define the TGSD for each species and are used by the SetTgsd task to generate some basic distributions'},
        'IF_GAUSSIAN':   {'name': 'Used when `DISTRIBUTION`=`GAUSSIAN`'},
        'IF_BIGAUSSIAN': {'name': 'Used when `DISTRIBUTION`=`BIGAUSSIAN`'},
        'IF_WEIBULL':    {'name': 'Used when `DISTRIBUTION`=`WEIBULL`'},
        'IF_BIWEIBULL':  {'name': 'Used when `DISTRIBUTION`=`BIWEIBULL`'},
        'IF_CUSTOM':     {'name': 'Used when `DISTRIBUTION`=`CUSTOM`'},
        'IF_ESTIMATE':   {'name': 'Used when `DISTRIBUTION`=`ESTIMATE`'},
        },
    'PARTICLE_AGGREGATION': {
        'main': {'name': 'This block is used by task SetSrc and controls particle aggregation and cut-off (for categories particles and radionuclides only)'}
        },
    'SOURCE': {
        'main':              {'name': 'This block defines the variables needed by the SetSrc task to generate the source term for the emission phases'},
        'IF_SUZUKI_SOURCE':  {'name': 'Used when `SOURCE_TYPE`=`IF_SUZUKI_SOURCE`'},
        'IF_TOP-HAT_SOURCE': {'name': 'Used when `SOURCE_TYPE`=`IF_TOP-HAT_SOURCE`'},
        'IF_PLUME_SOURCE':   {'name': 'Used when `SOURCE_TYPE`=`PLUME`'},
        },
    'ENSEMBLE':{
        'main': {'name': 'This block is used by task SetEns to generate the ensemble members'},
        'IF_PERTURBATE_COLUMN_HEIGHT': {'name': 'Used when `PERTURBATE_COLUMN_HEIGHT` != `NO`'},
        'IF_PERTURBATE_MASS_FLOW_RATE': {'name': 'Used when `PERTURBATE_MASS_FLOW_RATE` != `NO`'},
        'IF_PERTURBATE_SOURCE_START': {'name': 'Used when `PERTURBATE_SOURCE_START` != `NO`'},
        'IF_PERTURBATE_SOURCE_DURATION': {'name': 'Used when `PERTURBATE_SOURCE_DURATION` != `NO`'},
        'IF_PERTURBATE_TOP-HAT_THICKNESS': {'name': 'Used when `PERTURBATE_TOP-HAT_THICKNESS` != `NO`'},
        'IF_PERTURBATE_SUZUKI_A': {'name': 'Used when `PERTURBATE_SUZUKI_A` != `NO`'},
        'IF_PERTURBATE_SUZUKI_L': {'name': 'Used when `PERTURBATE_SUZUKI_L` != `NO`'},
        'IF_PERTURBATE_WIND': {'name': 'Used when `PERTURBATE_WIND` != `NO`'},
        'IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT': {'name': 'Used when `PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` != `NO`'},
        'IF_PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS': {'name': 'Used when `PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` != `NO`'},
        'IF_PERTURBATE_FI_MEAN': {'name': 'Used when `PERTURBATE_FI_MEAN` != `NO`'},
        'IF_PERTURBATE_DIAMETER_AGGREGATES_(MIC)': {'name': 'Used when `PERTURBATE_DIAMETER_AGGREGATES_(MIC)` != `NO`'},
        'IF_PERTURBATE_DENSITY_AGGREGATES': {'name': 'Used when `PERTURBATE_DENSITY_AGGREGATES` != `NO`'},
        },
    'ENSEMBLE_POST':{
        'main': {'name': 'The block is used by the task PosEns for post-processing ensemble runs'},
        'IF_POSTPROCESS_PROBABILITY': {'name': 'Used when `POSTPROCESS_PROBABILITY` = `YES`'},
        'IF_POSTPROCESS_PERCENTILES': {'name': 'Used when `POSTPROCESS_PERCENTILES` = `YES`'},
        },
    'MODEL_PHYSICS':{
        'main': {'name': 'This block defines the specific variables related to physics in the FALL3D model'},
        'IF_GRAVITY_CURRENT': {'name': 'Used when `GRAVITY_CURRENT` = `YES`'},
        },
    'MODEL_OUTPUT':{
        'main': {'name': 'This block is read by task FALL3D and defines specific variables related to output strategy'},
        },
    'MODEL_VALIDATION':{
        'main': {'name': 'This block is read by task PosVal to perform automatic model validation with a set of quantitative and categorical metrics'},
        'IF_OBSERVATIONS_TYPE_SATELLITE': {'name': 'Used when `OBSERVATIONS_TYPE` = `SATELLITE_DETECTION` or `OBSERVATIONS_TYPE` = `SATELLITE_RETRIEVAL`'},
        'IF_OBSERVATIONS_TYPE_DEPOSIT': {'name': 'Used when `OBSERVATIONS_TYPE` = `DEPOSIT_CONTOURS` or `OBSERVATIONS_TYPE` = `DEPOSIT_POINTS`'},
        },
    }

######### TIME_UTC ######### 
x = {}
x["YEAR"] = readItem(
        2008,
        'Year of the starting date',
        inputs = ['integer'],
        )
x["MONTH"] = readItem(
        4,
        'Month of the starting date',
        inputs = ['integer'],
        )
x["DAY"] = readItem(
        29,
        'Day of the starting date',
        inputs = ['integer'],
        )
x["RUN_START_(HOURS_AFTER_00)"] = readItem(
        0,
        'Starting time in hours after 00:00 UTC of the starting day',
        )
x["RUN_END_(HOURS_AFTER_00)"] = readItem(
        10,
        'End time in hours after 00:00 UTC of the starting day',
        note = 'Runs can continue even after the source term has been switched off (e.g., when the eruption has stopped)',
        )
x["INITIAL_CONDITION"] = readItem(
        'NONE',
        'Type of initial concentration field',
        inputs = ['NONE','INSERTION','RESTART'],
        inputs_str = ['Initial concentration is zero',
                      'Initial concentration from an input file',
                      'Initial concentration from a previous run',
                       ],
        )
x["RESTART_FILE"] = readItem(
        'Example-8.0.rst.nc',
        'Path to the restart file',
        inputs = ['string'],
        note = 'Only used if `INITIAL_CONDITION = RESTART`',
        )
x["RESTART_ENSEMBLE_BASEPATH"] = readItem(
        './',
        'Root path for `RESTART_FILE`',
        inputs = ['string'],
        required = False,
        note = 'Used for ensemble runs when multiple restart files are available (`RESTART_ENSEMBLE_BASEPATH`/0001/`RESTART_FILE`...). If not provided a single restart file is used for the ensemble (`RESTART_FILE`)',
        )
data['TIME_UTC']['main']['params'] = x

######### INSERTION_DATA ######### 
x = {}
x["INSERTION_FILE"] = readItem(
        'Example-8.0.sat.nc',
        'Path to the initial condition file in netCDF format (e.g. a satellite retrieval)',
        inputs = ['string'],
        )
x["INSERTION_DICTIONARY_FILE"] = readItem(
        'Sat.tbl',
        'Path to the insertion dictionary file defining the variable names. An example can be found in `Other/Sat/Sat.tbl`',
        inputs = ['string'],
        required = False,
        note = 'If not given, a default dictionary will be used'
        )
x["INSERTION_TIME_SLAB"] = readItem(
        4,
        'Time slab in the insertion file to be used as the initial conditions',
        inputs = ['integer'],
        )
x["DIAMETER_CUT_OFF_(MIC)"] = readItem(
        64,
        'Cut-off diameter in microns. Maximum particle diameter used to define the initial condition',
        required = False,
        )
data['INSERTION_DATA']['main']['params'] = x

######### METEO_DATA ######### 
x = {}
x["METEO_DATA_FORMAT"] = readItem(
        'WRF',
        'Input meteorological model',
        inputs = ["WRF", "GFS", "ERA5", "ERA5ML", "IFS", "CARRA"],
        inputs_str = [
            "Advanced Research WRF (ARW), WRF-ARW - Mesoscale model",
            "Global Forecast System - Global weather forecast model",
            "ERA5 ECMWF reanalysis in pressure levels - Global climate and weather renalysis",
            "ERA5 ECMWF reanalysis in model levels - Global climate and weather renalysis",
            "ECMWF Integrated Forecasting System - Global weather forecast model",
            "Copernicus Arctic Regional Reanalysis in pressure levels - Arctic regional reanalysis",
            ]
        )
x["METEO_DATA_DICTIONARY_FILE"] = readItem(
        'WRF.tbl',
        'Path to the database dictionary file defining the variable names. An example can be found in the folder `Other/Meteo/Tables`',
        inputs = ['string'],
        required = False,
        note=' If not given, a default dictionary for each model will be used',
        )
x["METEO_DATA_FILE"] = readItem(
        'Example-8.0.wrf.nc',
        'Path to the meteo model data file in netCDF format',
        inputs = ['string'],
        )
x["METEO_ENSEMBLE_BASEPATH"] = readItem(
        './',
        'Root path for `METEO_DATA_FILE`',
        inputs = ['string'],
        required = False,
        note = 'Used for ensemble runs when multiple meteo files are available (`METEO_ENSEMBLE_BASEPATH`/0001/`METEO_DATA_FILE`...). If not provided a single meteo file is used for the ensemble (`METEO_DATA_FILE`)',
        )
x["METEO_LEVELS_FILE"] = readItem(
        'ERA5.hyb',
        'Path to the table defining the coefficients for vertical hybrid levels',
        inputs = ['string'],
        note = 'Only required if `METEO_DATA_FORMAT` = `ERA5ML`'
        )
x['DBS_BEGIN_METEO_DATA_(HOURS_AFTER_00)'] = readItem(
        0,
        "Starting time for the database file in hours after 00:00 UTC of the starting day",
        )
x['DBS_END_METEO_DATA_(HOURS_AFTER_00)']   = readItem(
        24,
        "End time for the database file in hours after 00:00 UTC of the starting day",
        )
x['METEO_COUPLING_INTERVAL_(MIN)']         = readItem(
        60, 
        'Time interval to update (couple) meteorological variables',
        note = 'wind velocity is linearly interpolated in time for each time step'
        )
x["MEMORY_CHUNK_SIZE"] = readItem(
        5,
        'Size of memory chunks used to store meteo data timesteps',
        inputs = ['integer'],
        note = 'Must be greater than 1',
        )
data['METEO_DATA']['main']['params'] = x

######### GRID #########
x = {}
x["HORIZONTAL_MAPPING"] = readItem(
        'SPHERICAL',
        'Horizontal mapping',
        inputs = ["CARTESIAN", "SPHERICAL"],
        inputs_str = ["Cartesian mapping",
                      "Spherical mapping",
                      ]
        )
x["VERTICAL_MAPPING"] = readItem(
        'SIGMA_LINEAR_DECAY',
        'Vertical mapping',
        inputs = ["SIGMA_NO_DECAY","SIGMA_LINEAR_DECAY","SIGMA_EXPONENTIAL_DECAY"],
        inputs_str = ["Terrain following levels with no decay",
                      "Terrain following levels with linear decay from surface to the flat top",
                      "Terrain following levels with exponential decay from surface to the flat top",
                      ],
        )
x["LONMIN"] = readItem(
        14.0,
        'West longitude in decimal degrees of the domain',
        )
x["LONMAX"] = readItem(
        16.0,
        'East longitude in decimal degrees of the domain',
        )
x["LATMIN"] = readItem(
        36.5,
        'South latitude in decimal degrees of the domain',
        )
x["LATMAX"] = readItem(
        38.5,
        'North latitude in decimal degrees of the domai',
        )
x["NX"] = readItem(
        50,
        'Define the number of grid cells or resolution along dimension x',
        inputs = ['integer','RESOLUTION float'],
        inputs_str = ['Number of grid cells (mass points) along x',
                      'Resolution (grid size) along x'],
        )
x["NY"] = readItem(
        50,
        'Define the number of grid cells or resolution along dimension y',
        inputs = ['integer','RESOLUTION float'],
        inputs_str = ['Number of grid cells (mass points) along y',
                      'Resolution (grid size) along y'],
        )
x["NZ"] = readItem(
        10,
        'Define the number of grid cells (mass points) along dimension z',
        inputs = ['integer'],
        )
x["ZMAX_(M)"] = readItem(
        10000,
        'Top height of the computational domain in meters',
        )
x["SIGMA_VALUES"] = readItem(
        [0.0, 0.01, 0.025, 0.05, 0.1],
        'List of values of sigma coordinate in the range (0,1). The list size should be less or equal than `NZ`+1',
        inputs = ['float_list'],
        required = False,
        note = 'If not present, uniform distribution of vertical layers is assumed'
        )
data['GRID']['main']['params'] = x

######### SPECIES #########
x = {}
x["TEPHRA"] = readItem(
        'On',
        'Indicate if `TEPHRA` species are included',
        inputs = ["ON","OFF"],
        inputs_str = ["Activate tephra particle species",
                      "Deactivate tephra particle species"],
        )
x["DUST"] = readItem(
        'Off',
        'Indicate if `DUST` species are included',
        inputs = ["ON","OFF"],
        inputs_str = ["Activate dust particle species",
                      "Deactivate dust particle species"],
        )
x["H2O"] = readItem(
        'Off',
        'Indicate if `H2O` aerosol species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate H2O aerosol species and set the mass fraction in percent",
                      "Deactivate H2O aerosol species"],
        note = '''Aerosols can run independently or coupled with tephra species.
               If `TEPHRA` = `ON` it is assumed that the aerosol species are all of 
               magmatic origin mass fraction of `SO2` and `H2O` are relative to tephra.
               If `TEPHRA` = `OFF` then the mass fraction of aerosols must sum 1'''
        )
x["SO2"] = readItem(
        'On MASS_FRACTION_(%) = 1.',
        'Indicate if `SO2` aerosol species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate SO2 aerosol species and set the mass fraction in percent",
                      "Deactivate SO2 aerosol species"],
        note = '''Aerosols can run independently or coupled with tephra species.
               If `TEPHRA` = `ON` it is assumed that the aerosol species are all of 
               magmatic origin mass fraction of `SO2` and `H2O` are relative to tephra.
               If `TEPHRA` = `OFF` then the mass fraction of aerosols must sum 1'''
        )
x["CS134"] = readItem(
        'Off',
        'Indicate if `CS-134` radionuclide species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate Cs-134 radionuclide species and set the mass fraction in percent",
                      "Deactivate Cs-134 radionuclide species"],
        note = '''All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1'''
        )
x["CS137"] = readItem(
        'Off',
        'Indicate if `CS-137` radionuclide species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate Cs-137 radionuclide species and set the mass fraction in percent",
                      "Deactivate Cs-137 radionuclide species"],
        note = '''All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1'''
        )
x["I131"] = readItem(
        'Off',
        'Indicate if `I-131` radionuclide species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate I-131 radionuclide species and set the mass fraction in percent",
                      "Deactivate I-131 radionuclide species"],
        note = '''All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1'''
        )
x["SR90"] = readItem(
        'Off',
        'Indicate if `SR-90` radionuclide species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate Sr-90 radionuclide species and set the mass fraction in percent",
                      "Deactivate Sr-90 radionuclide species"],
        note = '''All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1'''
        )
x["Y90"] = readItem(
        'Off',
        'Indicate if `Y-90` radionuclide species are included',
        inputs = ["ON MASS_FRACTION_(%) = float","OFF"],
        inputs_str = ["Activate Y-90 radionuclide species and set the mass fraction in percent",
                      "Deactivate Y-90 radionuclide species"],
        note = '''All species of RADIONUCLIDES can run simultaneously but are incompatible with 
               species of category PARTICLES. Mass fraction of all radionuclides must sum 1'''
        )
data['SPECIES']['main']['params'] = x

######### SPECIES_TGSD #########
x = {}
x["NUMBER_OF_BINS"] = readItem(
        6,
        'Number of bins in the TGSD',
        inputs = ['integer'],
        note = 'This value can be different from the number of classes defined in the granulometry file `*.grn` created by the SetSrc task if aggregate bins are included later on or if a cut-off diameter is imposed',
        )
x["FI_RANGE"] = readItem(
        [-2, 8],
        'A list of two values defining the minimum and maximum diameters in phi units',
        inputs = ['float_list'],
        )
x["DENSITY_RANGE"] = readItem(
        [1200, 2300],
        'A list of two values defining the minimum and maximum densities',
        inputs = ['float_list'],
        note = 'Linear interpolation (in phi) is assumed between the extremes',
        )
x["SPHERICITY_RANGE"] = readItem(
        [0.9, 0.9],
        'A list of two values defining the minimum and maximum values for sphericity',
        inputs = ['float_list'],
        note = 'Linear interpolation (in phi) is assumed between the extremes',
        )
x["DISTRIBUTION"] = readItem(
        'GAUSSIAN',
        'Type of TGSD distribution',
        inputs = ["GAUSSIAN","BIGAUSSIAN","WEIBULL","BIWEIBULL","CUSTOM","ESTIMATE"],
        inputs_str = ["Gaussian distribution",
                      "bi-Gaussian distribution",
                      "Weibull distribution",
                      "bi-Weibull distribution",
                      "Custom distribution from an external user file",
                      "TGSD is estimated from column height and magma viscosity (for tephra species only)"
                      ],
        )
data['SPECIES_TGSD']['main']['params'] = x

x = {}
x["FI_MEAN"] = readItem(
        2.5,
        'Mean of the gaussian distribution in phi units',
        )
x["FI_DISP"] = readItem(
        1.5,
        'Standard deviation of the gaussian distribution in phi units',
        )
data['SPECIES_TGSD']['IF_GAUSSIAN']['params'] = x

x = {}
x["FI_MEAN"] = readItem(
        [0.25, 0.75],
        'A list of two values in phi units defining the means of two gaussian distributions',
        inputs = ['float_list'],
        )
x["FI_DISP"] = readItem(
        [1.44, 1.46],
        'A list of two values in phi units defining the standard deviations of two gaussian distributions',
        inputs = ['float_list'],
        )
x["MIXING_FACTOR"] = readItem(
        0.5,
        'The mixing factor between both gaussian distributions',
        )
data['SPECIES_TGSD']['IF_BIGAUSSIAN']['params'] = x

x = {}
x["FI_SCALE"] = readItem(
        1.0,
        'Parameter of the Weibull distribution',
        )
x["W_SHAPE"] = readItem(
        1.0,
        'Parameter of the Weibull distribution',
        )
data['SPECIES_TGSD']['IF_WEIBULL']['params'] = x

x = {}
x["FI_SCALE"] = readItem(
        [1.0,1.0],
        'List of parameters for the bi-Weibull distribution',
        inputs = ['float_list'],
        )
x["W_SHAPE"] = readItem(
        [1.0,1.0],
        'List of parameters for the bi-Weibull distribution',
        inputs = ['float_list'],
        )
x["MIXING_FACTOR"] = readItem(
        0.5,
        'Parameter of the bi-Weibull distribution',
        )
data['SPECIES_TGSD']['IF_BIWEIBULL']['params'] = x

x = {}
x["FILE"] = readItem(
        'my_file.tgsd',
        'Path to the file with the custom TGSD',
        inputs = ['string'],
        )
data['SPECIES_TGSD']['IF_CUSTOM']['params'] = x

x = {}
x["VISCOSITY_(PAS)"] = readItem(
        1E6,
        'Magma viscosity in Pa.s',
        )
x["HEIGHT_ABOVE_VENT_(M)"] = readItem(
        20000,
        'Eruption column height above vent in meters',
        )
data['SPECIES_TGSD']['IF_ESTIMATE']['params'] = x

######### PARTICLE_AGGREGATION #########
x = {}
x["PARTICLE_CUT_OFF"] = readItem(
        'NONE',
        'Specifies a cut off diamater',
        inputs = ["NONE","FI_LOWER_THAN float","FI_LARGER_THAN float","D_(MIC)_LARGER_THAN float","D_(MIC)_LOWER_THAN float"],
        inputs_str = ["No cut-off used",
                      "Define an upper cut-off limit in phi units",
                      "Define a lower cut-off limit in phi units",
                      "Define an upper cut-off limit for diameter in microns",
                      "Define a lower cut-off limit for diameter in microns",
                      ],
        )
x["AGGREGATION_MODEL"] = readItem(
        'PERCENTAGE',
        'Define a particle aggregation parametrization',
        inputs = ['NONE','CORNELL','COSTA','PERCENTAGE'],
        inputs_str = ["No aggregation",
                      "Cornell model (Cornell et al., 1983)",
                      "Costa model (Costa et al., 2010). Valid only for tephra particles and `PLUME` source type",
                      "Percentage model (Sulpizio et al., 2012)"
                      ],
        )
x["NUMBER_OF_AGGREGATE_BINS"] = readItem(
        2,
        'Number of aggregate bins (default value is 1)',
        inputs = ['integer'],
        note = 'Only used if `AGGREGATION_MODEL`!=`NONE`',
        )
x["DIAMETER_AGGREGATES_(MIC)"] = readItem(
        [300., 200.],
        'Diameter of aggregate bins in microns',
        inputs = ['float_list'],
        note = 'Only used if `AGGREGATION_MODEL`!=`NONE`',
        )
x["DENSITY_AGGREGATES_(KGM3)"] = readItem(
        [350., 250.],
        'Density of aggregate bins in kg m-3',
        inputs = ['float_list'],
        note = 'Only used if `AGGREGATION_MODEL`!=`NONE`',
        )
x["PERCENTAGE_(%)"] = readItem(
        [20., 10.],
        'Percentage of aggregate bins',
        inputs = ['float_list'],
        note = 'Only used if `AGGREGATION_MODEL`=`PERCENTAGE`',
        )
x["VSET_FACTOR"] = readItem(
        0.5,
        'Multiplicative correction factor for settling velocity of aggregates',
        note = 'Only used if `AGGREGATION_MODEL`=`PERCENTAGE`',
        )
x["FRACTAL_EXPONENT"] = readItem(
        2.99,
        'Fractal exponent (see Costa et al., 2010 for details)',
        note = 'Only used if `AGGREGATION_MODEL`=`COSTA`',
        )
data['PARTICLE_AGGREGATION']['main']['params'] = x

######### SOURCE #########
x = {}
x["SOURCE_TYPE"] = readItem(
        'TOP-HAT',
        'Type of source vertical distribution',
        inputs = ['POINT','SUZUKI','TOP-HAT','PLUME','RESUSPENSION'],
        inputs_str = ["Point source",
                      "Suzuki-type source",
                      "Top-hat source",
                      "Plume source (based on the 1D BPT)",
                      "Resuspension source"
                      ],
        )
x["SOURCE_START_(HOURS_AFTER_00)"] = readItem(
        0,
        'List of source start time for each phase',
        inputs = ['float_list'],
        note = 'Alternatively, a file can be provided with 3 columns specifying start time/end time/height above vent for each phase. This option is useful in case of many source phases'
        )
x["SOURCE_END_(HOURS_AFTER_00)"] = readItem(
        10,
        'List of source end time for each phase',
        inputs = ['float_list'],
        note = 'If a file is provided in `SOURCE_START_(HOURS_AFTER_00)` this line is ignored'
        )
x["LON_VENT"] = readItem(
        15.0,
        'Vent longitude',
        )
x["LAT_VENT"] = readItem(
        37.75,
        'Vent latitude'
        )
x["VENT_HEIGHT_(M)"] = readItem(
        3000.,
        'Height of the vent in meters a.s.l.'
        )
x["HEIGHT_ABOVE_VENT_(M)"] = readItem(
        6000.,
        'List of source heights in meters above the vent for each eruptive phase',
        inputs = ['float_list'],
        note = 'The plume heights must be lower than the top of the computational domain. If a file is provided in `SOURCE_START_(HOURS_AFTER_00)` this line is ignored',
        )
x["MASS_FLOW_RATE_(KGS)"] = readItem(
        'ESTIMATE-DEGRUYTER',
        'Defines the MFR or how the it should be computed (i.e. derived from column height)',
        inputs = ['float_list','ESTIMATE-MASTIN','ESTIMATE-WOODHOUSE','ESTIMATE-DEGRUYTER'],
        inputs_str = ['Mass flow rate in kg/s for each eruptive phase',
                      'MFR is computed using the empirical fit by Mastin et al. (2009)',
                      'MFR is computed using the empirical fit by Woodhouse et al. (2013)',
                      'MFR is computed using the empirical fit by Degruyter and Bonadonna (2012)'
                      ],
        )
x["ALFA_PLUME"] = readItem(
        0.1,
        'Entrainment coefficient',
        note = 'Only used if `MASS_FLOW_RATE_(KGS)` is set to `ESTIMATE-WOODHOUSE` or `ESTIMATE-DEGRUYTER`',
        )
x["BETA_PLUME"] = readItem(
        0.5,
        'Entrainment coefficient',
        note = 'Only used if `MASS_FLOW_RATE_(KGS)` is set to `ESTIMATE-WOODHOUSE` or `ESTIMATE-DEGRUYTER`',
        )
x["EXIT_TEMPERATURE_(K)"] = readItem(
        1200.,
        'Mixture temperature',
        note = 'Only used if `SOURCE_TYPE`=`PLUME` or to estimate MFR'
        )
x["EXIT_WATER_FRACTION_(%)"] = readItem(
        0.,
        'Total (magmatic) water fraction',
        note = 'Only used if `SOURCE_TYPE`=`PLUME` or to estimate MFR'
        )
data['SOURCE']['main']['params'] = x

x = {}
x["A"] = readItem(
        4,
        'List of values giving the parameter A in the Suzuki distribution (Pfeiffer et al., 2005) for each phase',
        inputs = ['float_list'],
        )
x["L"] = readItem(
        5,
        'List of values giving the parameter lambda in the Suzuki distribution (Pfeiffer et al., 2005) for each phase',
        inputs = ['float_list'],
        )
data['SOURCE']['IF_SUZUKI_SOURCE']['params'] = x

x = {}
x["THICKNESS_(M)"] = readItem(
        2000.,
        'List of thickness of the emission slab in meters for each phase',
        inputs = ['float_list'],
        )
data['SOURCE']['IF_TOP-HAT_SOURCE']['params'] = x

x = {}
x["SOLVE_PLUME_FOR"] = readItem(
        'MFR',
        'Configure the FPLUME model embedded in FALL3D',
        inputs = ['MFR','HEIGHT'],
        inputs_str = ['SetSrc solves for mass flow rate given the column height (inverse problem)',
                      'SetSrc solves for column height given the mass flow rate',
                      ]
        )
x["MFR_SEARCH_RANGE"] = readItem(
        [4.0, 9.0],
        'Two values n and m such that 10^n and 10^m specify the range of MFR values admitted in the iterative solving procedure',
        inputs = ['float_list'],
        note = 'Only used if `SOLVE_PLUME_FOR`=`MFR`',
        )
x["EXIT_VELOCITY_(MS)"] = readItem(
        180.,
        'List of values of the magma exit velocity in m/s at the vent for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_GAS_WATER_TEMPERATURE_(K)"] = readItem(
        1000.,
        'List of values of the exit gas water temperature in K for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_LIQUID_WATER_TEMPERATURE_(K)"] = readItem(
        1000.,
        'List of values of the exit liquid water temperature in K for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_SOLID_WATER_TEMPERATURE_(K)"] = readItem(
        1000.,
        'List of values of the exit solid water temperature in K for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_GAS_WATER_FRACTION_(%)"] = readItem(
        5.,
        'List of values of the exit gas water fraction in percent for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_LIQUID_WATER_FRACTION_(%)"] = readItem(
        0.,
        'List of values of the exit liquid water fraction in percent for each eruptive phase',
        inputs = ['float_list'],
        )
x["EXIT_SOLID_WATER_FRACTION_(%)"] = readItem(
        0.,
        'List of values of the exit solid water fraction in percent for each eruptive phase',
        inputs = ['float_list'],
        )
x["WIND_COUPLING"] = readItem(
        'yes',
        'If wind coupling is considered',
        inputs = ["YES","NO"],
        inputs_str = ["Enabled","Vertical wind velocity profile is assumed zero"],
        )
x["AIR_MOISTURE"] = readItem(
        'yes',
        'Air moisture',
        inputs = ["YES","NO"],
        inputs_str = ["Enabled","Dry entrained air only"],
        )
x["LATENT_HEAT"] = readItem(
        'no',
        'Latent heat',
        inputs = ["YES","NO"],
        inputs_str = ["Enabled","Latent heat contribution is neglected"],
        )
x["REENTRAINMENT"] = readItem(
        'yes',
        "Reentrainment",
        inputs = ["YES","NO"],
        inputs_str = ["Enabled","Particle reentrainment is neglected"],
        )
x["BURSIK_FACTOR"] = readItem(
        0.1,
        'Bursik factor xi',
        note = 'If not given, assumed equal to 0.1',
        )
x["Z_MIN_WIND"] = readItem(
        100,
        'Ignore wind entrainment below this zvalue (low jet region)',
        note = 'If not given, assumed equal to 100'
        )
x["C_UMBRELLA"] = readItem(
        1.2,
        'Thickness of umbrella region relative to Hb (>1)',
        note = 'If not given, assumed equal to 1.32'
        )

x["A_S"] = readItem(
        'CONSTANT 0.075 0.12',
        'Plume entrainment coefficient',
        inputs = ["CONSTANT float_list","KAMINSKI-R","KAMINSKI-C","OLD"],
        inputs_str = ["A list of two values for: (value jet, value plume)",
                      "TODO",
                      "TODO",
                      "TODO",
                      ],
        )
x["A_V"] = readItem(
        'CONSTANT 0.3',
        'Plume entrainment coefficient',
        inputs = ["CONSTANT float","TATE"],
        inputs_str = ["A constant value is assumed","TODO"]
        )
data['SOURCE']['IF_PLUME_SOURCE']['params'] = x

######### ENSEMBLE #########
x = {}
x["RANDOM_NUMBERS_FROM_FILE"] = readItem(
        'yes',
        'Indicate whether the ensemble should be reconstructed',
        inputs = ['YES','NO'],
        inputs_str = ["Read ensemble perturbations from `*.ens` files",
                      "Reconstruct the ensemble and generate new `*.ens` files"
                      ],
        note = 'If `*.ens` are not found, option `RANDOM_NUMBERS_FROM_FILE` = `NO` will be used'
        )
x["PERTURBATE_COLUMN_HEIGHT"] = readItem(
        'RELATIVE',
        'Perturbate eruption column height',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        )
x["PERTURBATE_MASS_FLOW_RATE"] = readItem(
        'NO',
        'Perturbate mass eruption rate',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = "This option is deactivated for `SOURCE_TYPE=PLUME` or `MASS_FLOW_RATE_(KGS)=ESTIMATE-*` options"
        )
x["PERTURBATE_SOURCE_START"] = readItem(
        'NO',
        'Perturbate the starting time of each eruptive phase',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        )
x["PERTURBATE_SOURCE_DURATION"] = readItem(
        'NO',
        'Perturbate the duration time of each eruptive phase',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        )
x["PERTURBATE_TOP-HAT_THICKNESS"] = readItem(
        'NO',
        'Perturbate the top-hat thickness',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `SOURCE_TYPE = TOP-HAT`',
        )
x["PERTURBATE_SUZUKI_A"] = readItem(
        'RELATIVE',
        'Perturbate the Suzuki coefficient `A`',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `SOURCE_TYPE = SUZUKI`'
        )
x["PERTURBATE_SUZUKI_L"] = readItem(
        'RELATIVE',
        'Perturbate the Suzuki coefficient `L`',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `SOURCE_TYPE = SUZUKI`'
        )
x["PERTURBATE_WIND"] = readItem(
        'NO',
        'Perturbate the horizontal wind components',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Zonal and meridional wind components are independently perturbed (two random numbers are generated)'
        )
x["PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT"] = readItem(
        'NO',
        'Perturbate the cloud insertion height',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `INITIAL_CONDITION = INSERTION`'
        )
x["PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS"] = readItem(
        'NO',
        'Perturbate the cloud insertion thickness',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `INITIAL_CONDITION = INSERTION`'
        )
x["PERTURBATE_FI_MEAN"] = readItem(
        'NO',
        'Perturbate the mean of the TGSD',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'For bimodal distributions `BIGAUSSIAN` and `ESTIMATE`, only the fine subpopulation is perturbed'
        )
x["PERTURBATE_DIAMETER_AGGREGATES_(MIC)"] = readItem(
        'NO',
        'Perturbate the diameter of the particle aggregates',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `AGGREGATION_MODEL` != `NONE`'
        )
x["PERTURBATE_DENSITY_AGGREGATES"] = readItem(
        'NO',
        'Perturbate the density of the particle aggregates',
        inputs = ['NO','RELATIVE','ABSOLUTE'],
        inputs_str = ["Disable perturbation",
                      "Perturbation range given as a fraction of the central value",
                      "Perturbation range given as an absolute value",
                      ],
        note = 'Only used if `AGGREGATION_MODEL` != `NONE`'
        )
data['ENSEMBLE']['main']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        20,
        'Define the perturbation range in meters (`PERTURBATE_COLUMN_HEIGHT` = `ABSOLUTE`) or percent (`PERTURBATE_COLUMN_HEIGHT` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_COLUMN_HEIGHT']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        20,
        'Define the perturbation range in kg/s (`PERTURBATE_MASS_FLOW_RATE` = `ABSOLUTE`) or percent (`PERTURBATE_MASS_FLOW_RATE` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_MASS_FLOW_RATE']['params'] = x
   
x = {}
x["PERTURBATION_RANGE"] = readItem(
        3600,
        'Define the perturbation range in s (`PERTURBATE_SOURCE_START` = `ABSOLUTE`) or percent (`PERTURBATE_SOURCE_START` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_SOURCE_START']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        3600,
        'Define the perturbation range in s (`PERTURBATE_SOURCE_DURATION` = `ABSOLUTE`) or percent (`PERTURBATE_SOURCE_DURATION` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_SOURCE_DURATION']['params'] = x
   
x = {}
x["PERTURBATION_RANGE"] = readItem(
        1000,
        'Define the perturbation range in meters (`PERTURBATE_TOP-HAT_THICKNESS` = `ABSOLUTE`) or percent (`PERTURBATE_TOP-HAT_THICKNESS` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_TOP-HAT_THICKNESS']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        25,
        'Define the perturbation range of the dimensionless parameter `A` in absolute units (`IF_PERTURBATE_SUZUKI_A` = `ABSOLUTE`) or percent (`IF_PERTURBATE_SUZUKI_A` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_SUZUKI_A']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        25,
        'Define the perturbation range of the dimensionless parameter `L` in absolute units (`IF_PERTURBATE_SUZUKI_L` = `ABSOLUTE`) or percent (`IF_PERTURBATE_SUZUKI_L` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_SUZUKI_L']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        10.0,
        'Define the perturbation range in m/s (`PERTURBATE_WIND` = `ABSOLUTE`) or percent (`PERTURBATE_WIND` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_WIND']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        20,
        'Define the perturbation range in meters (`IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` = `ABSOLUTE`) or percent (`IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        20,
        'Define the perturbation range in meters (`PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` = `ABSOLUTE`) or percent (`PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        1,
        'Define the perturbation range in phi units (`PERTURBATE_FI_MEAN` = `ABSOLUTE`) or percent (`PERTURBATE_FI_MEAN` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_FI_MEAN']['params'] = x
   
x = {}
x["PERTURBATION_RANGE"] = readItem(
        100,
        'Define the perturbation range in microns (`PERTURBATE_DIAMETER_AGGREGATES_(MIC)` = `ABSOLUTE`) or percent (`PERTURBATE_DIAMETER_AGGREGATES_(MIC)` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_DIAMETER_AGGREGATES_(MIC)']['params'] = x

x = {}
x["PERTURBATION_RANGE"] = readItem(
        100,
        'Define the perturbation range in kg/m3 (`PERTURBATE_DENSITY_AGGREGATES` = `ABSOLUTE`) or percent (`PERTURBATE_DENSITY_AGGREGATES` = `RELATIVE`)',
        )
x["PDF"] = readItem(
        'UNIFORM',
        'Define the sampling Probability Density Function (PDF)',
        inputs = ['UNIFORM','GAUSSIAN'],
        inputs_str = ["Uniform (constant) PDF",
                      "Gaussian PDF centred at the reference value"
                      ],
        )
data['ENSEMBLE']['IF_PERTURBATE_DENSITY_AGGREGATES']['params'] = x

######### ENSEMBLE_POST #########
x = {}
x["POSTPROCESS_MEMBERS"] = readItem(
        'no',
        'Define if individual member results are included in the output file `.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include individual members",
                      "Exclude individual members"
                      ],
        note = 'The option `POSTPROCESS_MEMBERS` = `YES` may result in very large files'
        )
x["POSTPROCESS_MEAN"] = readItem(
        'no',
        'Postprocess the ensemble mean in the output file `*.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include ensemble mean",
                      "Exclude ensemble mean"
                      ],
        )
x["POSTPROCESS_LOGMEAN"] = readItem(
        'no',
        'Postprocess the ensemble (log) mean in the output file `*.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include ensemble (log) mean",
                      "Exclude ensemble (log) mean"
                      ],
        )
x["POSTPROCESS_MEDIAN"] = readItem(
        'yes',
        'Postprocess the ensemble median in the output file `*.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include ensemble median",
                      "Exclude ensemble median"
                      ],
        )
x["POSTPROCESS_STANDARD_DEV"] = readItem(
        'no',
        'Postprocess the ensemble standard deviation in the output file `*.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include ensemble standard deviation",
                      "Exclude ensemble standard deviation"
                      ],
        )
x["POSTPROCESS_PROBABILITY"] = readItem(
        'no',
        'Generate probabilistic forecasts based on thresholds (see below) in the output file `*.ens.nc`. Probabilities are obtained by counting the fraction of ensemble members exceeding a given threshold',
        inputs = ['YES','NO'],
        inputs_str = ["Include probabilistic forecasts",
                      "Exclude probabilistic forecasts"
                      ],
        )
x["POSTPROCESS_PERCENTILES"] = readItem(
        'no',
        'Generate percentile forecasts in the output file `*.ens.nc`',
        inputs = ['YES','NO'],
        inputs_str = ["Include percentile forecasts",
                      "Exclude percentile forecasts"
                      ],
        )
data['ENSEMBLE_POST']['main']['params'] = x

x = {}
x["CONCENTRATION_THRESHOLDS_(MG/M3)"] = readItem(
        2,
        'List of values giving the concentration threshold',
        inputs = ['float_list'],
        note = 'A probability forecast map is generated for each value'
        )
x["COLUMN_MASS_THRESHOLDS_(G/M2)"] = readItem(
        1,
        'List of values giving the column mass threshold for species `TEPHRA`',
        inputs = ['float_list'],
        note = 'A probability forecast map is generated for each value'
        )
x["COLUMN_MASS_THRESHOLDS_(DU)"] = readItem(
        100,
        'List of values giving the column mass threshold for species `SO2`',
        inputs = ['float_list'],
        note = 'A probability forecast map is generated for each value'
        )
x["GROUND_LOAD_THRESHOLDS_(KG/M2)"] = readItem(
        1,
        'List of values giving the deposit ground load threshold for species `TEPHRA`',
        inputs = ['float_list'],
        note = 'A probability forecast map is generated for each value'
        )
data['ENSEMBLE_POST']['IF_POSTPROCESS_PROBABILITY']['params'] = x
      
x = {}
x["PERCENTILE_VALUES_(%)"] = readItem(
        50,
        'List of values giving the percentiles in percent',
        inputs = ['float_list'],
        note = 'A percentile forecast map is generated for each value'
        )
data['ENSEMBLE_POST']['IF_POSTPROCESS_PERCENTILES']['params'] = x

######### MODEL_PHYSICS #########
x = {}
x["LIMITER"] = readItem(
        'SUPERBEE',
        'Flux limiter option',
        inputs = ['MINMOD','SUPERBEE','OSPRE'],
        inputs_str = ["MINMOD option",
                      "SUPERBEE option",
                      "OSPRE option",
                      ],
        )
x["TIME_MARCHING"] = readItem(
        'RUNGE-KUTTA',
        'Time integration scheme',
        inputs = ['EULER','RUNGE-KUTTA'],
        inputs_str = ["Euler (1st order)","Runge-Kutta (4th order)"],
        )
x["CFL_CRITERION"] = readItem(
        'ALL_DIMENSIONS',
        'Courant criterion (critical time step)',
        inputs = ['ONE_DIMENSIONAL','ALL_DIMENSIONS'],
        inputs_str = ["Minimum for each dimension","Minimum for all dimensions (default)"]
        )
x["CFL_SAFETY_FACTOR"] = readItem(
        0.9,
        'Safety factor for critical time step',
        required = False,
        note = 'Default = 0.9'
        )
x["TERMINAL_VELOCITY_MODEL"] = readItem(
        'GANSER',
        'Parametrization for terminal settling velocity estimation',
        inputs = ['ARASTOOPOUR','GANSER','WILSON','DELLINO','PFEIFFER','DIOGUARDI2017','DIOGUARDI2018'],
        inputs_str = ['TODO']*7,
        )
x["HORIZONTAL_TURBULENCE_MODEL"] = readItem(
        'CMAQ',
        'Type of parametrization used to compute the horizontal diffusion',
        inputs = ['CONSTANT float', 'CMAQ', 'RAMS'],
        inputs_str = ['Constant diffusion coefficient in m^2/s', 'CMAQ option', 'RAMS option'],
        )
x["VERTICAL_TURBULENCE_MODEL"] = readItem(
        'SIMILARITY',
        'Type of parametrization used to compute the vertical diffusion',
        inputs = ['CONSTANT float', 'SIMILARITY'],
        inputs_str = ['Constant diffusion coefficient in m^2/s', 'Equations based on similarity theory'],
        )
x["RAMS_CS"] = readItem(
        0.2275,
        'Parameter CS required by the horizontal diffusion parametrization',
        note = 'Only used if `HORIZONTAL_TURBULENCE_MODEL` = `RAMS`'
        )
x["WET_DEPOSITION"] = readItem(
        'no',
        'Defines whether wet deposition model is enabled',
        inputs = ['YES','NO'],
        inputs_str = ["Enable wet deposition","Disable wet deposition"],
        )
x["DRY_DEPOSITION"] = readItem(
        'no',
        'Defines whether dry deposition model is enabled',
        inputs = ['YES','NO'],
        inputs_str = ["Enable dry deposition","Disable dry deposition"],
        )
x["GRAVITY_CURRENT"] = readItem(
        'no',
        'Define whether the gravity current model is enabled',
        inputs = ['YES','NO'],
        inputs_str = ["Enable gravity current","Disable gravity current"],
        note = 'Only for `TEPHRA` species',
        )
data['MODEL_PHYSICS']['main']['params'] = x

x = {}
x["C_FLOW_RATE"] = readItem(
        870,
        'Empirical constant for volumetric flow rate at the Neutral Buoyancy Level (NBL)'
        )
x["LAMBDA_GRAV"] = readItem(
        0.2,
        'Empirical constant for the gravity current model'
        )
x["K_ENTRAIN"] = readItem(
        0.1,
        'Entrainment coefficient for the gravity current model'
        )
x["BRUNT_VAISALA"] = readItem(
        0.02,
        'Brunt-Väisälä frequency accounting for ambient stratification'
        )
x["GC_START_(HOURS_AFTER_00)"] = readItem(
        0,
        'Gravity current starting time'
        )
x["GC_END_(HOURS_AFTER_00)"] = readItem(
        3,
        'Gravity current end time'
        )
data['MODEL_PHYSICS']['IF_GRAVITY_CURRENT']['params'] = x

######### MODEL_OUTPUT #########
x = {}
x["PARALLEL_IO"] = readItem(
        'no',
        'Use parallel Input/Output for netCDF files',
        inputs = ['YES','NO'],
        inputs_str = ['Enable parallel I/O','Disable parallel I/O'],
        note = 'The option `PARALLEL_IO` = `YES` requires the installation of the high-performance parallel I/O library for accessing NetCDF files'
        )
x["LOG_FILE_LEVEL"] = readItem(
        'FULL',
        'Level of detail in the FALL3D log file',
        inputs = ['NONE','NORMAL','FULL'],
        inputs_str = ['TODO']*3,
        )
x["RESTART_TIME_INTERVAL_(HOURS)"] = readItem(
        'END_ONLY',
        'Define the restart file output frequency',
        inputs = ['float','NONE','END_ONLY'],
        inputs_str = ['Frequency in hours','Restart file not written','Restart file written only at the end of the simulation'],
        )
x["OUTPUT_JSON_FILES"] = readItem(
        'no',
        'Generate json files',
        inputs = ['YES','NO'],
        inputs_str = ["Generate json files","Do not generate json files"],
        required = False,
        note = 'Only for tasks SetTGSD SetDbs SetSrc FALL3D'
        )
x["OUTPUT_INTERMEDIATE_FILES"] = readItem(
        'no',
        'Generate intermediate files',
        inputs = ['YES','NO'],
        inputs_str = ["Generate intermediate files","Do not generate intermediate files"],
        note = 'Only for task ALL can be set `OUTPUT_INTERMEDIATE_FILES` = `NO`'
        )
x["OUTPUT_TIME_START_(HOURS)"] = readItem(
        'RUN_START',
        'Start time for output',
        inputs = ['float','RUN_START'],
        inputs_str = ['Time (in hours) from which the output file is written',
                      'Start writing output file from the beginning']
        )
x["OUTPUT_TIME_INTERVAL_(HOURS)"] = readItem(
        1.,
        'Time period of model output in hours',
        )
x["OUTPUT_3D_CONCENTRATION"] = readItem(
        'no',
        'Specify whether the 3D concentration field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the 3D concentration field",
                      "Do not write the 3D concentration field"],
        )
x["OUTPUT_3D_CONCENTRATION_BINS"] = readItem(
        'no',
        'Specify whether the 3D concentration field for each bin should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the 3D concentration field for each bin",
                      "Do not write the 3D concentration field for each bin"],
        )
x["OUTPUT_SURFACE_CONCENTRATION"] = readItem(
        'yes',
        'Specify whether the surface concentration field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the surface concentration field","Do not write the surface concentration field"],
        )
x["OUTPUT_COLUMN_LOAD"] = readItem(
        'yes',
        'Specify whether the column mass load field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the column mass load field","Do not write the column mass load field"],
        )
x["OUTPUT_COLUMN_LOAD_PM"] = readItem(
        'yes',
        'Specify whether the column mass load field for PM2.5, PM10, PM20 and PM64 should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the PM column mass load field","Do not write the PM column mass load field"],
        )
x["OUTPUT_CLOUD_TOP"] = readItem(
        'yes',
        'Specify whether the cloud top height field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the cloud top height field","Do not write the cloud top height field"],
        )
x["OUTPUT_GROUND_LOAD"] = readItem(
        'yes',
        'Specify whether the deposit mass load field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the deposit mass load field",
                      "Do not write the deposit mass load field"],
        )
x["OUTPUT_GROUND_LOAD_BINS"] = readItem(
        'no',
        'Specify whether the deposit mass load field for each bin should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the deposit mass load field for each bin",
                      "Do not write the deposit mass load field for each bin"],
        )
x["OUTPUT_WET_DEPOSITION"] = readItem(
        'no',
        'Specify whether the wet deposition mass field should be written in the output file',
        inputs = ['YES','NO'],
        inputs_str = ["Write the wet deposition mass field","Do not write the wet deposition mass field"],
        )
x["TRACK_POINTS"] = readItem(
        'no',
        'Specifies whether the timeseries for the tracking points should be written',
        inputs = ['YES','NO'],
        inputs_str = ['Generate tracking point file',
                      'Do not generate tracking point file'],
        )
x["TRACK_POINTS_FILE"] = readItem(
        '',
        'Path to the file with the list of tracked points',
        inputs = ['string'],
        note = 'Used only if `TRACK_POINTS_FILE` = `YES`'
        )
x["OUTPUT_CONCENTRATION_AT_XCUTS"] = readItem(
        'no',
        'Output concentration at x-coordinate values specified in `X-VALUES`',
        inputs = ['YES','NO'],
        inputs_str = ['Output concentration cuts','Do not output concentration cuts'],
        )
x["OUTPUT_CONCENTRATION_AT_YCUTS"] = readItem(
        'no',
        'Output concentration at y-coordinate values specified in `Y-VALUES`',
        inputs = ['YES','NO'],
        inputs_str = ['Output concentration cuts','Do not output concentration cuts'],
        )
x["OUTPUT_CONCENTRATION_AT_ZCUTS"] = readItem(
        'no',
        'Output concentration at z-coordinate values specified in `Z-VALUES`',
        inputs = ['YES','NO'],
        inputs_str = ['Output concentration cuts','Do not output concentration cuts'],
        )
x["OUTPUT_CONCENTRATION_AT_FL"] = readItem(
        'yes',
        'Output concentration at flight level values specified in `FL-VALUES`',
        inputs = ['YES','NO'],
        inputs_str = ['Output concentration cuts','Do not output concentration cuts'],
        )
x["X-VALUES"] = readItem(
        15,
        'List of x-coordinate values read when `OUTPUT_CONCENTRATION_AT_XCUTS` = `YES`',
        inputs = ['float_list']
        )
x["Y-VALUES"] = readItem(
        37.5,
        'List of y-coordinate values read when `OUTPUT_CONCENTRATION_AT_YCUTS` = `YES`',
        inputs = ['float_list']
        )
x["Z-VALUES"] = readItem(
        [5000],
        'List of z-coordinate values read when `OUTPUT_CONCENTRATION_AT_ZCUTS` = `YES`',
        inputs = ['float_list']
        )
x["FL-VALUES"] = readItem(
        [50, 100, 150, 200, 250, 300, 350, 400],
        'List of flight level values read when `OUTPUT_CONCENTRATION_AT_FL` = `YES`',
        inputs = ['float_list']
        )
x["OUTPUT_CONCENTRATION_IN_LAYERS"] = readItem(
        'yes',
        'Output layer-averaged concentration for a list of layers specified in `FL-LAYER-TOP` and `FL-LAYER-BOTTOM`',
        inputs = ['YES','NO'],
        inputs_str = ['Output layer-averaged concentration','Do not output layer-averaged concentration cuts'],
        )
x["FL-LAYER-TOP"] = readItem(
        [180,220,275],
        'List of flight level values for layer top read when `OUTPUT_CONCENTRATION_IN_LAYERS` = `YES`',
        inputs = ['float_list']
        )
x["FL-LAYER-BOTTOM"] = readItem(
        [120,180,225],
        'List of flight level values for layer bottom read when `OUTPUT_CONCENTRATION_IN_LAYERS` = `YES`',
        inputs = ['float_list']
        )
x["OUTPUT_FOOTPRINTS"] = readItem(
        'no',
        'Output footprint file',
        inputs = ['YES','NO'],
        inputs_str = ['Output footprint file','Do not output footprint file'],
        )
x["CONCENTRATION_INTENSITY_MEASURE_(MG/M3)"] = readItem(
        [2,10],
        'List of concentration values read when `OUTPUT_FOOTPRINTS` = `YES`',
        inputs = ['float_list']
        )
x["GROUND_LOAD_INTENSITY_MEASURE_(KG/M2)"] = readItem(
        [1,10,100],
        'List of ground mass load values read when `OUTPUT_FOOTPRINTS` = `YES`',
        inputs = ['float_list']
        )
data['MODEL_OUTPUT']['main']['params'] = x

######### MODEL_VALIDATION #########
x = {}
x["OBSERVATIONS_TYPE"] = readItem(
        'SATELLITE_RETRIEVAL',
        'Type of observations',
        inputs = ["SATELLITE_DETECTION","SATELLITE_RETRIEVAL","DEPOSIT_CONTOURS","DEPOSIT_POINTS"],
        inputs_str = ["Satellite yes/no flag contours",
                      "Satellite quantitative retrievals of `TEPHRA`/`SO2` species",
                      "Deposit isopach/isopleth contours read from a gridded netCDF file",
                      "Deposit point-wise observations"
                      ],
        )
x["OBSERVATIONS_FILE"] = readItem(
        'Example-8.0.sat.nc',
        'Path to the files with observations',
        inputs = ['string'],
        )
x["OBSERVATIONS_DICTIONARY_FILE"] = readItem(
        'Sat.tbl',
        'Path to the observations dictionary file',
        inputs = ['string'],
        required = False,
        note = 'Examples can be found in `Other/Dep`',
        )
x["RESULTS_FILE"] = readItem(
        'Example.ens.nc',
        'Path to the FALL3D output file with simulation results (`*.res.nc` or `*.ens.nc`)',
        inputs = ['string'],
        )
data['MODEL_VALIDATION']['main']['params'] = x

x = {}
x["COLUMN_MASS_OBSERVATION_THRESHOLD_(G/M2)"] = readItem(
        0.1,
        'Column mass threshold in g/m2 for `TEPHRA` species',
        note = 'It should be consistent with `COLUMN_MASS_THRESHOLDS_(G/M2)` for ensemble runs',
        )
x["COLUMN_MASS_OBSERVATION_THRESHOLD_(DU)"] = readItem(
        1,
        'Column mass threshold in DU for `SO2` species',
        note = 'It should be consistent with `COLUMN_MASS_THRESHOLDS_(DU)` for ensemble runs',
        )
data['MODEL_VALIDATION']['IF_OBSERVATIONS_TYPE_SATELLITE']['params'] = x
   
x = {}
x["GROUND_LOAD_OBSERVATION_THRESHOLD_(KG/M2)"] = readItem(
        0.1,
        'Deposit load threshold in kg/m2 for `TEPHRA` species',
        note = 'It should be consistent with `GROUND_LOAD_THRESHOLDS_(KG/M2)` for ensemble runs',
        )
data['MODEL_VALIDATION']['IF_OBSERVATIONS_TYPE_DEPOSIT']['params'] = x

######### Save data
with open(fname,'w') as f:
    json.dump(data,f, indent=2)
