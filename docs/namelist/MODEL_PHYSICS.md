# Block `MODEL_PHYSICS`
This block defines the specific variables related to physics in the FALL3D model
1. `LIMITER` = (options)
   * Flux limiter option
   * Input options:
      - `MINMOD`: MINMOD option
      - `SUPERBEE`: SUPERBEE option
      - `OSPRE`: OSPRE option
1. `TIME_MARCHING` = (options)
   * Time integration scheme
   * Input options:
      - `EULER`: Euler (1st order)
      - `RUNGE-KUTTA`: Runge-Kutta (4th order)
1. `CFL_CRITERION` = (options)
   * Courant criterion (critical time step)
   * Input options:
      - `ONE_DIMENSIONAL`: Minimum for each dimension
      - `ALL_DIMENSIONS`: Minimum for all dimensions (default)
1. `CFL_SAFETY_FACTOR` = (float)
   * Safety factor for critical time step
   * Optional
   * Note: Default = 0.9
1. `TERMINAL_VELOCITY_MODEL` = (options)
   * Parametrization for terminal settling velocity estimation
   * Input options:
      - `ARASTOOPOUR`: TODO
      - `GANSER`: TODO
      - `WILSON`: TODO
      - `DELLINO`: TODO
      - `PFEIFFER`: TODO
      - `DIOGUARDI2017`: TODO
      - `DIOGUARDI2018`: TODO
1. `HORIZONTAL_TURBULENCE_MODEL` = (options)
   * Type of parametrization used to compute the horizontal diffusion
   * Input options:
      - `CONSTANT` (float): Constant diffusion coefficient in m^2/s
      - `CMAQ`: CMAQ option
      - `RAMS`: RAMS option
1. `VERTICAL_TURBULENCE_MODEL` = (options)
   * Type of parametrization used to compute the vertical diffusion
   * Input options:
      - `CONSTANT` (float): Constant diffusion coefficient in m^2/s
      - `SIMILARITY`: Equations based on similarity theory
1. `RAMS_CS` = (float)
   * Parameter CS required by the horizontal diffusion parametrization
   * Note: Only used if `HORIZONTAL_TURBULENCE_MODEL` = `RAMS`
1. `WET_DEPOSITION` = (options)
   * Defines whether wet deposition model is enabled
   * Input options:
      - `YES`: Enable wet deposition
      - `NO`: Disable wet deposition
1. `DRY_DEPOSITION` = (options)
   * Defines whether dry deposition model is enabled
   * Input options:
      - `YES`: Enable dry deposition
      - `NO`: Disable dry deposition
1. `GRAVITY_CURRENT` = (options)
   * Define whether the gravity current model is enabled
   * Input options:
      - `YES`: Enable gravity current
      - `NO`: Disable gravity current
   * Note: Only for `TEPHRA` species
## Sub-block `IF_GRAVITY_CURRENT`
Used when `GRAVITY_CURRENT` = `YES`
1. `C_FLOW_RATE` = (float)
   * Empirical constant for volumetric flow rate at the Neutral Buoyancy Level (NBL)
1. `LAMBDA_GRAV` = (float)
   * Empirical constant for the gravity current model
1. `K_ENTRAIN` = (float)
   * Entrainment coefficient for the gravity current model
1. `BRUNT_VAISALA` = (float)
   * Brunt-Väisälä frequency accounting for ambient stratification
1. `GC_START_(HOURS_AFTER_00)` = (float)
   * Gravity current starting time
1. `GC_END_(HOURS_AFTER_00)` = (float)
   * Gravity current end time
