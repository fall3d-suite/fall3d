# Block `MODEL_OUTPUT`
This block is read by task FALL3D and defines specific variables related to output strategy
1. `PARALLEL_IO` = (options)
   * Use parallel Input/Output for netCDF files
   * Input options:
      - `YES`: Enable parallel I/O
      - `NO`: Disable parallel I/O
   * Note: The option `PARALLEL_IO` = `YES` requires the installation of the high-performance parallel I/O library for accessing NetCDF files
1. `LOG_FILE_LEVEL` = (options)
   * Level of detail in the FALL3D log file
   * Input options:
      - `NONE`: TODO
      - `NORMAL`: TODO
      - `FULL`: TODO
1. `RESTART_TIME_INTERVAL_(HOURS)` = (options)
   * Define the restart file output frequency
   * Input options:
      - (float): Frequency in hours
      - `NONE`: Restart file not written
      - `END_ONLY`: Restart file written only at the end of the simulation
1. `OUTPUT_JSON_FILES` = (options)
   * Generate json files
   * Optional
   * Input options:
      - `YES`: Generate json files
      - `NO`: Do not generate json files
   * Note: Only for tasks SetTGSD SetDbs SetSrc FALL3D
1. `OUTPUT_INTERMEDIATE_FILES` = (options)
   * Generate intermediate files
   * Input options:
      - `YES`: Generate intermediate files
      - `NO`: Do not generate intermediate files
   * Note: Only for task ALL can be set `OUTPUT_INTERMEDIATE_FILES` = `NO`
1. `OUTPUT_TIME_START_(HOURS)` = (options)
   * Start time for output
   * Input options:
      - (float): Time (in hours) from which the output file is written
      - `RUN_START`: Start writing output file from the beginning
1. `OUTPUT_TIME_INTERVAL_(HOURS)` = (float)
   * Time period of model output in hours
1. `OUTPUT_3D_CONCENTRATION` = (options)
   * Specify whether the 3D concentration field should be written in the output file
   * Input options:
      - `YES`: Write the 3D concentration field
      - `NO`: Do not write the 3D concentration field
1. `OUTPUT_3D_CONCENTRATION_BINS` = (options)
   * Specify whether the 3D concentration field for each bin should be written in the output file
   * Input options:
      - `YES`: Write the 3D concentration field for each bin
      - `NO`: Do not write the 3D concentration field for each bin
1. `OUTPUT_SURFACE_CONCENTRATION` = (options)
   * Specify whether the surface concentration field should be written in the output file
   * Input options:
      - `YES`: Write the surface concentration field
      - `NO`: Do not write the surface concentration field
1. `OUTPUT_COLUMN_LOAD` = (options)
   * Specify whether the column mass load field should be written in the output file
   * Input options:
      - `YES`: Write the column mass load field
      - `NO`: Do not write the column mass load field
1. `OUTPUT_COLUMN_LOAD_PM` = (options)
   * Specify whether the column mass load field for PM2.5, PM10, PM20 and PM64 should be written in the output file
   * Input options:
      - `YES`: Write the PM column mass load field
      - `NO`: Do not write the PM column mass load field
1. `OUTPUT_CLOUD_TOP` = (options)
   * Specify whether the cloud top height field should be written in the output file
   * Input options:
      - `YES`: Write the cloud top height field
      - `NO`: Do not write the cloud top height field
1. `OUTPUT_GROUND_LOAD` = (options)
   * Specify whether the deposit mass load field should be written in the output file
   * Input options:
      - `YES`: Write the deposit mass load field
      - `NO`: Do not write the deposit mass load field
1. `OUTPUT_GROUND_LOAD_BINS` = (options)
   * Specify whether the deposit mass load field for each bin should be written in the output file
   * Input options:
      - `YES`: Write the deposit mass load field for each bin
      - `NO`: Do not write the deposit mass load field for each bin
1. `OUTPUT_WET_DEPOSITION` = (options)
   * Specify whether the wet deposition mass field should be written in the output file
   * Input options:
      - `YES`: Write the wet deposition mass field
      - `NO`: Do not write the wet deposition mass field
1. `TRACK_POINTS` = (options)
   * Specifies whether the timeseries for the tracking points should be written
   * Input options:
      - `YES`: Generate tracking point file
      - `NO`: Do not generate tracking point file
1. `TRACK_POINTS_FILE` = (string)
   * Path to the file with the list of tracked points
   * Note: Used only if `TRACK_POINTS_FILE` = `YES`
1. `OUTPUT_CONCENTRATION_AT_XCUTS` = (options)
   * Output concentration at x-coordinate values specified in `X-VALUES`
   * Input options:
      - `YES`: Output concentration cuts
      - `NO`: Do not output concentration cuts
1. `OUTPUT_CONCENTRATION_AT_YCUTS` = (options)
   * Output concentration at y-coordinate values specified in `Y-VALUES`
   * Input options:
      - `YES`: Output concentration cuts
      - `NO`: Do not output concentration cuts
1. `OUTPUT_CONCENTRATION_AT_ZCUTS` = (options)
   * Output concentration at z-coordinate values specified in `Z-VALUES`
   * Input options:
      - `YES`: Output concentration cuts
      - `NO`: Do not output concentration cuts
1. `OUTPUT_CONCENTRATION_AT_FL` = (options)
   * Output concentration at flight level values specified in `FL-VALUES`
   * Input options:
      - `YES`: Output concentration cuts
      - `NO`: Do not output concentration cuts
1. `X-VALUES` = (float_list)
   * List of x-coordinate values read when `OUTPUT_CONCENTRATION_AT_XCUTS` = `YES`
1. `Y-VALUES` = (float_list)
   * List of y-coordinate values read when `OUTPUT_CONCENTRATION_AT_YCUTS` = `YES`
1. `Z-VALUES` = (float_list)
   * List of z-coordinate values read when `OUTPUT_CONCENTRATION_AT_ZCUTS` = `YES`
1. `FL-VALUES` = (float_list)
   * List of flight level values read when `OUTPUT_CONCENTRATION_AT_FL` = `YES`
1. `OUTPUT_CONCENTRATION_IN_LAYERS` = (options)
   * Output layer-averaged concentration for a list of layers specified in `FL-LAYER-TOP` and `FL-LAYER-BOTTOM`
   * Input options:
      - `YES`: Output layer-averaged concentration
      - `NO`: Do not output layer-averaged concentration cuts
1. `FL-LAYER-TOP` = (float_list)
   * List of flight level values for layer top read when `OUTPUT_CONCENTRATION_IN_LAYERS` = `YES`
1. `FL-LAYER-BOTTOM` = (float_list)
   * List of flight level values for layer bottom read when `OUTPUT_CONCENTRATION_IN_LAYERS` = `YES`
1. `OUTPUT_FOOTPRINTS` = (options)
   * Output footprint file
   * Input options:
      - `YES`: Output footprint file
      - `NO`: Do not output footprint file
1. `CONCENTRATION_INTENSITY_MEASURE_(MG/M3)` = (float_list)
   * List of concentration values read when `OUTPUT_FOOTPRINTS` = `YES`
1. `GROUND_LOAD_INTENSITY_MEASURE_(KG/M2)` = (float_list)
   * List of ground mass load values read when `OUTPUT_FOOTPRINTS` = `YES`
