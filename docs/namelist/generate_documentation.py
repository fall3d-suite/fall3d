import json

with open('namelist.json') as f:
    data = json.load(f)

def getFormatted(input_string):
    valid_types = ['float','float_list','integer','string']
    input_list = ["({})".format(item) if item in valid_types
             else '`{}`'.format(item) for item in input_string.split()]
    input_format = " ".join(input_list)
    return input_format

def to_markdown(name,block):
    output = "# Block `{}`\n".format(name)
    for sub_name, sub_block in block.items():
        if sub_name != 'main':
            output += "## Sub-block `{}`\n".format(sub_name)
        output += "{}\n".format(sub_block['name'])
        data = sub_block['params']
        ###
        ### Iterate over parameters
        ###
        for item_name, item in data.items():
            input_number = len(item['inputs'])
            if input_number == 1:
                input_fmt = getFormatted(item['inputs'][0])
                output += "1. `{}` = {}\n".format(item_name,input_fmt)
            else:
                output += "1. `{}` = (options)\n".format(item_name)
            output += "   * {}\n".format(item['description'])
            if not item['required']: output += "   * Optional\n"
            if input_number > 1:
                output += "   * Input options:\n"
                for (opt_str,opt_desc) in zip(item['inputs'],item['inputs_str']):
                    opt_fmt = getFormatted(opt_str)
                    output += "      - {}: {}\n".format(opt_fmt,opt_desc)
            if item['note']:
                output += '   * Note: {}\n'.format(item['note'])
    return output

for block_name, block in data.items():
    fname = "{}.md".format(block_name)
    with open(fname,'w') as f:
        f.write(to_markdown(block_name,block))
