# Block `ENSEMBLE_POST`
The block is used by the task PosEns for post-processing ensemble runs
1. `POSTPROCESS_MEMBERS` = (options)
   * Define if individual member results are included in the output file `.ens.nc`
   * Input options:
      - `YES`: Include individual members
      - `NO`: Exclude individual members
   * Note: The option `POSTPROCESS_MEMBERS` = `YES` may result in very large files
1. `POSTPROCESS_MEAN` = (options)
   * Postprocess the ensemble mean in the output file `*.ens.nc`
   * Input options:
      - `YES`: Include ensemble mean
      - `NO`: Exclude ensemble mean
1. `POSTPROCESS_LOGMEAN` = (options)
   * Postprocess the ensemble (log) mean in the output file `*.ens.nc`
   * Input options:
      - `YES`: Include ensemble (log) mean
      - `NO`: Exclude ensemble (log) mean
1. `POSTPROCESS_MEDIAN` = (options)
   * Postprocess the ensemble median in the output file `*.ens.nc`
   * Input options:
      - `YES`: Include ensemble median
      - `NO`: Exclude ensemble median
1. `POSTPROCESS_STANDARD_DEV` = (options)
   * Postprocess the ensemble standard deviation in the output file `*.ens.nc`
   * Input options:
      - `YES`: Include ensemble standard deviation
      - `NO`: Exclude ensemble standard deviation
1. `POSTPROCESS_PROBABILITY` = (options)
   * Generate probabilistic forecasts based on thresholds (see below) in the output file `*.ens.nc`. Probabilities are obtained by counting the fraction of ensemble members exceeding a given threshold
   * Input options:
      - `YES`: Include probabilistic forecasts
      - `NO`: Exclude probabilistic forecasts
1. `POSTPROCESS_PERCENTILES` = (options)
   * Generate percentile forecasts in the output file `*.ens.nc`
   * Input options:
      - `YES`: Include percentile forecasts
      - `NO`: Exclude percentile forecasts
## Sub-block `IF_POSTPROCESS_PROBABILITY`
Used when `POSTPROCESS_PROBABILITY` = `YES`
1. `CONCENTRATION_THRESHOLDS_(MG/M3)` = (float_list)
   * List of values giving the concentration threshold
   * Note: A probability forecast map is generated for each value
1. `COLUMN_MASS_THRESHOLDS_(G/M2)` = (float_list)
   * List of values giving the column mass threshold for species `TEPHRA`
   * Note: A probability forecast map is generated for each value
1. `COLUMN_MASS_THRESHOLDS_(DU)` = (float_list)
   * List of values giving the column mass threshold for species `SO2`
   * Note: A probability forecast map is generated for each value
1. `GROUND_LOAD_THRESHOLDS_(KG/M2)` = (float_list)
   * List of values giving the deposit ground load threshold for species `TEPHRA`
   * Note: A probability forecast map is generated for each value
## Sub-block `IF_POSTPROCESS_PERCENTILES`
Used when `POSTPROCESS_PERCENTILES` = `YES`
1. `PERCENTILE_VALUES_(%)` = (float_list)
   * List of values giving the percentiles in percent
   * Note: A percentile forecast map is generated for each value
