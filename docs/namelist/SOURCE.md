# Block `SOURCE`
This block defines the variables needed by the SetSrc task to generate the source term for the emission phases
1. `SOURCE_TYPE` = (options)
   * Type of source vertical distribution
   * Input options:
      - `POINT`: Point source
      - `SUZUKI`: Suzuki-type source
      - `TOP-HAT`: Top-hat source
      - `PLUME`: Plume source (based on the 1D BPT)
      - `RESUSPENSION`: Resuspension source
1. `SOURCE_START_(HOURS_AFTER_00)` = (float_list)
   * List of source start time for each phase
   * Note: Alternatively, a file can be provided with 3 columns specifying start time/end time/height above vent for each phase. This option is useful in case of many source phases
1. `SOURCE_END_(HOURS_AFTER_00)` = (float_list)
   * List of source end time for each phase
   * Note: If a file is provided in `SOURCE_START_(HOURS_AFTER_00)` this line is ignored
1. `LON_VENT` = (float)
   * Vent longitude
1. `LAT_VENT` = (float)
   * Vent latitude
1. `VENT_HEIGHT_(M)` = (float)
   * Height of the vent in meters a.s.l.
1. `HEIGHT_ABOVE_VENT_(M)` = (float_list)
   * List of source heights in meters above the vent for each eruptive phase
   * Note: The plume heights must be lower than the top of the computational domain. If a file is provided in `SOURCE_START_(HOURS_AFTER_00)` this line is ignored
1. `MASS_FLOW_RATE_(KGS)` = (options)
   * Defines the MFR or how the it should be computed (i.e. derived from column height)
   * Input options:
      - (float_list): Mass flow rate in kg/s for each eruptive phase
      - `ESTIMATE-MASTIN`: MFR is computed using the empirical fit by Mastin et al. (2009)
      - `ESTIMATE-WOODHOUSE`: MFR is computed using the empirical fit by Woodhouse et al. (2013)
      - `ESTIMATE-DEGRUYTER`: MFR is computed using the empirical fit by Degruyter and Bonadonna (2012)
1. `ALFA_PLUME` = (float)
   * Entrainment coefficient
   * Note: Only used if `MASS_FLOW_RATE_(KGS)` is set to `ESTIMATE-WOODHOUSE` or `ESTIMATE-DEGRUYTER`
1. `BETA_PLUME` = (float)
   * Entrainment coefficient
   * Note: Only used if `MASS_FLOW_RATE_(KGS)` is set to `ESTIMATE-WOODHOUSE` or `ESTIMATE-DEGRUYTER`
1. `EXIT_TEMPERATURE_(K)` = (float)
   * Mixture temperature
   * Note: Only used if `SOURCE_TYPE`=`PLUME` or to estimate MFR
1. `EXIT_WATER_FRACTION_(%)` = (float)
   * Total (magmatic) water fraction
   * Note: Only used if `SOURCE_TYPE`=`PLUME` or to estimate MFR
## Sub-block `IF_SUZUKI_SOURCE`
Used when `SOURCE_TYPE`=`IF_SUZUKI_SOURCE`
1. `A` = (float_list)
   * List of values giving the parameter A in the Suzuki distribution (Pfeiffer et al., 2005) for each phase
1. `L` = (float_list)
   * List of values giving the parameter lambda in the Suzuki distribution (Pfeiffer et al., 2005) for each phase
## Sub-block `IF_TOP-HAT_SOURCE`
Used when `SOURCE_TYPE`=`IF_TOP-HAT_SOURCE`
1. `THICKNESS_(M)` = (float_list)
   * List of thickness of the emission slab in meters for each phase
## Sub-block `IF_PLUME_SOURCE`
Used when `SOURCE_TYPE`=`PLUME`
1. `SOLVE_PLUME_FOR` = (options)
   * Configure the FPLUME model embedded in FALL3D
   * Input options:
      - `MFR`: SetSrc solves for mass flow rate given the column height (inverse problem)
      - `HEIGHT`: SetSrc solves for column height given the mass flow rate
1. `MFR_SEARCH_RANGE` = (float_list)
   * Two values n and m such that 10^n and 10^m specify the range of MFR values admitted in the iterative solving procedure
   * Note: Only used if `SOLVE_PLUME_FOR`=`MFR`
1. `EXIT_VELOCITY_(MS)` = (float_list)
   * List of values of the magma exit velocity in m/s at the vent for each eruptive phase
1. `EXIT_GAS_WATER_TEMPERATURE_(K)` = (float_list)
   * List of values of the exit gas water temperature in K for each eruptive phase
1. `EXIT_LIQUID_WATER_TEMPERATURE_(K)` = (float_list)
   * List of values of the exit liquid water temperature in K for each eruptive phase
1. `EXIT_SOLID_WATER_TEMPERATURE_(K)` = (float_list)
   * List of values of the exit solid water temperature in K for each eruptive phase
1. `EXIT_GAS_WATER_FRACTION_(%)` = (float_list)
   * List of values of the exit gas water fraction in percent for each eruptive phase
1. `EXIT_LIQUID_WATER_FRACTION_(%)` = (float_list)
   * List of values of the exit liquid water fraction in percent for each eruptive phase
1. `EXIT_SOLID_WATER_FRACTION_(%)` = (float_list)
   * List of values of the exit solid water fraction in percent for each eruptive phase
1. `WIND_COUPLING` = (options)
   * If wind coupling is considered
   * Input options:
      - `YES`: Enabled
      - `NO`: Vertical wind velocity profile is assumed zero
1. `AIR_MOISTURE` = (options)
   * Air moisture
   * Input options:
      - `YES`: Enabled
      - `NO`: Dry entrained air only
1. `LATENT_HEAT` = (options)
   * Latent heat
   * Input options:
      - `YES`: Enabled
      - `NO`: Latent heat contribution is neglected
1. `REENTRAINMENT` = (options)
   * Reentrainment
   * Input options:
      - `YES`: Enabled
      - `NO`: Particle reentrainment is neglected
1. `BURSIK_FACTOR` = (float)
   * Bursik factor xi
   * Note: If not given, assumed equal to 0.1
1. `Z_MIN_WIND` = (float)
   * Ignore wind entrainment below this zvalue (low jet region)
   * Note: If not given, assumed equal to 100
1. `C_UMBRELLA` = (float)
   * Thickness of umbrella region relative to Hb (>1)
   * Note: If not given, assumed equal to 1.32
1. `A_S` = (options)
   * Plume entrainment coefficient
   * Input options:
      - `CONSTANT` (float_list): A list of two values for: (value jet, value plume)
      - `KAMINSKI-R`: TODO
      - `KAMINSKI-C`: TODO
      - `OLD`: TODO
1. `A_V` = (options)
   * Plume entrainment coefficient
   * Input options:
      - `CONSTANT` (float): A constant value is assumed
      - `TATE`: TODO
