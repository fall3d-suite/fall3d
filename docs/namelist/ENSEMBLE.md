# Block `ENSEMBLE`
This block is used by task SetEns to generate the ensemble members
1. `RANDOM_NUMBERS_FROM_FILE` = (options)
   * Indicate whether the ensemble should be reconstructed
   * Input options:
      - `YES`: Read ensemble perturbations from `*.ens` files
      - `NO`: Reconstruct the ensemble and generate new `*.ens` files
   * Note: If `*.ens` are not found, option `RANDOM_NUMBERS_FROM_FILE` = `NO` will be used
1. `PERTURBATE_COLUMN_HEIGHT` = (options)
   * Perturbate eruption column height
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
1. `PERTURBATE_MASS_FLOW_RATE` = (options)
   * Perturbate mass eruption rate
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: This option is deactivated for `SOURCE_TYPE=PLUME` or `MASS_FLOW_RATE_(KGS)=ESTIMATE-*` options
1. `PERTURBATE_SOURCE_START` = (options)
   * Perturbate the starting time of each eruptive phase
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
1. `PERTURBATE_SOURCE_DURATION` = (options)
   * Perturbate the duration time of each eruptive phase
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
1. `PERTURBATE_TOP-HAT_THICKNESS` = (options)
   * Perturbate the top-hat thickness
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `SOURCE_TYPE = TOP-HAT`
1. `PERTURBATE_SUZUKI_A` = (options)
   * Perturbate the Suzuki coefficient `A`
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `SOURCE_TYPE = SUZUKI`
1. `PERTURBATE_SUZUKI_L` = (options)
   * Perturbate the Suzuki coefficient `L`
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `SOURCE_TYPE = SUZUKI`
1. `PERTURBATE_WIND` = (options)
   * Perturbate the horizontal wind components
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Zonal and meridional wind components are independently perturbed (two random numbers are generated)
1. `PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` = (options)
   * Perturbate the cloud insertion height
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `INITIAL_CONDITION = INSERTION`
1. `PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` = (options)
   * Perturbate the cloud insertion thickness
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `INITIAL_CONDITION = INSERTION`
1. `PERTURBATE_FI_MEAN` = (options)
   * Perturbate the mean of the TGSD
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: For bimodal distributions `BIGAUSSIAN` and `ESTIMATE`, only the fine subpopulation is perturbed
1. `PERTURBATE_DIAMETER_AGGREGATES_(MIC)` = (options)
   * Perturbate the diameter of the particle aggregates
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `AGGREGATION_MODEL` != `NONE`
1. `PERTURBATE_DENSITY_AGGREGATES` = (options)
   * Perturbate the density of the particle aggregates
   * Input options:
      - `NO`: Disable perturbation
      - `RELATIVE`: Perturbation range given as a fraction of the central value
      - `ABSOLUTE`: Perturbation range given as an absolute value
   * Note: Only used if `AGGREGATION_MODEL` != `NONE`
## Sub-block `IF_PERTURBATE_COLUMN_HEIGHT`
Used when `PERTURBATE_COLUMN_HEIGHT` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in meters (`PERTURBATE_COLUMN_HEIGHT` = `ABSOLUTE`) or percent (`PERTURBATE_COLUMN_HEIGHT` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_MASS_FLOW_RATE`
Used when `PERTURBATE_MASS_FLOW_RATE` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in kg/s (`PERTURBATE_MASS_FLOW_RATE` = `ABSOLUTE`) or percent (`PERTURBATE_MASS_FLOW_RATE` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_SOURCE_START`
Used when `PERTURBATE_SOURCE_START` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in s (`PERTURBATE_SOURCE_START` = `ABSOLUTE`) or percent (`PERTURBATE_SOURCE_START` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_SOURCE_DURATION`
Used when `PERTURBATE_SOURCE_DURATION` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in s (`PERTURBATE_SOURCE_DURATION` = `ABSOLUTE`) or percent (`PERTURBATE_SOURCE_DURATION` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_TOP-HAT_THICKNESS`
Used when `PERTURBATE_TOP-HAT_THICKNESS` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in meters (`PERTURBATE_TOP-HAT_THICKNESS` = `ABSOLUTE`) or percent (`PERTURBATE_TOP-HAT_THICKNESS` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_SUZUKI_A`
Used when `PERTURBATE_SUZUKI_A` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range of the dimensionless parameter `A` in absolute units (`IF_PERTURBATE_SUZUKI_A` = `ABSOLUTE`) or percent (`IF_PERTURBATE_SUZUKI_A` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_SUZUKI_L`
Used when `PERTURBATE_SUZUKI_L` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range of the dimensionless parameter `L` in absolute units (`IF_PERTURBATE_SUZUKI_L` = `ABSOLUTE`) or percent (`IF_PERTURBATE_SUZUKI_L` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_WIND`
Used when `PERTURBATE_WIND` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in m/s (`PERTURBATE_WIND` = `ABSOLUTE`) or percent (`PERTURBATE_WIND` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT`
Used when `PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in meters (`IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` = `ABSOLUTE`) or percent (`IF_PERTURBATE_DATA_INSERTION_CLOUD_HEIGHT` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS`
Used when `PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in meters (`PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` = `ABSOLUTE`) or percent (`PERTURBATE_DATA_INSERTION_CLOUD_THICKNESS` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_FI_MEAN`
Used when `PERTURBATE_FI_MEAN` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in phi units (`PERTURBATE_FI_MEAN` = `ABSOLUTE`) or percent (`PERTURBATE_FI_MEAN` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_DIAMETER_AGGREGATES_(MIC)`
Used when `PERTURBATE_DIAMETER_AGGREGATES_(MIC)` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in microns (`PERTURBATE_DIAMETER_AGGREGATES_(MIC)` = `ABSOLUTE`) or percent (`PERTURBATE_DIAMETER_AGGREGATES_(MIC)` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
## Sub-block `IF_PERTURBATE_DENSITY_AGGREGATES`
Used when `PERTURBATE_DENSITY_AGGREGATES` != `NO`
1. `PERTURBATION_RANGE` = (float)
   * Define the perturbation range in kg/m3 (`PERTURBATE_DENSITY_AGGREGATES` = `ABSOLUTE`) or percent (`PERTURBATE_DENSITY_AGGREGATES` = `RELATIVE`)
1. `PDF` = (options)
   * Define the sampling Probability Density Function (PDF)
   * Input options:
      - `UNIFORM`: Uniform (constant) PDF
      - `GAUSSIAN`: Gaussian PDF centred at the reference value
