# Block `SPECIES_TGSD`
These blocks define the TGSD for each species and are used by the SetTgsd task to generate some basic distributions
1. `NUMBER_OF_BINS` = (integer)
   * Number of bins in the TGSD
   * Note: This value can be different from the number of classes defined in the granulometry file `*.grn` created by the SetSrc task if aggregate bins are included later on or if a cut-off diameter is imposed
1. `FI_RANGE` = (float_list)
   * A list of two values defining the minimum and maximum diameters in phi units
1. `DENSITY_RANGE` = (float_list)
   * A list of two values defining the minimum and maximum densities
   * Note: Linear interpolation (in phi) is assumed between the extremes
1. `SPHERICITY_RANGE` = (float_list)
   * A list of two values defining the minimum and maximum values for sphericity
   * Note: Linear interpolation (in phi) is assumed between the extremes
1. `DISTRIBUTION` = (options)
   * Type of TGSD distribution
   * Input options:
      - `GAUSSIAN`: Gaussian distribution
      - `BIGAUSSIAN`: bi-Gaussian distribution
      - `WEIBULL`: Weibull distribution
      - `BIWEIBULL`: bi-Weibull distribution
      - `CUSTOM`: Custom distribution from an external user file
      - `ESTIMATE`: TGSD is estimated from column height and magma viscosity (for tephra species only)
## Sub-block `IF_GAUSSIAN`
Used when `DISTRIBUTION`=`GAUSSIAN`
1. `FI_MEAN` = (float)
   * Mean of the gaussian distribution in phi units
1. `FI_DISP` = (float)
   * Standard deviation of the gaussian distribution in phi units
## Sub-block `IF_BIGAUSSIAN`
Used when `DISTRIBUTION`=`BIGAUSSIAN`
1. `FI_MEAN` = (float_list)
   * A list of two values in phi units defining the means of two gaussian distributions
1. `FI_DISP` = (float_list)
   * A list of two values in phi units defining the standard deviations of two gaussian distributions
1. `MIXING_FACTOR` = (float)
   * The mixing factor between both gaussian distributions
## Sub-block `IF_WEIBULL`
Used when `DISTRIBUTION`=`WEIBULL`
1. `FI_SCALE` = (float)
   * Parameter of the Weibull distribution
1. `W_SHAPE` = (float)
   * Parameter of the Weibull distribution
## Sub-block `IF_BIWEIBULL`
Used when `DISTRIBUTION`=`BIWEIBULL`
1. `FI_SCALE` = (float_list)
   * List of parameters for the bi-Weibull distribution
1. `W_SHAPE` = (float_list)
   * List of parameters for the bi-Weibull distribution
1. `MIXING_FACTOR` = (float)
   * Parameter of the bi-Weibull distribution
## Sub-block `IF_CUSTOM`
Used when `DISTRIBUTION`=`CUSTOM`
1. `FILE` = (string)
   * Path to the file with the custom TGSD
## Sub-block `IF_ESTIMATE`
Used when `DISTRIBUTION`=`ESTIMATE`
1. `VISCOSITY_(PAS)` = (float)
   * Magma viscosity in Pa.s
1. `HEIGHT_ABOVE_VENT_(M)` = (float)
   * Eruption column height above vent in meters
