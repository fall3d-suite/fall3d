# Block `PARTICLE_AGGREGATION`
This block is used by task SetSrc and controls particle aggregation and cut-off (for categories particles and radionuclides only)
1. `PARTICLE_CUT_OFF` = (options)
   * Specifies a cut off diamater
   * Input options:
      - `NONE`: No cut-off used
      - `FI_LOWER_THAN` (float): Define an upper cut-off limit in phi units
      - `FI_LARGER_THAN` (float): Define a lower cut-off limit in phi units
      - `D_(MIC)_LARGER_THAN` (float): Define an upper cut-off limit for diameter in microns
      - `D_(MIC)_LOWER_THAN` (float): Define a lower cut-off limit for diameter in microns
1. `AGGREGATION_MODEL` = (options)
   * Define a particle aggregation parametrization
   * Input options:
      - `NONE`: No aggregation
      - `CORNELL`: Cornell model (Cornell et al., 1983)
      - `COSTA`: Costa model (Costa et al., 2010). Valid only for tephra particles and `PLUME` source type
      - `PERCENTAGE`: Percentage model (Sulpizio et al., 2012)
1. `NUMBER_OF_AGGREGATE_BINS` = (integer)
   * Number of aggregate bins (default value is 1)
   * Note: Only used if `AGGREGATION_MODEL`!=`NONE`
1. `DIAMETER_AGGREGATES_(MIC)` = (float_list)
   * Diameter of aggregate bins in microns
   * Note: Only used if `AGGREGATION_MODEL`!=`NONE`
1. `DENSITY_AGGREGATES_(KGM3)` = (float_list)
   * Density of aggregate bins in kg m-3
   * Note: Only used if `AGGREGATION_MODEL`!=`NONE`
1. `PERCENTAGE_(%)` = (float_list)
   * Percentage of aggregate bins
   * Note: Only used if `AGGREGATION_MODEL`=`PERCENTAGE`
1. `VSET_FACTOR` = (float)
   * Multiplicative correction factor for settling velocity of aggregates
   * Note: Only used if `AGGREGATION_MODEL`=`PERCENTAGE`
1. `FRACTAL_EXPONENT` = (float)
   * Fractal exponent (see Costa et al., 2010 for details)
   * Note: Only used if `AGGREGATION_MODEL`=`COSTA`
