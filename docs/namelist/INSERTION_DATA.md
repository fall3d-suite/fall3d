# Block `INSERTION_DATA`
This block is read by the FALL3D task only if `INITIAL_CONDITION` = `INSERTION`
1. `INSERTION_FILE` = (string)
   * Path to the initial condition file in netCDF format (e.g. a satellite retrieval)
1. `INSERTION_DICTIONARY_FILE` = (string)
   * Path to the insertion dictionary file defining the variable names. An example can be found in `Other/Sat/Sat.tbl`
   * Optional
   * Note: If not given, a default dictionary will be used
1. `INSERTION_TIME_SLAB` = (integer)
   * Time slab in the insertion file to be used as the initial conditions
1. `DIAMETER_CUT_OFF_(MIC)` = (float)
   * Cut-off diameter in microns. Maximum particle diameter used to define the initial condition
   * Optional
