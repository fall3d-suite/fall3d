# Example test case

In order to run the Example test case, create a run folder 
`$WORKPATH` and copy the template in the FALL3D distribution 
path (`$FALL3DPATH`):
```console
> mkdir $WORKPATH
> cd $WORKPATH
> cp $FALL3DPATH/Templates/template.inp Example.inp
```

Two additional input files are required to run this example. 
The meteorological database can be downloaded from the 
[testsuite repository](https://gitlab.geo3bcn.csic.es/fall3d/test-suite/example)
using the `wget` command:
```console
> wget https://gitlab.geo3bcn.csic.es/fall3d/test-suite/example/-/raw/d1a704bb0551ff50033f0df3300d70166a7eccf5/Example.wrf.nc
```

In order to properly read this meteorological file, FALL3D needs a dictionary to
decode the variable names in the input file and translate them into the
internal variable representation. Samples of different dictionaries are found
in the folder `Other/Meteo/Tables` of the distribution. For this specific 
example case, you will need the `WRF.tbl` dictionary:

```console
> cp $FALL3DPATH/Other/Meteo/Tables/WRF.tbl .
```

The program is now ready to be executed.
You can run FALL3D in serial mode using the model configuration defined in 
the input file `Example.inp` with the following command:
```console
> $BINPATH/Fall3d.x Example.inp All
```

or in parallel using:
```console
> mpirun -n 4 $BINPATH/Fall3d.x Example.inp All 2 2 1
```

where `$BINPATH` is the folder where FALL3D was built. 
See the [User Guide](https://fall3d-suite.gitlab.io/fall3d/)
for further information.

# Template for the tracking points file

The tracking points file is an optional input file specifying 
a list of coordinates where model outputs will be interpolated
to produce a set of time series of relevant variables 
(e.g., surface PM10 concentration or deposit mass loading). 
You can enable this functionality in the `.inp` configuration 
file by setting:

```
TRACK_POINTS = YES
TRACK_POINTS_FILE = Example.pts
```

The format of this file is (see `template.pts`):

| ID for location | Longitudes | Latitudes |
|:----------:     |:---------: |:--------: |
| site1           | lon1       | lat1      |
| site2           | lon2       | lat2      |
| site3           | lon3       | lat3      |
| site4           | lon4       | lat4      |

For example, a tracking points file with 3 locations looks like:

```
etna    14.9934 37.7510
palermo 13.3615 38.1157 
airport 15.0651 37.4663
```

# Template for the emission phases file

In order to define multiple eruptive phases, 
you can provide a list of values for the following 
options in the `.inp` configuration file:

```
SOURCE_START_(HOURS_AFTER_00) = 0  12
SOURCE_END_(HOURS_AFTER_00)   = 10 24
HEIGHT_ABOVE_VENT_(M)         = 6000. 2000.
MASS_FLOW_RATE_(KGS)          = 1E7 2E6
```

In this case, we are defining two eruptive phases. 
However, in some cases you have several phases or
continuous emissions. In such a situation it is 
impractical to define a very long list of values.
You can can provide an additional file with 4 
columns to set the source term parameters by setting
the `.inp` configuration file in this way:

```
SOURCE_START_(HOURS_AFTER_00) = template.phases
SOURCE_END_(HOURS_AFTER_00)   = 
HEIGHT_ABOVE_VENT_(M)         = 
MASS_FLOW_RATE_(KGS)          = 
```

The format of the emission phases file is (see `template.phases`):

| Source start | Source end (optional)  | Height above vent (optional) | Mass flow rate (optional) |
|:----------:  |:---------:  |:--------:         | :------------:  |
| START_TIME1  | END_TIME1   | COLUMN_HEIGHT1    | MASS_FLOW_RATE1 | 
| START_TIME2  | END_TIME2   | COLUMN_HEIGHT2    | MASS_FLOW_RATE2 |
| START_TIME3  | END_TIME3   | COLUMN_HEIGHT3    | MASS_FLOW_RATE3 |
| START_TIME4  | END_TIME4   | COLUMN_HEIGHT4    | MASS_FLOW_RATE4 |
| START_TIME5  | END_TIME5   | COLUMN_HEIGHT5    | MASS_FLOW_RATE5 |

The `template.phases` file must have at least one column and at 
most 4 columns. If, for example, the `template.phases` has just two
columns, then both options `SOURCE_START_(HOURS_AFTER_00)` and
`SOURCE_END_(HOURS_AFTER_00)` will be read from this file and the 
remaining options `HEIGHT_ABOVE_VENT_(M)` and `MASS_FLOW_RATE_(KGS)`
will be read from the `.inp` configuration file.
