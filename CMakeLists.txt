cmake_minimum_required(VERSION 3.10)
project(fall3d Fortran)

# Define default values for the options
option(WITH-R4 "Compile with single precision (R4)" OFF)
option(WITH-MPI "Turn on parallel execution (with MPI)" OFF)
option(WITH-ACC "Use OpenACC GPU accelerated version" OFF)
option(DETAIL_BIN "Generates a binary with all options in its name" OFF)

# Set the user CUSTOM_COMPILER flags
set(CUSTOM_COMPILER_FLAGS "" CACHE STRING "Custom Fortran compilation flags")
separate_arguments(CUSTOM_COMPILER_FLAGS)
# Set the user CUSTOM_LINKER flags
set(CUSTOM_LINKER_FLAGS "" CACHE STRING "Custom Fortran linker flags")
separate_arguments(CUSTOM_LINKER_FLAGS)
# Print nectdf section title
message("")
message(STATUS "NetCDFF Configuration")

if (DEFINED ENV{FALL3D_NETCDF_DIR} AND EXISTS "$ENV{FALL3D_NETCDF_DIR}")

    # check if the environment variable has a path
    set(NetCDF_ROOT $ENV{FALL3D_NETCDF_DIR})
    message(STATUS "    Using NetCDFF path from environment variable: FALL3D_NETCDF_DIR")
    set(NetCDF_INCLUDE_DIR "${NetCDF_ROOT}include/")
    set(NetCDF_LIBRARY "${NetCDF_ROOT}/lib")
    find_library(NETCDFF_LIBRARY NAMES netcdff PATHS ${NetCDF_LIBRARY})

else()
    # Find the NetCDF C and Fortran libraries
    find_library(NETCDFF_LIBRARY NAMES netcdff)
    find_library(NETCDF_C_LIBRARY NAMES netcdf)
    find_path(NetCDF_INCLUDE_DIR NAMES netcdf.mod PATH_SUFFIXES include HINTS ${NetCDF_LIBRARY})
    message(STATUS "    NetCDFF auto-detection")

    # Check if netcdff library was found
    if(NOT NETCDFF_LIBRARY OR "${NetCDF_INCLUDE_DIR}" MATCHES "NOTFOUND")
        message(STATUS "    NetCDFF auto-detection failed")
        message(STATUS "    Searching for NetCDFF using environment variables")
        # Define a list with potential environment variables that can contain the NetCDF path
        set(NETCDF_ENV_VARS "NETCDF_DIR" "CRAY_NETCDF_DIR" "NETCDF_FORTRAN_HOME" "NETCDF_HOME" "NETCDF_ROOT" "NETCDF_PATH")

        # Check each environment variable and set NetCDF_ROOT if it exists
        foreach(ENV_VAR ${NETCDF_ENV_VARS})
            if (DEFINED ENV{${ENV_VAR}})
                set(NetCDF_ROOT $ENV{${ENV_VAR}})
                message(STATUS "    Using NetCDF path from environment variable: ${ENV_VAR}")
                break()  # Exit the loop after finding a valid environment variable
            endif()
        endforeach()

        # If NetCDF_ROOT is not set by environment variables, check if it's set manually
        if (NOT NetCDF_ROOT)
            message(FATAL_ERROR "NetCDFF not found.\nSet the path in the FALL3D_NETCDF_DIR environment variable.")
        endif()

        # Define paths based on NetCDF_ROOT
        set(NetCDF_INCLUDE_DIR "${NetCDF_ROOT}/include")
        set(NetCDF_LIBRARY "${NetCDF_ROOT}/lib")
        find_library(NETCDF_LIBRARY NAMES netcdf PATHS ${NetCDF_LIBRARY})
        find_library(NETCDFF_LIBRARY NAMES netcdff PATHS ${NetCDF_LIBRARY})

        # Verify NetCDF directories
        if(NOT EXISTS ${NetCDF_INCLUDE_DIR})
            message(FATAL_ERROR "NetCDF include directory does not exist: ${NetCDF_INCLUDE_DIR}")
        endif()

        if(NOT EXISTS ${NetCDF_LIBRARY})
            message(FATAL_ERROR "NetCDF libraries directory does not exist: ${NetCDF_LIBRARY}")
        endif()

        # Find NetCDF components
        find_path(NETCDF_INCLUDE_DIR NAMES netcdf.inc PATHS ${NetCDF_INCLUDE_DIR})
        find_library(NETCDF_LIBRARY NAMES netcdf PATHS ${NetCDF_LIBRARY})

        if (NOT NETCDF_INCLUDE_DIR)
            message(FATAL_ERROR "Could not find NetCDF include directory.")
        endif()

        if (NOT NETCDF_LIBRARY)
            message(FATAL_ERROR "Could not find NetCDF library.")
        endif()
    endif()
endif()
# Print NetCDF configuration
message(STATUS "    NetCDFF include directory: ${NetCDF_INCLUDE_DIR}")
message(STATUS "    NetCDFF Fortran library: ${NETCDFF_LIBRARY}")


# Print compiler info title
message("")
message(STATUS "Compiler Info")
# Print compiler info
message(STATUS "    Default Fortran compiler: ${CMAKE_Fortran_COMPILER}")
message(STATUS "    Compiler ID: ${CMAKE_Fortran_COMPILER_ID}")

# Obtain Git information
execute_process(
    COMMAND git describe --always --dirty
    RESULT_VARIABLE GIT_DESCRIBE_RESULT
    OUTPUT_VARIABLE GIT_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_DESCRIBE_RESULT)
    message(WARNING "Failed to get Git describe information.")
    set(GIT_VERSION "vx")
endif()

execute_process(
    COMMAND git log -1 --format=%cd
    RESULT_VARIABLE GIT_LOG_DATE_RESULT
    OUTPUT_VARIABLE GIT_DATE
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_LOG_DATE_RESULT)
    message(WARNING "Failed to get Git date information.")
    set(GIT_DATE "noDate")
endif()

execute_process(
    COMMAND git log -1 --format=%H
    RESULT_VARIABLE GIT_LOG_COMMIT_RESULT
    OUTPUT_VARIABLE GIT_COMMIT
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_LOG_COMMIT_RESULT)
    message(WARNING "Failed to get Git commit hash.")
    set(GIT_COMMIT "000000")
endif()

execute_process(
    COMMAND git config --get remote.origin.url
    RESULT_VARIABLE GIT_URL_RESULT
    OUTPUT_VARIABLE GIT_URL
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_URL_RESULT)
    message(WARNING "Failed to get Git repository URL.")
    set(GIT_URL "noURL")
endif()

execute_process(
    COMMAND git rev-parse --abbrev-ref HEAD
    RESULT_VARIABLE GIT_BRANCH_RESULT
    OUTPUT_VARIABLE GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_BRANCH_RESULT)
    message(WARNING "Failed to get Git branch name.")
    set(GIT_BRANCH "unknown")
endif()

# Create a version string with branch
set(PACKAGE_VERSION_WITH_BRANCH "${GIT_VERSION}/${GIT_BRANCH}")

# Print version info title
message("")
message(STATUS "Version info")
# Print Git information
message(STATUS "    Git Version: ${GIT_VERSION}")
message(STATUS "    Git Date: ${GIT_DATE}")
message(STATUS "    Git Commit: ${GIT_COMMIT}")
message(STATUS "    Git URL: ${GIT_URL}")
message(STATUS "    Git Branch: ${GIT_BRANCH}")
message(STATUS "    Package Version with Branch: ${PACKAGE_VERSION_WITH_BRANCH}")

# Verify template file exists
if(NOT EXISTS "${CMAKE_SOURCE_DIR}/Sources/mod_Config.f90.in")
    message(FATAL_ERROR "Template file not found: ${CMAKE_SOURCE_DIR}/Sources/mod_Config.f90.in")
endif()

# Set configuration data
set(CONFIG_FILE_OUTPUT "${CMAKE_BINARY_DIR}/mod_Config.f90")
set(PACKAGE_VERSION "${PACKAGE_VERSION_WITH_BRANCH}")
set(VERSION_URL "${GIT_URL}")
set(VERSION_DATE "${GIT_DATE}")
set(VERSION_LICENSE "GNU v3 see LICENSE file")
set(DO_NOT_MODIFY_MESSAGE "DO NOT MODIFY (file generated by build system)")

# Configure the mod_Config.f90 file
configure_file(
  "${CMAKE_SOURCE_DIR}/Sources/mod_Config.f90.in"
  "${CONFIG_FILE_OUTPUT}"
  @ONLY
)

# Verify the output file
if(NOT EXISTS "${CONFIG_FILE_OUTPUT}")
    message(FATAL_ERROR "Generated file not found: ${CONFIG_FILE_OUTPUT}")
endif()

# Initialize compiler and linker flags
set(COMPILER_FLAGS "")
set(LINKER_FLAGS "")

# Set the bin path where the binary will be stored
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

# Print precision information title
message("")
message(STATUS "Precision info")
# Set compiler flags based on options and compiler ID
if(WITH-R4)
    list(APPEND COMPILER_FLAGS "-DWITH_R4")
    message(STATUS "    Single precision (R4) enabled")
else()
    message(STATUS "    Double precision (R8) enabled")
endif()

# Print MPI information title
message("")
message(STATUS "MPI info")
if(WITH-MPI)
    find_package(MPI REQUIRED)
    list(APPEND COMPILER_FLAGS "${MPI_Fortran_COMPILE_FLAGS}" "-DWITH_MPI")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MPI_Fortran_LINK_FLAGS}")

    # Print MPI enabled message
    message(STATUS "    MPI Enabled")

    # If MPI_Fortran_COMPILER is not set, print a warning
    if(NOT MPI_Fortran_COMPILER)
        message(WARNING "MPI Fortran compiler not set.")
    else()
        set(CMAKE_Fortran_COMPILER "${MPI_Fortran_COMPILER}")
        message(STATUS "    MPI Fortran compiler: ${MPI_Fortran_COMPILER}")
        message(STATUS "    MPI compiler ID: ${CMAKE_Fortran_COMPILER_ID}")
    endif()
else()
    message(STATUS "    MPI Disabled")
endif()
# Prepare the compiler flags based on the compiler ID and user's CUSTOM flags
# CASE GNU compiler
if("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "GNU")
    message(STATUS "    Detected GNU Fortran compiler")
    # If the CUSTOM flags are set, append them to the list
    if(CUSTOM_COMPILER_FLAGS)
        list(APPEND COMPILER_FLAGS ${CUSTOM_COMPILER_FLAGS})
    endif()
    if(CUSTOM_LINKER_FLAGS)
        list(APPEND LINKER_FLAGS ${CUSTOM_LINKER_FLAGS})
    endif()
    if(WITH-ACC)
        message(FATAL_ERROR "OpenACC is not supported with GNU Fortran!")
    endif()
    # CASE NVIDIA HPC compiler
elseif("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "NVHPC")
    message(STATUS "    Detected NVIDIA HPC Fortran compiler")
    # If the CUSTOM flags are set, append them to the list
    if(CUSTOM_COMPILER_FLAGS)
        list(APPEND COMPILER_FLAGS ${CUSTOM_COMPILER_FLAGS})
    endif()
    if(CUSTOM_LINKER_FLAGS)
        list(APPEND LINKER_FLAGS ${CUSTOM_LINKER_FLAGS})
    endif()
    if(WITH-ACC)
        list(APPEND COMPILER_FLAGS "-acc")
        list(APPEND COMPILER_FLAGS "-DWITH_ACC")
        list(APPEND LINKER_FLAGS "-acc")
    endif()
    # CASE Cray compiler
elseif("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "Cray")
    message(STATUS "    Detected Cray Fortran compiler")
    # If the CUSTOM flags are set, append them to the list
    if(CUSTOM_COMPILER_FLAGS)
        list(APPEND COMPILER_FLAGS ${CUSTOM_COMPILER_FLAGS})
    endif()
    if(CUSTOM_LINKER_FLAGS)
        list(APPEND LINKER_FLAGS ${CUSTOM_LINKER_FLAGS})
    endif()
    if(WITH-ACC)
        list(APPEND COMPILER_FLAGS "-hacc")
        list(APPEND COMPILER_FLAGS "-DWITH_ACC")
    endif()

else()
    message(WARNING "Unsupported Fortran compiler: ${CMAKE_Fortran_COMPILER_ID}")
endif()
# add the linker_flags to CMAKE_EXE_LINKER_FLAGS
if(LINKER_FLAGS)
    list(APPEND CMAKE_EXE_LINKER_FLAGS ${LINKER_FLAGS})
endif()
# remove the ; from the list and leave as a string
string(REPLACE ";" " " CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}")

# Print OpenACC information title
message("")
message(STATUS "OpenACC info")
if(WITH-ACC)
    message(STATUS "    OpenACC Enabled")
else()
    message(STATUS "    OpenACC Disabled")
endif()

# Construct the executable name based on options
if(DETAIL_BIN)
    set(EXECUTABLE_NAME "Fall3d")
    set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.${CMAKE_Fortran_COMPILER_ID}")
    if(WITH-R4)
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.r4")
    else()
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.r8")
    endif()
    if(WITH-MPI)
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.mpi")
    else()
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.serial")
    endif()
    if(WITH-ACC)
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.acc")
    else()
        set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.cpu")
    endif()
    set(EXECUTABLE_NAME "${EXECUTABLE_NAME}.x")
else()
    set(EXECUTABLE_NAME "Fall3d.x")
endif()

# Collect all .F90 and .f90 files from the Sources directory
file(GLOB SOURCES "Sources/*.F90")

# Apply flags and add the executable
add_executable(${EXECUTABLE_NAME} ${SOURCES} ${CONFIG_FILE_OUTPUT})
target_compile_options(${EXECUTABLE_NAME} PRIVATE ${COMPILER_FLAGS})
target_include_directories(${EXECUTABLE_NAME} PRIVATE ${NetCDF_INCLUDE_DIR})
target_link_libraries(${EXECUTABLE_NAME} PRIVATE ${NETCDFF_LIBRARY})

# Print the summary message
message("")
message(STATUS "Summary")
message(STATUS "    Binary name:\t${EXECUTABLE_NAME}")
message(STATUS "    Build path:\t${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
# Print the options selected
if(DETAIL_BIN)
    message(STATUS "    Binary name:\tDetailed")
else()
    message(STATUS "    Binary name:\tSimple")
endif()
if(WITH-R4)
    message(STATUS "    Precision:\tSingle (R4)")
else()
    message(STATUS "    Precision:\tDouble (R8)")
endif()
if(WITH-MPI)
    message(STATUS "    MPI:\t\tEnabled")
else()
    message(STATUS "    MPI:\t\tDisabled")
endif()
if(WITH-ACC)
    message(STATUS "    OpenACC:\t\tEnabled")
else()
    message(STATUS "    OpenACC:\t\tDisabled")
endif()
# Print all compiler flags
message(STATUS "    Compiler flags:\t${COMPILER_FLAGS}")
message(STATUS "    Linker flags:\t${LINKER_FLAGS}")
message("")
message(STATUS "To compile the binary, use the following command:")
message(STATUS "    make -jN")
message(STATUS "where N is the number of parallel jobs (processes) you want to use for compilation.")
message(STATUS "For example, to utilize 4 parallel jobs, run:")
message(STATUS "    make -j4")
message(STATUS "This can significantly speed up the build process by distributing the workload across multiple CPU cores.")
message("")
message(STATUS "To build the project, run the following commands:")
message(STATUS "    cd ${CMAKE_BINARY_DIR}")
message(STATUS "    make -j20")
message("")
message(STATUS "Configuration done.")
