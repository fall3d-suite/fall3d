subroutine task_Fall3d(MY_FILES, MY_ERR)
   !
   !------------------------------------
   !    subroutine task_Fall3d
   !------------------------------------
   !
   !>   @brief
   !>   Task 3-D Advection-Diffusion-Sedimentation model
   !>   @author
   !>   Arnau Folch
   !
   use KindType, only: ip, rp, &
      LOG_LEVEL_NORMAL, &
      FILE_LIST, &
      ERROR_STATUS, &
      TASK_RUN_FALL3D
   use Shared, only: MY_TRA, MY_TIME, MY_GRID, &
      MY_ENS, MY_MOD, MY_OUT, &
      MY_AGR, MY_SPE, MY_MET
   use Domain, only: my_ips, my_ipe, &
      my_jps, my_jpe, &
      my_kps, my_kpe, &
      my_ibs_1h, my_ibe_1h, &
      my_jbs_1h, my_jbe_1h, &
      my_kbs_1h, my_kbe_1h, &
      my_ips_2h, my_ipe_2h, &
      my_jps_2h, my_jpe_2h, &
      my_kps_2h, my_kpe_2h
   use Parallel, only: master_model, parallel_bcast
   use nc_IO, only: nc_IO_read_rst, &
      nc_IO_out_rst, &
      nc_IO_out_ftp, &
      nc_IO_out_check_fl_warns
   use Sat, only: sat_set_initial_condition
   use Dbs, only: dbs_close_files
   use json_IO, only: json_IO_write_f3d
   use F3D, only: F3D_set_pts, &
      F3D_write_data, &
      F3D_initialize, &
      F3D_init_meteo, &
      F3D_set_source_term, &
      F3D_update_meteo, &
      F3D_print_meteo_info, &
      F3D_time_step, &
      F3D_end_time_step, &
      F3D_release, &
      F3D_out_pts_grn
   implicit none
   !
   type(FILE_LIST), intent(INOUT) :: MY_FILES
   type(ERROR_STATUS), intent(INOUT) :: MY_ERR
   !
   logical  :: sourcetime, meteotime
   !
   !*** Initializations
   !
   call CPU_TIME(MY_ERR % cpu_start_time)
   !
   !*** Compute interpolation factors for tracking points
   !
   if (MY_OUT % track_points) then
      call F3D_set_pts(MY_OUT, MY_GRID, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
   end if
   !
   !*** Allocates tracer memory in MY_TRA
   !
   allocate (MY_TRA % my_c(my_ips_2h:my_ipe_2h, my_jps_2h:my_jpe_2h, my_kps_2h:my_kpe_2h, 1:MY_TRA % nbins))
   allocate (MY_TRA % my_s(my_ips:my_ipe, my_jps:my_jpe, my_kps:my_kpe, 1:MY_TRA % nbins))
   allocate (MY_TRA % my_vs(my_ips:my_ipe, my_jps:my_jpe, my_kbs_1h:my_kbe_1h, 1:MY_TRA % nbins))
   allocate (MY_TRA % my_acum(my_ips_2h:my_ipe_2h, my_jps_2h:my_jpe_2h, 1:MY_TRA % nbins))
   allocate (MY_TRA % my_awet(my_ips_2h:my_ipe_2h, my_jps_2h:my_jpe_2h, 1:MY_TRA % nbins))
   !
   !*** Set the initial condition
   !
   if (MY_TIME % restart) then
      call nc_IO_read_rst(MY_FILES, MY_GRID, MY_TRA, MY_OUT, MY_TIME, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
      !
      MY_TRA % my_awet(:, :, :) = 0.0_rp
      !
   else if (MY_TIME % insertion) then
      call sat_set_initial_condition(MY_FILES, MY_TIME, MY_GRID, MY_TRA, MY_ENS, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
      !
      MY_TRA % my_acum(:, :, :) = 0.0_rp
      MY_TRA % my_awet(:, :, :) = 0.0_rp
   else
      MY_TRA % my_c(:, :, :, :) = 0.0_rp
      MY_TRA % my_acum(:, :, :) = 0.0_rp
      MY_TRA % my_awet(:, :, :) = 0.0_rp
   end if
   !
   !*** Master writes input data to the log file
   !
   if (MY_OUT % log_level .ge. LOG_LEVEL_NORMAL) then
      if (master_model) call F3D_write_data(MY_FILES, MY_TIME, MY_GRID, MY_MOD, MY_TRA, MY_OUT, MY_ERR)
      call parallel_bcast(MY_ERR % flag, 1, 0)
      if (MY_ERR % flag .ne. 0) return
   end if
   !
   !*** Starts time integration
   !
   MY_TIME % go_on = .true.
   MY_TIME % time = MY_TIME % run_start
   MY_TIME % meteo_time = MY_TIME % run_start
   MY_TIME % source_time = MY_TIME % run_start
   MY_TIME % iiter = 0
   !
   sourcetime = .true.
   meteotime = .true.
   !
   !*** Initialize F3D resources
   !
   !call F3D_initialize(MY_MOD%limiter,MY_MOD%time_marching) !acc change
   call F3D_initialize(MY_MOD % limiter, MY_MOD % time_marching, MY_TRA, MY_GRID)
   !
   !*** Initialize meteo
   !
   call F3D_init_meteo(MY_FILES, MY_MOD, MY_TRA, MY_TIME, MY_OUT, MY_MET, MY_ERR)
   if (MY_ERR%flag.ne.0) return
   !
   !*** Time counter for initialisation phase
   !
   call CPU_TIME(MY_ERR%MY_TIMERS%cpu_IO_read)
   !
   do while (MY_TIME % go_on)
      !
      !*** If necessary reads source data
      !
      if (MY_TIME % source_time <= MY_TIME % time) then
         if (sourcetime) then
            call F3D_set_source_term(MY_FILES, MY_TIME, MY_MOD, MY_OUT, MY_GRID, MY_TRA, MY_ERR)
            if (MY_ERR%flag.ne.0) return
         end if
         if (MY_TIME % source_time >= MY_TIME % run_end) sourcetime = .false.  ! switch off source
      end if
      !
      !*** If necessary load meteo data for the current time interval
      !
      if (MY_TIME % meteo_time <= MY_TIME % time) then
         if (meteotime) then
            call F3D_update_meteo(MY_FILES, MY_GRID, MY_AGR, MY_TIME, MY_MOD, MY_MET, MY_TRA, MY_ERR)
            if (MY_ERR%flag.ne.0) return
            if (master_model) call F3D_print_meteo_info(MY_FILES, MY_OUT, MY_TIME, MY_MET, MY_TRA, MY_ERR)
         end if
         !
         if (MY_TIME % meteo_time >= MY_TIME % run_end) then
            meteotime = .false.  ! switch off meteo update
            call dbs_close_files(MY_FILES, MY_ERR)
            if (MY_ERR % flag .ne. 0) return
         end if
      end if
      !
      !*** Integrate in time
      !
      call F3D_time_step(MY_TIME, MY_GRID, MY_MOD, MY_MET, MY_SPE, MY_TRA, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
      !
      MY_TIME % time = MY_TIME % time + MY_TIME % gl_dt
      MY_TIME % iiter = MY_TIME % iiter + 1
      MY_TRA % gl_mass_in = MY_TRA % gl_mass_in + MY_TIME % gl_dt * MY_TRA % gl_mass_rate    ! Total mass (including gas species)
      !
      if (MY_TIME % time .ge. MY_TIME % run_end) MY_TIME % go_on = .false.
      !
      !*** ends a time step
      !
      call F3D_end_time_step(MY_FILES, MY_TIME, MY_MOD, MY_OUT, MY_GRID, MY_MET, MY_SPE, MY_TRA, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
      !
   end do
   !
   !*** Release resources used in time loop
   !
   call F3D_release(MY_TRA, MY_GRID)
   !
   !*** If necessary, writes final deposit at tracked_points
   !
   if (MY_OUT % track_points) call F3D_out_pts_grn(MY_FILES, MY_SPE, MY_OUT, MY_TRA, MY_ERR)
   !
   !*** If necessary, write the restart file
   !
   if (MY_OUT % out_rst) then
      call nc_IO_out_rst(MY_FILES, MY_GRID, MY_TRA, MY_OUT, MY_TIME, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
   end if
   !
   !*** If necessary, write json file with task metadata (master model only)
   !
   if (MY_OUT % out_json) then
      if (master_model) call json_IO_write_f3d(MY_FILES, MY_SPE, MY_TIME, MY_GRID, MY_MET, MY_MOD, MY_AGR, MY_OUT, MY_ERR)
      call parallel_bcast(MY_ERR % flag, 1_ip, 0_ip)
      if (MY_ERR % flag .ne. 0) return
   end if
   !
   !*** If necessary, write the footprint file (master model only)
   !
   if (MY_OUT % out_ftp) then
      if (master_model) call nc_IO_out_ftp(MY_FILES, MY_GRID, MY_OUT % MY_CUTS, MY_SPE, MY_OUT, MY_ERR)
      call parallel_bcast(MY_ERR % flag, 1_ip, 0_ip)
      if (MY_ERR % flag .ne. 0) return
   end if
   !
   !*** Check if extrapolations were required in computations involving flight levels
   !
   call nc_IO_out_check_fl_warns(MY_OUT%MY_CUTS, MY_ERR)
   !
   return
end subroutine task_Fall3D

subroutine notask_Fall3D(MY_FILES, MY_ERRORS)
   use KindType, only: ip, NTASKS, &
      TASK_SET_DBS, &
      TASK_FLAG, &
      FILE_LIST, &
      ERROR_STATUS
   use Shared, only: MY_GRID, MY_TIME, MY_MET, MY_OUT
   use Parallel, only: master_model, parallel_bcast
   use Dbs, only: dbs_update_data, &
      dbs_create_file, &
      dbs_update_file, &
      dbs_close_files
   implicit none
   !
   type(FILE_LIST), intent(INOUT) :: MY_FILES
   type(ERROR_STATUS), intent(INOUT) :: MY_ERRORS(NTASKS)
   !
   integer(ip) :: task, it
   !
   if (TASK_FLAG(TASK_SET_DBS)) then
      task = TASK_SET_DBS
   else
      return
   end if
   !
   associate (MY_ERR => MY_ERRORS(task))
      !
      !*** Create dbs.nc file
      !
      if (MY_OUT % out_dbs_file) then
         call dbs_create_file(MY_GRID, MY_OUT, MY_MET, MY_FILES, MY_ERR)
         if (MY_ERR % flag .ne. 0) return
      end if
      !
      !*** Interpolates rest of meteo variables within the dbs time slab
      !
      MY_MET % its = 0
      MY_MET % ite = 0
      !
      do it = 1, MY_MET % nt
         !
         !*** Update data in MY_MET
         !
         call dbs_update_data(it, MY_FILES, MY_GRID, MY_MET, MY_ERR)
         if (MY_ERR % flag .ne. 0) return
         !
         !*** Update dbs.nc file
         !
         if (MY_OUT % out_dbs_file) then
            call dbs_update_file(it, MY_FILES, MY_MET, MY_ERR)
            if (MY_ERR % flag .ne. 0) return
         end if
         !
      end do
      !
      !*** Close DBS and MET files
      !
      call dbs_close_files(MY_FILES, MY_ERR)
      if (MY_ERR % flag .ne. 0) return
      !
   end associate
   !
   return
end subroutine notask_Fall3D
