subroutine task_SetEns(MY_FILES,MY_ERR)
  !
  !------------------------------------
  !    subroutine task_SetEns
  !------------------------------------
  !
  !>   @brief
  !>   Task for generation of ensemble runs in FALL3D
  !>   @author
  !>   lmingari
  !
  use KindType, only: ip, &
                      TASK_SET_ENS, &
                      FILE_LIST, &
                      ERROR_STATUS
  use Shared,   only: MY_ENS, nens
  use Parallel, only: COMM_WORLD, &
                      COMM_COUPLE, &
                      master_model, &
                      master_world, &
                      task_id, &
                      parallel_sum, &
                      parallel_bcast
  use InpOut,   only: inpout_open_file, &
                      inpout_close_file
!  use json_IO,  only: json_IO_write_json_start,     &
!                      json_IO_write_code_block,     &
!                      json_IO_write_ensemble_block, &
!                      json_IO_write_json_end
  use Ensemble, only: nper, &
                      ensemble_init,            &
                      ensemble_read_inp,        &
                      ensemble_bcast_params,    &
                      ensemble_read_random,     &
                      ensemble_write_random,    &
                      ensemble_init_random,     &
                      ensemble_print_info
  implicit none
  !
  type(FILE_LIST),    intent(INOUT) :: MY_FILES
  type(ERROR_STATUS), intent(INOUT) :: MY_ERR
  !
  integer(ip) :: ierr
  !
  !*** Initializations
  !
  call CPU_TIME(MY_ERR%cpu_start_time)
  !
  !*** Initialise MY_ENS structure and set default values
  !    Define:
  !      - MY_ENS%perturbation_name
  !      - MY_ENS%perturbation_type
  !      - MY_ENS%perturbation_pdf
  !      - MY_ENS%perturbation_range
  !      - MY_ENS%perturbation_random
  !
  call ensemble_init(MY_ENS,MY_ERR)
  !
  !*** Read ENSEMBLE block from input file
  !    Define:
  !      - MY_ENS%read_random_from_file
  !      - MY_ENS%perturbation_type
  !      - MY_ENS%perturbation_pdf
  !      - MY_ENS%perturbation_range
  !
  if(master_model) call ensemble_read_inp(MY_FILES,MY_ENS,MY_ERR)
  call parallel_bcast(MY_ERR%flag,1,0)
  if(MY_ERR%flag.ne.0) return
  call ensemble_bcast_params(MY_ENS,MY_ERR)
  !
  !*** Read/set random numbers in the range (-1,1)
  !    Define:
  !      - MY_ENS%perturbation_random
  !      - MY_ENS%read_random_from_file
  !
  if(MY_ENS%read_random_from_file) then
      ierr = 0
      if(master_model) then
          call ensemble_read_random(MY_FILES,MY_ENS,MY_ERR)
          if(MY_ERR%flag.ne.0) then
              MY_ERR%flag = 0
              ierr = 1
          end if
          call parallel_sum(ierr,COMM_COUPLE)
      end if
      call parallel_bcast(ierr,1,0)
  end if
  !
  if(ierr>0) then
      MY_ENS%read_random_from_file = .false.  ! force subsequent file creation
      call task_wriwarn(MY_ERR,'Unable to read random numbers')
  end if
  !
  if(.not.MY_ENS%read_random_from_file) then
      if(master_model) then
          call ensemble_init_random (MY_ENS,  MY_ERR)
          call ensemble_write_random(MY_FILES,MY_ENS,MY_ERR)
      end if
  end if
  call parallel_bcast(MY_ENS%perturbation_random,nper,0)
  !
  !*** Write information to the log file
  !
  call ensemble_print_info(MY_FILES,MY_ENS,MY_ERR)
  !
  !*** Write json file with task metadata
  !
!  if(master_world) then
!     call inpout_open_file(MY_FILES%file_json,MY_FILES%lujsons(TASK_SET_ENS),MY_ERR)
!     call json_IO_write_json_start(TASK_SET_ENS,MY_FILES,MY_ERR)
!     !
!     call json_IO_write_code_block(TASK_SET_ENS,MY_FILES,MY_ERR)
!     call json_IO_write_ensemble_block(nens,MY_FILES,MY_ENS,MY_ERR)
!     !
!     call json_IO_write_json_end(TASK_SET_ENS,MY_FILES,MY_ERR)
!     call inpout_close_file(MY_FILES%lujsons(TASK_SET_ENS),MY_ERR)
!  end if
!  call parallel_bcast(MY_ERR%flag,1,0,COMM_WORLD)
!  if(MY_ERR%flag.ne.0) return
  !
  return
end subroutine task_SetEns
