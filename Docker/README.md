## USING DOCKER FOR FALL3D

This directory contains the Docker files (Dockerfile) to create a Docker image with all the necessary packages for the installation of FALL3D. The Dockerfiles are available for MAC with ARM64 architecture and Linux with AMD64 architecture.

If you have no experience creating Docker images, this repository offers pre-built Docker images ready to use. They can be found at the following link:

https://gitlab.com/fall3d-suite/fall3d/container_registry