#!/bin/bash

# Description:
#   Convert a set of GFS/GEFS grib files 
#   into a single netcdf file
#   which can be read by FALL3D.
#
# User intervention:
#   Modify only the header section!
#   You have to define the filename format 
#   for the input grib files. For example, 
#   if the input filenames are:
#   gfs.t00z.pgrb2.0p25.f000
#   gfs.t00z.pgrb2.0p25.f001 ...
#   edit the fname variable in the UPDATE_FNAME 
#   function as:
#   fname="gfs.t${CYCLE}z.pgrb2.0p25.f${HOUR}"
#
# Parameters:
# - TABLEFILE: 
#   auxiliary file defining pressure levels.
#   You have to define "TABLEFILE" depending on 
#   the GFS resolution (the list of pressure 
#   levels depends on the resolution)
# - TMIN/TMAX:
#   refers to min/max forecast hours.
# - CYCLE:
#   is the cycle time (0,6,12,18)
# - GRIBPATH:
#   path with the grib files
# - UPDATE_FNAME:
#   function to update filenames

########## Edit header ########## 
WGRIBEXE=wgrib2
TABLEFILE=grib_tables/gfs_0p50.levels
TMIN=0
TMAX=48
STEP=3
CYCLE=0
GRIBPATH=GFS
UPDATE_FNAME (){
    fname=${GRIBPATH}/gfs.t${CYCLE}z.pgrb2full.0p50.f${HOUR}
    OUTPUTFILE=etna.gfs.nc
}
################################# 

variables="HGT|TMP|RH|UGRD|VGRD|VVEL|PRES|PRATE|LAND|HPBL|SFCR"
CYCLE=$(printf %02d $CYCLE)

for i in $(seq ${TMIN} ${STEP} ${TMAX})
do 
    HOUR=$(printf %03d $i)
    UPDATE_FNAME

    echo "Processing ${fname}..."
    ${WGRIBEXE} "${fname}" \
        -match ":(${variables}):" \
        -match ":(([0-9]*[.])?[0-9]+ mb|surface|2 m above ground|10 m above ground):" \
        -nc_table "${TABLEFILE}" \
        -append \
        -nc3 \
        -netcdf \
        "${OUTPUTFILE}" > wgrib.log
done
