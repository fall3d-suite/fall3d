import xarray as xr
import cfgrib
from dask.diagnostics import ProgressBar

# This script is intended to be used to convert CARRA data in grib
# format into netcdf format since FALL3D only can read netcdf data.
# However, it could be adapted for other datasets, including ERA5 

### Parameters ###
fname_sfc = 'surface.grib'         #Grib file with surface data
fname_lev = 'pressure_levels.grib' #Grib file with upper level data
fname_out = 'merged.nc'            #netCDF output file to be read by FALL3D
useChunks = False                  #Use chunking: important for avoiding memory overflows
##################

ds_list  = cfgrib.open_datasets(fname_lev)
ds_list += cfgrib.open_datasets(fname_sfc)

# Remove some metadata, required by the merge method used below
# Important for CARRA datasets!
keys2remove = ['surface','heightAboveGround']

for i,ds in enumerate(ds_list):
    ds_list[i] = ds.drop_vars(keys2remove,errors='ignore')
    #Apparently it was fixed and the following lines 
    #are not required anymore
#    if 'sr' in ds: 
#        print("******Warn******* Correcting coordinates...")
#        ds_list[i]['latitude']  = ds_list[0].latitude
#        ds_list[i]['longitude'] = ds_list[0].longitude

print("Merging datasets...")
if useChunks:
    ds = xr.merge(ds_list).chunk(chunks={"time": 1})
    print("Chunks information:")
    print(ds.chunks)
else:
    ds = xr.merge(ds_list)

print("Saving data...")
delayed_obj = ds.to_netcdf(fname_out, compute=False)

with ProgressBar():
    results = delayed_obj.compute()
