## Credits

* The EuroHPC “Center of Excellence for Exascale in Solid Earth” (**[ChEESE-1P](https://cheese-coe.eu)** and **[ChEESE-2P](https://www.cheese2.eu)**, 2018-2026, GA No823844 and 101093038 respectively).
* Horizon Europe project “A Digital Twin for GEOphysical extremes” (**[DT-GEO](https://www.dtgeo.eu)**, GA No 101058129, Sep 2022 to Aug 2025).

## Development Lead

* Arnau Folch (CSIC): <afolch@geo3bcn.csic.es>

## Main contributors

* Leonardo Mingari (CSIC): <lmingari@geo3bcn.csic.es>
* Heribert Pascual (CSIC): <hpascual@geo3bcn.csic.es> 
* Antonio Costa (INGV): <antonio.costa@ingv.it>
* Giovanni Macedonio (INGV): <giovanni.macedonio@ingv.it>

